/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QEventLoop>
#include <QMessageBox>
#include <QWidget>

// PivotCalibration includes
#include "qSlicerPivotCalibrationModuleWidget.h"
#include "ui_qSlicerPivotCalibrationModule.h"

// Slicer includes
#include <qSlicerApplication.h>

// VTK includes
#include <vtkMRMLScene.h>
#include <vtkSmartPointer.h>
#include <vtksys/SystemTools.hxx>
#include <vtkMatrix4x4.h>

// MRML includes
#include "vtkMRMLLinearTransformNode.h"
#include "vtkMRMLIGTLConnectorNode.h"

// PlusLib includes
#include <vtkPivotCalibrationAlgo.h>
#include <vtkTransformRepository.h>

#include <limits>


namespace /* anonymous */ {
// local unbound functions

//-----------------------------------------------------------------------------
/// \brief Compare to matrices4x4 element by element
///
///
 bool operator == ( const vtkSmartPointer< vtkMatrix4x4 > lh,
                    const vtkSmartPointer< vtkMatrix4x4 > rh )
{
// I could not find any instance of this matrix equality operator, so
// I put one here. If there is one already available, then it should
// be used instead of this one.
  for ( int i = 0; i < 4; i++ )
  {
    for ( int j = 0; j < 4; j++ )
    {
      if ( lh->GetElement( i, j ) != rh->GetElement( i, j ) )
      {
        return false;
      }
    }
  }
  return true;
}


bool operator != ( const vtkSmartPointer< vtkMatrix4x4 > lh,
                   const vtkSmartPointer< vtkMatrix4x4 > rh )
{
  return ! (lh == rh);
}

} // end namespace


//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerPivotCalibrationModuleWidgetPrivate: public Ui_qSlicerPivotCalibrationModule
{
public:
  qSlicerPivotCalibrationModuleWidgetPrivate();

  // Nothing yet
};


//-----------------------------------------------------------------------------
// qSlicerPivotCalibrationModuleWidgetPrivate methods

//-----------------------------------------------------------------------------
qSlicerPivotCalibrationModuleWidgetPrivate::qSlicerPivotCalibrationModuleWidgetPrivate()
{
}


//-----------------------------------------------------------------------------
// qSlicerPivotCalibrationModuleWidget methods

//-----------------------------------------------------------------------------
// Constructor
qSlicerPivotCalibrationModuleWidget::qSlicerPivotCalibrationModuleWidget( QWidget* _parent )
  : Superclass( _parent ),
  d_ptr( new qSlicerPivotCalibrationModuleWidgetPrivate ),
  editingState(false)
{
  // Allocate a new plusConfig if one not already there.
  // Note that this uses fixed relative paths for the files and directories.
  //
  if ( !this->plusConfig )
  {
    this->plusConfig = vtkSmartPointer< vtkPlusConfig >( vtkPlusConfig::GetInstance() );
    this->plusConfig->SetOutputDirectory( "../PlusLib-build/Output" );
    this->plusConfig->SetDeviceSetConfigurationDirectory( "../PlusLib/data/ConfigFiles" );
  }

  /// \todo This doesn't look right
  this->m_PivotCalibration = vtkSmartPointer< vtkPivotCalibrationAlgo >::Take( vtkPivotCalibrationAlgo::New() );
  //+ this->m_PivotCalibration = vtkSmartPointer< vtkPivotCalibrationAlgo >::New(); Would this work better???

  //+ The memory management for this mode is not clear. Here we
  // allocate a new node and manage it with a smart pointer. Later we
  // accept a pointer from a method and overwrite this smart pointer.
  ///todo This needs more investigation.
  this->pivotTransformNode = vtkSmartPointer< vtkMRMLLinearTransformNode >::New();
  this->numberOfPointsToAcquire = 0;
}


//-----------------------------------------------------------------------------
qSlicerPivotCalibrationModuleWidget::~qSlicerPivotCalibrationModuleWidget()
{
  vtkPlusConfig::SetInstance(0);
}


//-----------------------------------------------------------------------------
void qSlicerPivotCalibrationModuleWidget::setup()
{
  Q_D( qSlicerPivotCalibrationModuleWidget );
  d->setupUi( this );
  // Connect node selector with module itself
  this->connect( d->TransformNodeSelector,
                 SIGNAL( currentNodeChanged( vtkMRMLNode* ) ),
                 SLOT( onNodeSelected( vtkMRMLNode* ) ) );

  // Connect calibration button
  this->connect( d->StartCalibrationPushButton,
                 SIGNAL( clicked() ),
                 SLOT( startCalibration() ) );

  // Connect "No Calibration Required" button
  this->connect( d->NoCalibrationRequired,
                 SIGNAL( clicked() ),
                 SLOT( noCalibrationrequired() ) );

  this->connect( d->editSave,
                 SIGNAL( clicked() ),
                 SLOT( editSave() ) );

  this->Superclass::setup();
}


//-----------------------------------------------------------------------------
void qSlicerPivotCalibrationModuleWidget::onNodeSelected( vtkMRMLNode* node )
{
  Q_D( qSlicerPivotCalibrationModuleWidget );

  // Get vtkMRMLLinearTransformNode node from passed in node.
  vtkMRMLLinearTransformNode* transformNode = vtkMRMLLinearTransformNode::SafeDownCast( node );

  // Enable 'start' button if node is a vtkMRMLLinearTransformNode
  // This basically validates the above node extraction
  bool transformValid( transformNode != 0 );
  d->StartCalibrationPushButton->setEnabled( transformValid );
  d->editSave->setEnabled( transformValid );

  // If our node is not valid, then do not replace existing transform
  if ( transformValid )
  {
    this->pivotTransformNode = transformNode;
  }

  d->StatusLabel->setText( "Select calibration method" );
  d->CurrentMatrixWidget->setEnabled( 1 );
  d->CalibrationProgressBar->setValue( 0 );

  d->CurrentMatrixWidget->setEditable(false); // start out as ineditable
  d->CurrentMatrixWidget->setDecimals(6);
  d->CurrentMatrixWidget->setMinimum( - std::numeric_limits<double>::max() );
  d->CurrentMatrixWidget->setMaximum (std::numeric_limits<double>::max() );
}


//-----------------------------------------------------------------------------
void qSlicerPivotCalibrationModuleWidget::startCalibration()
{
  Q_D( qSlicerPivotCalibrationModuleWidget );

  d->StatusLabel->setEnabled( 1 );
  d->StatusLabel->setText( "Current point position matrix:" );
  d->CurrentMatrixWidget->setEnabled( 1 );
  d->CurrentMatrixWidget->setMRMLTransformNode( this->pivotTransformNode );

  //Initialize pivot calibration algorithm
  this->m_PivotCalibration->RemoveAllCalibrationPoints();

  vtkSmartPointer< vtkMatrix4x4 > stylusToReferenceMatrix;
  vtkSmartPointer< vtkMatrix4x4 > previousMatrix = vtkSmartPointer< vtkMatrix4x4 >::New();

  numberOfPointsToAcquire = d->NumberOfPointsToAcquireSliderWidget->value();

  int progressBar = 0;
  int notEnoughPointsForCalibration = 0;

  // validate tracker is on line
  {
    vtkMRMLScene* currentScene = qSlicerApplication::application()->mrmlScene();
    vtkMRMLIGTLConnectorNode* node = vtkMRMLIGTLConnectorNode::SafeDownCast( currentScene->GetFirstNodeByName("IGTLConnector") );
    if (!node || (node->GetState() != vtkMRMLIGTLConnectorNode::STATE_CONNECTED) )
    {
      QMessageBox::warning( this, "Error"," Please establish link between tracker and SlicerPET" );
      // really should not copntinue
      return;
    }
  }

  // Acquire positions for pivot calibration
  for ( int i = 0; i < numberOfPointsToAcquire; ++i )
  {
    progressBar = ( ( i + 1 ) * 100 ) / numberOfPointsToAcquire;
    d->CalibrationProgressBar->setValue( progressBar );
    vtksys::SystemTools::Delay( 50 );

    //Get current matrix of Tracker
    stylusToReferenceMatrix = this->pivotTransformNode->GetMatrixTransformToParent();
    this->m_PivotCalibration->InsertNextCalibrationPoint( stylusToReferenceMatrix );

    // Small check to see if tracker is out of range. We use this
    // method of comparing from t to t-1 because if the tracker is out
    // of range the matrix will remain unchanged
    if ( previousMatrix != stylusToReferenceMatrix )
    {
      d->StatusLabel->setText( "Tracker out of range!" );
      notEnoughPointsForCalibration++;
    }
    else
    {
      d->StatusLabel->setText( "Current point position matrix:" );
    }

    previousMatrix->DeepCopy( stylusToReferenceMatrix );

    //Give the priority to other events to update the tracker transform in SlicerPET
    QCoreApplication::processEvents( QEventLoop::AllEvents, 10 );
  } // end for

  // validate tracker is still on line
  {
    vtkMRMLScene* currentScene = qSlicerApplication::application()->mrmlScene();
    vtkMRMLIGTLConnectorNode* node = vtkMRMLIGTLConnectorNode::SafeDownCast( currentScene->GetFirstNodeByName("IGTLConnector") );
    if (!node || (node->GetState() != vtkMRMLIGTLConnectorNode::STATE_CONNECTED) )
    {
      QMessageBox::warning( this, "Error"," Please establish link between tracker and SlicerPET" );
      // really should not copntinue
      return;
    }
  }

  /*
  if ( ( notEnoughPointsForCalibration >= 40 )
       && ( notEnoughPointsForCalibration >= int(numberOfPointsToAcquire / 2) ) )
  {
    QMessageBox::warning( this, "Error", "Not enough points for calibration algorithm" );
    return;
  }
  */

  if ( this->m_PivotCalibration->DoPivotCalibration() != PLUS_SUCCESS )
  {
    QMessageBox::warning( this, "Error", "Calibration error!" );
    return;
  }

  d->StatusLabel->setText( "CalibrationCompleted! The resulting transform is:" );

  vtkMatrix4x4* finalTransform;
  finalTransform = this->GetMatrix(); // get calibration matrix
  updateTransformNode( finalTransform ); // save in MRML node

} // qSlicerPivotCalibrationModuleWidget::startCalibration


//-----------------------------------------------------------------------------
/// \brief Update transform in MRML node.
///
/// The supplied transform is stored in our calibration transform node.
/// If that node is not present, then it is created. Otherwise the transform is updated
/// in that node.
///
/// The matrix data is DeepCopied into the transform node, so memory
/// management is not too much of an issue here.
///

void qSlicerPivotCalibrationModuleWidget::updateTransformNode( vtkMatrix4x4* finalTransform )
{
  Q_D( qSlicerPivotCalibrationModuleWidget );

  // The name used here is important. This node is looked for, by name, from other source files.
  const char* nodeName( "CalibrationResultTransform" );

  //Get the current SlicerPET scene
  vtkMRMLScene* currentScene = qSlicerApplication::application()->mrmlScene();

  /// \todo Maybe a plain pointer here.
  vtkMRMLLinearTransformNode* existingResultNode = vtkMRMLLinearTransformNode::SafeDownCast( currentScene->GetFirstNodeByName( nodeName ) );

  // When finalTransform is passed to the MRML node, it is kept by reference,
  // so somewhere we have to maintain the storage for the matrix.

  if ( existingResultNode )
  {
    existingResultNode->Reset();
    existingResultNode->SetMatrixTransformToParent( finalTransform );
    d->CurrentMatrixWidget->setMRMLTransformNode( existingResultNode );
  }
  else
  {
    vtkMRMLLinearTransformNode* resultNode = vtkMRMLLinearTransformNode::New();
    resultNode->SetName( nodeName );
    resultNode->SetMatrixTransformToParent( finalTransform );

    d->CurrentMatrixWidget->setMRMLTransformNode( resultNode );
    currentScene->AddNode( resultNode );
  }
}


//-----------------------------------------------------------------------------
/// \brief Handles "No Calibration required" button press
void qSlicerPivotCalibrationModuleWidget::noCalibrationrequired()
{
  Q_D( qSlicerPivotCalibrationModuleWidget );

  // Disable calibration button
  d->StartCalibrationPushButton->setEnabled(false);
  d->CurrentMatrixWidget->setEditable(false);
  d->editSave->setEnabled( false );

  // Get default identity matrix
  vtkSmartPointer< vtkMatrix4x4 > finalTransform = vtkSmartPointer< vtkMatrix4x4 >::New();
  updateTransformNode( finalTransform ); // save in MRML node
  d->StatusLabel->setText( "Calibration done" );
}


//-----------------------------------------------------------------------------
/// \brief Handle edit and save transform events.
void qSlicerPivotCalibrationModuleWidget::editSave()
{
  Q_D( qSlicerPivotCalibrationModuleWidget );

  // Disable calibration button
  d->StartCalibrationPushButton->setEnabled(false);
  d->NoCalibrationRequired->setEnabled(false);

  // Determine our current state
  if ( this->editingState )
  {
    // We were editing, so now we must:
    this->editingState = false; // 1) change our state back to not editing
    d->CurrentMatrixWidget->setEditable(false); // 2) make matrix not editable
    d->editSave->setText("Edit Transform"); // 3) change button text
    // 4) save edited transform
    vtkMatrix4x4* finalTransform = d->CurrentMatrixWidget->matrix();
    updateTransformNode( finalTransform ); // save in MRML node
    d->StatusLabel->setText( "Editing done" );
  }
  else // we were not editing
  {
    // Edit button pressed, go to editing state
    this->editingState = true;

    d->CurrentMatrixWidget->setEditable(true);
    d->editSave->setText("Save Transform"); // change button text
    d->StatusLabel->setText( "Edit calibration matrix" );
  }
}


//-----------------------------------------------------------------------------
/// \brief Get the result matrix of pivot calibration.
///
/// Get a copy of the pivot calibratrion matrix.
///
/// \return Pointer to new matrix containing the calibration transform.
/// \todo This may be better to return vtkMatrix4x4 const*
/// I think all we want to do here is to return a reference to the matrix, which
/// is in turn saved in the MRML node.
vtkMatrix4x4* qSlicerPivotCalibrationModuleWidget::GetMatrix()
{
  // The datum is a writable pointer to a matrix in the pivot calibration module.
  vtkMatrix4x4* resultTransform = this->m_PivotCalibration->GetPivotPointToMarkerTransformMatrix();
  return resultTransform;
}
