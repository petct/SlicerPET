/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

#ifndef __qSlicerPivotCalibrationModuleWidget_h
#define __qSlicerPivotCalibrationModuleWidget_h

// SlicerQt includes
#include "qSlicerAbstractModuleWidget.h"

#include "qSlicerPivotCalibrationModuleExport.h"

/// VTK includes
#include <vtkSmartPointer.h>

class qSlicerPivotCalibrationModuleWidgetPrivate;
class vtkMatrix4x4;
class vtkMRMLLinearTransformNode;
class vtkMRMLNode;
class vtkPivotCalibrationAlgo;
class vtkPlusConfig;

/// \ingroup Slicer_QtModules_ExtensionTemplate
class Q_SLICER_QTMODULES_PIVOTCALIBRATION_EXPORT qSlicerPivotCalibrationModuleWidget
  : public qSlicerAbstractModuleWidget
{
  Q_OBJECT

    public:
  typedef qSlicerAbstractModuleWidget Superclass;

  qSlicerPivotCalibrationModuleWidget(QWidget *parent = 0);
  virtual ~qSlicerPivotCalibrationModuleWidget();

public slots:

protected:
  QScopedPointer< qSlicerPivotCalibrationModuleWidgetPrivate > d_ptr;

  virtual void setup();
  vtkMatrix4x4* GetMatrix();

  vtkSmartPointer< vtkPlusConfig > plusConfig;
  vtkSmartPointer< vtkPivotCalibrationAlgo > m_PivotCalibration;
  vtkMRMLLinearTransformNode*pivotTransformNode;
  double numberOfPointsToAcquire;


protected slots:
  void onNodeSelected( vtkMRMLNode* node );
  void startCalibration();
  void noCalibrationrequired();
  void editSave();

private:
  void updateTransformNode( vtkMatrix4x4* finalTransform );
  bool editingState;

  Q_DECLARE_PRIVATE( qSlicerPivotCalibrationModuleWidget );
  Q_DISABLE_COPY( qSlicerPivotCalibrationModuleWidget );
};

#endif
