/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QtPlugin>

// PivotCalibration Logic includes
#include <vtkSlicerPivotCalibrationLogic.h>

// PivotCalibration includes
#include "qSlicerPivotCalibrationModule.h"
#include "qSlicerPivotCalibrationModuleWidget.h"

//-----------------------------------------------------------------------------
Q_EXPORT_PLUGIN2(qSlicerPivotCalibrationModule, qSlicerPivotCalibrationModule);

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerPivotCalibrationModulePrivate
{
public:
  qSlicerPivotCalibrationModulePrivate();
};

//-----------------------------------------------------------------------------
// qSlicerPivotCalibrationModulePrivate methods

//-----------------------------------------------------------------------------
qSlicerPivotCalibrationModulePrivate
::qSlicerPivotCalibrationModulePrivate()
{
}

//-----------------------------------------------------------------------------
// qSlicerPivotCalibrationModule methods

//-----------------------------------------------------------------------------
qSlicerPivotCalibrationModule
::qSlicerPivotCalibrationModule(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerPivotCalibrationModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerPivotCalibrationModule::~qSlicerPivotCalibrationModule()
{
}

//-----------------------------------------------------------------------------
QString qSlicerPivotCalibrationModule::helpText()const
{
  return "This module runs a pivot calibration algorithm for a given tool.<br> It generates the transform to pass from the marker's coordinate system to the tip of the tool coordinate system, using the Plus library";
}

//-----------------------------------------------------------------------------
QString qSlicerPivotCalibrationModule::acknowledgementText()const
{
  return "This work is the result of a collaboration between Geortown University, Children National Medical Center, Prof. Paul Kinahan (University of Washington), Dr.Ron Korn (Scottsdale Medical Imaging) and Kitware Inc.<br>It was funded by the National Cancer Institute and the National Institutes of Health, Grant R42 CA153488";
}

//-----------------------------------------------------------------------------
QStringList qSlicerPivotCalibrationModule::contributors()const
{
  QStringList moduleContributors;
  moduleContributors << QString("Guillaume Sala (Kitware)");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QIcon qSlicerPivotCalibrationModule::icon()const
{
  return QIcon("");
}

//-----------------------------------------------------------------------------
QStringList qSlicerPivotCalibrationModule::categories() const
{
  return QStringList() << "IGT";
}

//-----------------------------------------------------------------------------
QStringList qSlicerPivotCalibrationModule::dependencies() const
{
  return QStringList();
}

//-----------------------------------------------------------------------------
void qSlicerPivotCalibrationModule::setup()
{
  this->Superclass::setup();
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation * qSlicerPivotCalibrationModule
::createWidgetRepresentation()
{
  return new qSlicerPivotCalibrationModuleWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerPivotCalibrationModule::createLogic()
{
  return vtkSlicerPivotCalibrationLogic::New();
}
