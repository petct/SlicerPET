// ITK includes
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>
#include <itkImageFileWriter.h>
#include <itkImageSeriesReader.h>
#include <itkImageSeriesReader.h>
#include <itkMetaDataDictionary.h>
#include <itkNumericSeriesFileNames.h>

//String includes
#include <string>
#include <fstream>

//gcdm includes
#include "gdcmFile.h"
#include "gdcmGlobal.h"

#define GetStringMacro(name)                    \
	virtual const char* Get##name () const  \
  {                                             \
  return this->m_##name.c_str();                \
  }

#define SetStringMacro(name)                            \
	virtual void Set##name (const char* _arg)       \
  {                                                     \
  if ( _arg && (_arg == this->m_##name) ) { return;}    \
  if (_arg)                                             \
	  {                                             \
	  this->m_##name = _arg;                        \
	  }                                             \
	 else                                           \
	  {                                             \
	  this->m_##name = "";                          \
	  }                                             \
  }

#define GetMacro(name,type)                     \
	virtual type Get##name ()               \
  {                                             \
  return this->m_##name;                        \
  }

#define SetMacro(name,type)                             \
	virtual void Set##name (const type _arg)        \
  {                                                     \
  if (this->m_##name != _arg)                           \
	  {                                             \
	  this->m_##name = _arg;                        \
	  }                                             \
  }

class PETDICOMReader{

public:
	PETDICOMReader();
	~PETDICOMReader();
	bool readDicomTags();
private:
	std::string m_WeightUnits;
	std::string m_DoseRadioactivityUnits;
	std::string m_TissueRadioactivityUnits;
	std::string m_PatientName;
	std::string m_StudyDate;
	std::string m_RadiopharmaceuticalStartTime;
	std::string m_DecayCorrection;
	std::string m_DecayFactor;
	std::string m_FrameReferenceTime;
	std::string m_RadionuclideHalfLife;
	std::string m_SeriesTime;
	std::string m_PhilipsSUVFactor;
	std::string m_CalibrationFactor;
	std::string m_VolumeUnits;

	double m_PETMin;
	double m_PETMax;
	double m_PETSUVmax;
	double m_PETSUVmin;
	double m_InjectedDose;
	double m_PatientWeight;

	int m_StudyDateDay;
	int m_StudyDateMonth;
	int m_StudyDateYear;
	std::string m_DirectoryName;

public:

	GetStringMacro ( DirectoryName );
	SetStringMacro ( DirectoryName );

	GetStringMacro ( WeightUnits );
	SetStringMacro ( WeightUnits );

	GetStringMacro ( DoseRadioactivityUnits );
	SetStringMacro ( DoseRadioactivityUnits );

	GetStringMacro ( TissueRadioactivityUnits );
	SetStringMacro ( TissueRadioactivityUnits );

	GetStringMacro ( PatientName );
	SetStringMacro ( PatientName );

	GetStringMacro ( StudyDate );
	SetStringMacro ( StudyDate );

	GetStringMacro ( RadiopharmaceuticalStartTime );
	SetStringMacro ( RadiopharmaceuticalStartTime );

	GetStringMacro (DecayCorrection);
	SetStringMacro (DecayCorrection);

	GetStringMacro (DecayFactor );
	SetStringMacro (DecayFactor );

	GetStringMacro (FrameReferenceTime );
	SetStringMacro (FrameReferenceTime );

	GetStringMacro (RadionuclideHalfLife );
	SetStringMacro (RadionuclideHalfLife );

	GetStringMacro (SeriesTime );
	SetStringMacro (SeriesTime );


	GetStringMacro (PhilipsSUVFactor );
	SetStringMacro (PhilipsSUVFactor );

	GetStringMacro (CalibrationFactor );
	SetStringMacro (CalibrationFactor );

	GetStringMacro (VolumeUnits );
	SetStringMacro (VolumeUnits );

	GetMacro ( PETMin, double );
	SetMacro ( PETMin, double );

	GetMacro ( PETMax, double );
	SetMacro ( PETMax, double );

	GetMacro ( PETSUVmax, double );
	SetMacro ( PETSUVmax, double );

	GetMacro ( InjectedDose, double );
	SetMacro ( InjectedDose, double );

	GetMacro ( PatientWeight, double );
	SetMacro ( PatientWeight, double );

	GetMacro( StudyDateYear, int );
	SetMacro( StudyDateYear, int );

	GetMacro( StudyDateMonth, int );
	SetMacro( StudyDateMonth, int );

	GetMacro( StudyDateDay, int );
	SetMacro( StudyDateDay, int );
};
