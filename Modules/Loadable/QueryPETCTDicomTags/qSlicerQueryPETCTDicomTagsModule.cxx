/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QtPlugin>

// QueryPETCTDicomTags Logic includes
#include <vtkSlicerQueryPETCTDicomTagsLogic.h>

// QueryPETCTDicomTags includes
#include "qSlicerQueryPETCTDicomTagsModule.h"
#include "qSlicerQueryPETCTDicomTagsModuleWidget.h"

//-----------------------------------------------------------------------------
Q_EXPORT_PLUGIN2(qSlicerQueryPETCTDicomTagsModule, qSlicerQueryPETCTDicomTagsModule);

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerQueryPETCTDicomTagsModulePrivate
{
public:
  qSlicerQueryPETCTDicomTagsModulePrivate();
};

//-----------------------------------------------------------------------------
// qSlicerQueryPETCTDicomTagsModulePrivate methods

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModulePrivate
::qSlicerQueryPETCTDicomTagsModulePrivate()
{
}

//-----------------------------------------------------------------------------
// qSlicerQueryPETCTDicomTagsModule methods

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModule
::qSlicerQueryPETCTDicomTagsModule(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerQueryPETCTDicomTagsModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModule::~qSlicerQueryPETCTDicomTagsModule()
{
}

//-----------------------------------------------------------------------------
QString qSlicerQueryPETCTDicomTagsModule::helpText()const
{
  return "This is a test module bundled in an extension to query for PETCT Dicom tags";
}

//-----------------------------------------------------------------------------
QString qSlicerQueryPETCTDicomTagsModule::acknowledgementText()const
{
  return "This work was funded by an NIH grant";
}

//-----------------------------------------------------------------------------
QStringList qSlicerQueryPETCTDicomTagsModule::contributors()const
{
  QStringList moduleContributors;
  moduleContributors << QString("Rahul Khare (CNMC)");
  moduleContributors << QString("Andinet Enquobahrie (Kitware)");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QIcon qSlicerQueryPETCTDicomTagsModule::icon()const
{
  return QIcon(":/Icons/QueryPETCTDicomTags.png");
}

//-----------------------------------------------------------------------------
QStringList qSlicerQueryPETCTDicomTagsModule::categories() const
{
  return QStringList() << "Examples";
}

//-----------------------------------------------------------------------------
QStringList qSlicerQueryPETCTDicomTagsModule::dependencies() const
{
  return QStringList();
}

//-----------------------------------------------------------------------------
void qSlicerQueryPETCTDicomTagsModule::setup()
{
  this->Superclass::setup();
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation * qSlicerQueryPETCTDicomTagsModule
::createWidgetRepresentation()
{
  return new qSlicerQueryPETCTDicomTagsModuleWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerQueryPETCTDicomTagsModule::createLogic()
{
  return vtkSlicerQueryPETCTDicomTagsLogic::New();
}
