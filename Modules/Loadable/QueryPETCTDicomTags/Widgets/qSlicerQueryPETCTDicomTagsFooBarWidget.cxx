/*==============================================================================

  Program: 3D Slicer

  Copyright (c) Kitware Inc.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
  and was partially funded by NIH grant 3P41RR013218-12S1

==============================================================================*/

// FooBar Widgets includes
#include "qSlicerQueryPETCTDicomTagsFooBarWidget.h"
#include "ui_qSlicerQueryPETCTDicomTagsFooBarWidget.h"

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_QueryPETCTDicomTags
class qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate
  : public Ui_qSlicerQueryPETCTDicomTagsFooBarWidget
{
  Q_DECLARE_PUBLIC(qSlicerQueryPETCTDicomTagsFooBarWidget);
protected:
  qSlicerQueryPETCTDicomTagsFooBarWidget* const q_ptr;

public:
  qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate(
    qSlicerQueryPETCTDicomTagsFooBarWidget& object);
  virtual void setupUi(qSlicerQueryPETCTDicomTagsFooBarWidget*);
};

// --------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate
::qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate(
  qSlicerQueryPETCTDicomTagsFooBarWidget& object)
  : q_ptr(&object)
{
}

// --------------------------------------------------------------------------
void qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate
::setupUi(qSlicerQueryPETCTDicomTagsFooBarWidget* widget)
{
  this->Ui_qSlicerQueryPETCTDicomTagsFooBarWidget::setupUi(widget);
}

//-----------------------------------------------------------------------------
// qSlicerQueryPETCTDicomTagsFooBarWidget methods

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsFooBarWidget
::qSlicerQueryPETCTDicomTagsFooBarWidget(QWidget* parentWidget)
  : Superclass( parentWidget )
  , d_ptr( new qSlicerQueryPETCTDicomTagsFooBarWidgetPrivate(*this) )
{
  Q_D(qSlicerQueryPETCTDicomTagsFooBarWidget);
  d->setupUi(this);
}

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsFooBarWidget
::~qSlicerQueryPETCTDicomTagsFooBarWidget()
{
}
