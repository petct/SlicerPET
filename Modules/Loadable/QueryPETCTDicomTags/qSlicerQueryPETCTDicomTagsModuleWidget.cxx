/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QDebug>

// SlicerQt includes
#include "qSlicerQueryPETCTDicomTagsModuleWidget.h"
#include "ui_qSlicerQueryPETCTDicomTagsModule.h"


//ITK PET Dicom reader
#include "QueryPETCTDicomTagsITKDICOMReader.h"

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerQueryPETCTDicomTagsModuleWidgetPrivate: public Ui_qSlicerQueryPETCTDicomTagsModule
{
public:
  qSlicerQueryPETCTDicomTagsModuleWidgetPrivate();
};

//-----------------------------------------------------------------------------
// qSlicerQueryPETCTDicomTagsModuleWidgetPrivate methods

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModuleWidgetPrivate::qSlicerQueryPETCTDicomTagsModuleWidgetPrivate()
{
}

//-----------------------------------------------------------------------------
// qSlicerQueryPETCTDicomTagsModuleWidget methods

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModuleWidget::qSlicerQueryPETCTDicomTagsModuleWidget(QWidget* _parent)
  : Superclass( _parent )
  , d_ptr( new qSlicerQueryPETCTDicomTagsModuleWidgetPrivate )
{
	m_pPETDicomReader = new PETDICOMReader;
}

//-----------------------------------------------------------------------------
qSlicerQueryPETCTDicomTagsModuleWidget::~qSlicerQueryPETCTDicomTagsModuleWidget()
{
	delete m_pPETDicomReader;
}

//-----------------------------------------------------------------------------
void qSlicerQueryPETCTDicomTagsModuleWidget::setup()
{
  Q_D(qSlicerQueryPETCTDicomTagsModuleWidget);
  d->setupUi(this);

  //Connecting slots and signals
  connect(d->PathLineEditLoadDICOM, SIGNAL(currentPathChanged(const QString &)),
	  this, SLOT (loadPETCTFile(const QString &)));
  connect(d->UpdateDicomTagsPushButton, SIGNAL(pressed()),
	  this, SLOT(updateDICOMTags()));

  this->Superclass::setup();
}


//-----------------------------------------------------------------------------
bool qSlicerQueryPETCTDicomTagsModuleWidget::loadPETCTFile( const QString & path)
{
	Q_D(qSlicerQueryPETCTDicomTagsModuleWidget);
	bool bLoaded = false;
	if(!path.isEmpty()) //Make sure the path is not empty
	{
		bLoaded = true;
		d->PETDICOMTagsBtn->setEnabled(true);
		d->PETDICOMTagsBtn->setCollapsed(false);

		//First clear up all the control values
		clearAllValues();

		//Enable the Update Dicom Tags button
		d->UpdateDicomTagsPushButton->setEnabled(true);

		m_pPETDicomReader->SetDirectoryName(path.toLocal8Bit().constData());

	}
	if(bLoaded)
		return true;
	else
		return false;
}

bool qSlicerQueryPETCTDicomTagsModuleWidget::updateDICOMTags()
{
	Q_D(qSlicerQueryPETCTDicomTagsModuleWidget);
		//Now use ITK to read the data
		
		bool retVal = m_pPETDicomReader->readDicomTags();
		//if(retVal)
		//	d->DebugTextBrowser->setText(QString("Working\n"));
		//else
		//	d->DebugTextBrowser->setText(QString("Something wrong\n"));


		if(retVal)
		{
		//Now load up all the DICOM values
		d->lineDecayCorrection->setText(QString(m_pPETDicomReader->GetDecayCorrection()));
		d->lineCalibrationFactor->setText(QString (m_pPETDicomReader->GetCalibrationFactor()));
		d->lineDecayFactor->setText(QString(m_pPETDicomReader->GetDecayFactor()));
		d->lineDoseRadioactivityUnits->setText(QString(m_pPETDicomReader->GetDoseRadioactivityUnits()));
		d->lineFrameReferenceTime->setText(QString(m_pPETDicomReader->GetFrameReferenceTime()));
		d->lineInjectedDose->setText(QString::number(m_pPETDicomReader->GetInjectedDose()));
		d->linePatientName->setText(QString(m_pPETDicomReader->GetPatientName()));
		d->linePAtientWeight->setText(QString::number(m_pPETDicomReader->GetPatientWeight()));
		d->linePhillipsSUVFactor->setText(QString(m_pPETDicomReader->GetPhilipsSUVFactor()));
		d->lineRadioNuclideHalfTime->setText(QString(m_pPETDicomReader->GetRadionuclideHalfLife()));
		d->lineRadiopharmaceuticalStartTime->setText(QString(m_pPETDicomReader->GetRadiopharmaceuticalStartTime()));
		d->lineSeriesTime->setText(QString(m_pPETDicomReader->GetSeriesTime()));
		d->lineStudyDate->setText(QString(m_pPETDicomReader->GetStudyDate()));
		d->lineWeightUnits->setText(QString(m_pPETDicomReader->GetWeightUnits()));
		}
		
	
	if(retVal)
		return true;
	else
		return false;
}

//-----------------------------------------------------------------------------
void qSlicerQueryPETCTDicomTagsModuleWidget::clearAllValues()
{
	Q_D(qSlicerQueryPETCTDicomTagsModuleWidget);
	d->lineDecayCorrection->setText(QString(""));
	d->lineCalibrationFactor->setText(QString (""));
	d->lineDecayFactor->setText(QString(""));
	d->lineDoseRadioactivityUnits->setText(QString(""));
	d->lineFrameReferenceTime->setText(QString(""));
	d->lineInjectedDose->setText(QString(""));
	d->linePatientName->setText(QString(""));
	d->linePAtientWeight->setText(QString(""));
	d->linePhillipsSUVFactor->setText(QString(""));
	d->lineRadioNuclideHalfTime->setText(QString(""));
	d->lineRadiopharmaceuticalStartTime->setText(QString(""));
	d->lineSeriesTime->setText(QString(""));
	d->lineStudyDate->setText(QString(""));
	d->lineWeightUnits->setText(QString(""));
}

