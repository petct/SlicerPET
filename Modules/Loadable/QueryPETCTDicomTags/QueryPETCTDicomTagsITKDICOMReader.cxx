#include "QueryPETCTDicomTagsITKDICOMReader.h"
#include "DCMTKFileReader.h"

PETDICOMReader::PETDICOMReader()
{

}

PETDICOMReader::~PETDICOMReader()
{

}

bool PETDICOMReader::readDicomTags()
{
	// read the DICOM dir to get the radiological data

	typedef short PixelValueType;
	typedef itk::Image< PixelValueType, 3 > VolumeType;
	typedef itk::ImageSeriesReader< VolumeType > VolumeReaderType;
	typedef itk::Image< PixelValueType, 2 > SliceType;
	typedef itk::ImageFileReader< SliceType > SliceReaderType;
	typedef itk::GDCMImageIO ImageIOType;
	typedef itk::GDCMSeriesFileNames InputNamesGeneratorType;
	typedef itk::VectorImage< PixelValueType, 3 > NRRDImageType;

	//std::string tempStr = fileName;
	////std::size_t pos = tempStr.find_last_of("/");
	//std::string dirName = tempStr;//tempStr.substr(0,pos);
	//m_DirectoryName = dirName;

	InputNamesGeneratorType::Pointer inputNames = InputNamesGeneratorType::New();
	inputNames->SetUseSeriesDetails(true);
	inputNames->SetDirectory(m_DirectoryName);
	itk::SerieUIDContainer seriesUIDs = inputNames->GetSeriesUIDs();

	const VolumeReaderType::FileNamesContainer & filenames = inputNames->GetFileNames(seriesUIDs[0]);

	std::string tag;
	std::string yearstr;
	std::string monthstr;
	std::string daystr;
	std::string hourstr;
	std::string minutestr;
	std::string secondstr;
	int len;
	short tagShort;
	double tagDouble;

	int parsingDICOM = 1;
	itk::DCMTKFileReader fileReader;
	fileReader.SetFileName(filenames[0]);
	fileReader.LoadFile();

	itk::DCMTKSequence seq;

	if(fileReader.GetElementSQ(0x0054,0x0016,seq,false) == EXIT_SUCCESS)
	{
		//parsingDICOM = 1;
		//      for(int i = 0; i < seq.card(); ++i)
		//        {
		//        itk::DCMTKSequence sqItem;
		//        seq.GetSequence(i,sqItem);
		//---
		//--- Radiopharmaceutical Start Time
		//        sqItem.GetElementTM(0x0018,0x1072,tag);
		seq.GetElementTM(0x0018,0x1072,tag);
		//--- expect A string of characters of the format hhmmss.frac;
		//---where hh contains hours (range "00" - "23"), mm contains minutes
		//---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
		//---contains a fractional part of a second as small as 1 millionth of a
		//---second (range "000000" - "999999"). A 24 hour clock is assumed.
		//---Midnight can be represented by only "0000" since "2400" would
		//---violate the hour range. The string may be padded with trailing
		//---spaces. Leading and embedded spaces are not allowed. One
		//---or more of the components mm, ss, or frac may be unspecified
		//---as long as every component to the right of an unspecified
		//---component is also unspecified. If frac is unspecified the preceding "."
		//---may not be included. Frac shall be held to six decimal places or
		//---less to ensure its format conforms to the ANSI
		//---Examples -
		//---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
		//---2. "1010" represents a time of 10 hours, and 10 minutes.
		//---3. "021" is an invalid value.
		if ( tag.c_str() == NULL || *(tag.c_str()) == '\0' )
		{
			this->SetRadiopharmaceuticalStartTime ("no value found");
		}
		else
		{
			len = tag.length();
			hourstr.clear();
			minutestr.clear();
			secondstr.clear();
			if ( len >= 2 )
			{
				hourstr = tag.substr(0, 2);
			}
			else
			{
				hourstr = "00";
			}
			if ( len >= 4 )
			{
				minutestr = tag.substr(2, 2);
			}
			else
			{
				minutestr = "00";
			}
			if ( len >= 6 )
			{
				secondstr = tag.substr(4);
			}
			else
			{
				secondstr = "00";
			}
			tag.clear();
			tag = hourstr.c_str();
			tag += ":";
			tag += minutestr.c_str();
			tag += ":";
			tag += secondstr.c_str();
			//list.injectionTime = tag.c_str();
			this->SetRadiopharmaceuticalStartTime(tag.c_str());
		}

		//---
		//--- Radionuclide Total Dose
		//        if(sqItem.GetElementDS(0x0018,0x1074,1,&list.injectedDose,false) != EXIT_SUCCESS)
		if(seq.GetElementDS<double>(0x0018,0x1074,1,&tagDouble,false) != EXIT_SUCCESS)
		{
			//list.injectedDose = 0.0;
			this->SetInjectedDose(0.0);
		}
		else
			this->SetInjectedDose(tagDouble);

		//---
		//--- RadionuclideHalfLife
		//--- Expect a Decimal String
		//--- A string of characters representing either
		//--- a fixed point number or a floating point number.
		//--- A fixed point number shall contain only the characters 0-9
		//--- with an optional leading "+" or "-" and an optional "." to mark
		//--- the decimal point. A floating point number shall be conveyed
		//--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
		//--- of the exponent. Decimal Strings may be padded with leading
		//--- or trailing spaces. Embedded spaces are not allowed.
		//        if(sqItem.GetElementCS(0x0018,0x1075,list.radionuclideHalfLife,false) != EXIT_SUCCESS)
		if(seq.GetElementDS(0x0018,0x1075,tag,false) != EXIT_SUCCESS)
		{
			//list.radionuclideHalfLife = "MODULE_INIT_NO_VALUE";
			this->SetRadionuclideHalfLife("no value found");
		}
		else
			this->SetRadionuclideHalfLife(tag.c_str());
		//---
		//---Radionuclide Positron Fraction
		//--- not currently using this one?
		std::string radioNuclidePositronFraction;
		//        if(sqItem.GetElementCS(0x0018,0x1075,radioNuclidePositronFraction,false) != EXIT_SUCCESS)
		if(seq.GetElementDS(0x0018,0x1075,radioNuclidePositronFraction,false) != EXIT_SUCCESS)
		{
			radioNuclidePositronFraction = "MODULE_INIT_NO_VALUE";
		}
	}

	//--
	//--- UNITS: something like BQML:
	//--- CNTS, NONE, CM2, PCNT, CPS, BQML,
	//--- MGMINML, UMOLMINML, MLMING, MLG,
	//--- 1CM, UMOLML, PROPCNTS, PROPCPS,
	//--- MLMINML, MLML, GML, STDDEV
	//---

	if(fileReader.GetElementCS(0x0054,0x1001,tag,false) == EXIT_SUCCESS)
	{
		//--- I think these are piled together. MBq ml... search for all.
		std::string units = tag.c_str();
		if ( ( units.find ("BQML") != std::string::npos) ||
			( units.find ("BQML") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("Bq");
			//list.radioactivityUnits= "Bq";
			//list.tissueRadioactivityUnits = "Bq";
		}
		else if ( ( units.find ("MBq") != std::string::npos) ||
			( units.find ("MBQ") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("MBq");
			//list.radioactivityUnits = "MBq";
			//list.tissueRadioactivityUnits = "MBq";
		}
		else if ( (units.find ("kBq") != std::string::npos) ||
			(units.find ("kBQ") != std::string::npos) ||
			(units.find ("KBQ") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("kBq");
			//list.radioactivityUnits = "kBq";
			//list.tissueRadioactivityUnits = "kBq";
		}
		else if ( (units.find ("mBq") != std::string::npos) ||
			(units.find ("mBQ") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("mBq");
			//list.radioactivityUnits = "mBq";
			//list.tissueRadioactivityUnits = "mBq";
		}
		else if ( (units.find ("uBq") != std::string::npos) ||
			(units.find ("uBQ") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("uBq");
			//list.radioactivityUnits = "uBq";
			//list.tissueRadioactivityUnits = "uBq";
		}
		else if ( (units.find ("Bq") != std::string::npos) ||
			(units.find ("BQ") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("Bq");
			//list.radioactivityUnits = "Bq";
			//list.tissueRadioactivityUnits = "Bq";
		}
		else if ( (units.find ("MCi") != std::string::npos) ||
			( units.find ("MCI") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("MCi");
			//list.radioactivityUnits = "MCi";
			//list.tissueRadioactivityUnits = "MCi";
		}
		else if ( (units.find ("kCi") != std::string::npos) ||
			(units.find ("kCI") != std::string::npos)  ||
			(units.find ("KCI") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("kCi");
			//list.radioactivityUnits = "kCi";
			//list.tissueRadioactivityUnits = "kCi";
		}
		else if ( (units.find ("mCi") != std::string::npos) ||
			(units.find ("mCI") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("mCi");
			//list.radioactivityUnits = "mCi";
			//list.tissueRadioactivityUnits = "mCi";
		}
		else if ( (units.find ("uCi") != std::string::npos) ||
			(units.find ("uCI") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("uCi");
			//list.radioactivityUnits = "uCi";
			//list.tissueRadioactivityUnits = "uCi";
		}
		else if ( (units.find ("Ci") != std::string::npos) ||
			(units.find ("CI") != std::string::npos) )
		{
			this->SetDoseRadioactivityUnits("Ci");
			//list.radioactivityUnits = "Ci";
			//list.tissueRadioactivityUnits = "Ci";
		}
		//list.volumeUnits = "ml";
		this->SetVolumeUnits("ml");
	}
	else
	{
		//--- default values.
		this->SetDoseRadioactivityUnits("MBq");
		this->SetVolumeUnits("ml");
		//list.radioactivityUnits = "MBq";
		//list.tissueRadioactivityUnits = "MBq";
		//list.volumeUnits = "ml";
	}


	//---
	//--- DecayCorrection
	//--- Possible values are:
	//--- NONE = no decay correction
	//--- START= acquisition start time
	//--- ADMIN = radiopharmaceutical administration time
	//--- Frame Reference Time  is the time that the pixel values in the Image occurred.
	//--- It's defined as the time offset, in msec, from the Series Reference Time.
	//--- Series Reference Time is defined by the combination of:
	//--- Series Date (0008,0021) and
	//--- Series Time (0008,0031).
	//--- We don't pull these out now, but can if we have to.
	if(fileReader.GetElementCS(0x0054,0x1102,tag,false) == EXIT_SUCCESS)
	{
		//---A string of characters with leading or trailing spaces (20H) being non-significant.
		//list.decayCorrection = tag.c_str();
		this->SetDecayCorrection(tag.c_str());
	}
	else
	{
		//list.decayCorrection = "MODULE_INIT_NO_VALUE";
		this->SetDecayCorrection("no value found");
	}

	//---
	//--- StudyDate
	//if(fileReader.GetElementDA(0x0008,0x0021,tag,false) == EXIT_SUCCESS)
	if(fileReader.GetElementDA(0x0008,0x0020,tag,false) == EXIT_SUCCESS)
	{
		//--- YYYYMMDD
		yearstr.clear();
		daystr.clear();
		monthstr.clear();
		len = tag.length();
		if ( len >= 4 )
		{
			yearstr = tag.substr(0, 4);
			// this->Year = atoi(yearstr.c_str() );
		}
		else
		{
			yearstr = "????";
			// this->Year = 0;
		}
		if ( len >= 6 )
		{
			monthstr = tag.substr(4, 2);
			// this->Month = atoi ( monthstr.c_str() );
		}
		else
		{
			monthstr = "??";
			// this->Month = 0;
		}
		if ( len >= 8 )
		{
			daystr = tag.substr (6, 2);
			//            this->Day = atoi ( daystr.c_str() );
		}
		else
		{
			daystr = "??";
			//            this->Day = 0;
		}
		tag.clear();
		tag = yearstr.c_str();
		tag += "/";
		tag += monthstr.c_str();
		tag += "/";
		tag += daystr.c_str();
		//list.studyDate = tag.c_str();
		this->SetStudyDate(tag.c_str());
	}
	else
	{
		//list.studyDate = "MODULE_INIT_NO_VALUE";
		this->SetStudyDate("no value found");
	}

	//---
	//--- PatientName
	if(fileReader.GetElementPN(0x0010,0x0010,tag,false) == EXIT_SUCCESS)
	{
		//list.patientName = tag.c_str();
		this->SetPatientName(tag.c_str());
	}
	else
	{
		//list.patientName = "MODULE_INIT_NO_VALUE";
		this->SetPatientName("no value found");
	}

	//---
	//--- DecayFactor
	if(fileReader.GetElementDS(0x0054,0x1321,tag,false) == EXIT_SUCCESS)
	{
		//--- have to parse this out. what we have is
		//---A string of characters representing either a fixed point number or a
		//--- floating point number. A fixed point number shall contain only the
		//---characters 0-9 with an optional leading "+" or "-" and an optional "."
		//---to mark the decimal point. A floating point number shall be conveyed
		//---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
		//---exponent. Decimal Strings may be padded with leading or trailing spaces.
		//---Embedded spaces are not allowed. or maybe atof does it already...
		//list.decayFactor =  tag.c_str() ;
		this->SetDecayFactor(tag.c_str());
	}
	else
	{
		//list.decayFactor =  "MODULE_INIT_NO_VALUE" ;
		this->SetDecayFactor("no value found");
	}


	//---
	//--- FrameReferenceTime
	if(fileReader.GetElementDS(0x0054,0x1300,tag,false) == EXIT_SUCCESS)
	{
		//--- The time that the pixel values in the image
		//--- occurred. Frame Reference Time is the
		//--- offset, in msec, from the Series reference
		//--- time.
		//list.frameReferenceTime = tag.c_str();
		this->SetFrameReferenceTime(tag.c_str());
	}
	else
	{
		//list.frameReferenceTime = "MODULE_INIT_NO_VALUE";
		this->SetFrameReferenceTime("no value found");
	}


	//---
	//--- SeriesTime
	if(fileReader.GetElementTM(0x0008,0x0031,tag,false) == EXIT_SUCCESS)
	{
		hourstr.clear();
		minutestr.clear();
		secondstr.clear();
		len = tag.length();
		if ( len >= 2 )
		{
			hourstr = tag.substr(0, 2);
		}
		else
		{
			hourstr = "00";
		}
		if ( len >= 4 )
		{
			minutestr = tag.substr(2, 2);
		}
		else
		{
			minutestr = "00";
		}
		if ( len >= 6 )
		{
			secondstr = tag.substr(4);
		}
		else
		{
			secondstr = "00";
		}
		tag.clear();
		tag = hourstr.c_str();
		tag += ":";
		tag += minutestr.c_str();
		tag += ":";
		tag += secondstr.c_str();
		//list.seriesReferenceTime = tag.c_str();
		this->SetSeriesTime(tag.c_str());
	}
	else
	{
		//list.seriesReferenceTime = "MODULE_INIT_NO_VALUE";
		this->SetSeriesTime("no value found");
	}


	//---
	//--- PatientWeight
	if(fileReader.GetElementDS<double>(0x0010,0x1030,1,&tagDouble,false) == EXIT_SUCCESS)
	{
		//--- Expect same format as RadionuclideHalfLife
		//list.weightUnits = "kg";
		this->SetWeightUnits("kg");
		this->SetPatientWeight(tagDouble);
	}
	else
	{
		this->SetPatientWeight(0.0);
		this->SetWeightUnits("no value found");
		//list.patientWeight = 0.0;
		//list.weightUnits = "";

	}


	//---
	//--- CalibrationFactor
	if(fileReader.GetElementDS<double>(0x7053,0x1009,1,
		&tagDouble,false) != EXIT_SUCCESS)
	{
		//list.calibrationFactor =  0.0 ;
		this->SetCalibrationFactor("0.0");
	}
	else
	{
		std::stringstream ss;
		std::string s;
		ss << tagDouble;
		s = ss.str();
		this->SetCalibrationFactor(s.c_str());
	}



	//// check.... did we get all params we need for computation?
	//if ( (parsingDICOM) &&
	//	(list.injectedDose != 0.0) &&
	//	(list.patientWeight != 0.0) &&
	//	(list.seriesReferenceTime.compare("MODULE_INIT_NO_VALUE") != 0) &&
	//	(list.injectionTime.compare("MODULE_INIT_NO_VALUE") != 0) &&
	//	(list.radionuclideHalfLife.compare("MODULE_INIT_NO_VALUE") != 0) )
	//{
	//	std::cout << "Input parameters okay..." << std::endl;
	//}
	//else
	//{
	//	std::cerr << "Missing some parameters..." << std::endl;
	//	std::cerr << "\tinjected Dose " << list.injectedDose << " should not be 0" << std::endl;
	//	std::cerr << "\tpatientWeight " << list.patientWeight << " should not be 0" << std::endl;
	//	std::cerr << "\tseriesReferenceTime " << list.seriesReferenceTime << " should not be MODULE_INIT_NO_VALUE" << std::endl;
	//	std::cerr << "\tinjectionTime " << list.injectionTime<< " should not be MODULE_INIT_NO_VALUE" << std::endl;
	//	std::cerr << "\tradionuclideHalfLife " << list.radionuclideHalfLife << " should not be MODULE_INIT_NO_VALUE" << std::endl;
	//	return EXIT_FAILURE;
	//}




	//// convert from input units.
	//if( list.radioactivityUnits.c_str() == NULL )
	//{
	//	std::cerr << "ComputeSUV: Got NULL radioactivity units. No computation done." << std::endl;
	//	return EXIT_FAILURE;
	//}
	//if( list.weightUnits.c_str() == NULL )
	//{
	//	std::cerr << "ComputeSUV: Got NULL weight units. No computation could be done." << std::endl;
	//	return EXIT_FAILURE;
	//}

	//double suvmax, suvmin, suvmean;

	//// make up a string with output to return
	//std::string outputLabelString = "OutputLabel = ";
	//std::string outputLabelValueString = "OutputLabelValue = ";
	//std::string outputSUVMaxString = "SUVMax = ";
	//std::string outputSUVMeanString = "SUVMean = ";
	//std::string outputSUVMinString = "SUVMin = ";

	//// --- find the max and min label in mask
	//vtkImageAccumulate *stataccum = vtkImageAccumulate::New();
	//stataccum->SetInput( voiVolume );
	//stataccum->Update();
	//int lo = static_cast<int>(stataccum->GetMin()[0]);
	//int hi = static_cast<int>(stataccum->GetMax()[0]);
	//stataccum->Delete();

	//std::string labelName;
	//int         NumberOfVOIs = 0;
	//for( int i = lo; i <= hi; i++ )
	//{
	//	std::stringstream ss;
	//	if( i == 0 )
	//	{
	//		// --- eliminate 0 (background) label.
	//		continue;
	//	}

	//	labelName.clear();
	//	labelName = MapLabelIDtoColorName(i, list.VOIVolumeColorTableFile);
	//	if( labelName.empty() )
	//	{
	//		labelName.clear();
	//		labelName = "unknown";
	//	}

	//	// --- get label name from labelID

	//	suvmax = 0.0;
	//	suvmean = 0.0;

	//	// create the binary volume of the label
	//	vtkImageThreshold *thresholder = vtkImageThreshold::New();
	//	thresholder->SetInput(voiVolume);
	//	thresholder->SetInValue(1);
	//	thresholder->SetOutValue(0);
	//	thresholder->ReplaceOutOn();
	//	thresholder->ThresholdBetween(i, i);
	//	thresholder->SetOutputScalarType(petVolume->GetScalarType() );
	//	thresholder->Update();

	//	// use vtk's statistics class with the binary labelmap as a stencil
	//	vtkImageToImageStencil *stencil = vtkImageToImageStencil::New();
	//	stencil->SetInput(thresholder->GetOutput() );
	//	stencil->ThresholdBetween(1, 1);

	//	vtkImageAccumulate *labelstat = vtkImageAccumulate::New();
	//	labelstat->SetInput(petVolume);
	//	labelstat->SetStencil(stencil->GetOutput() );
	//	labelstat->Update();

	//	stencil->Delete();

	//	// --- For how many labels was SUV computed?

	//	int voxNumber = labelstat->GetVoxelCount();
	//	if( voxNumber > 0 )
	//	{
	//		NumberOfVOIs++;

	//		double CPETmin = (labelstat->GetMin() )[0];
	//		double CPETmax = (labelstat->GetMax() )[0];
	//		double CPETmean = (labelstat->GetMean() )[0];

	//		// --- we want to use the following units as noted at file top:
	//		// --- CPET(t) -- tissue radioactivity in pixels-- kBq/mlunits
	//		// --- injectced dose-- MBq and
	//		// --- patient weight-- kg.
	//		// --- computed SUV should be in units g/ml
	//		double weight = list.patientWeight;
	//		double dose = list.injectedDose;

	//		// --- do some error checking and reporting.
	//		if( list.radioactivityUnits.c_str() == NULL )
	//		{
	//			std::cerr << "ComputeSUV: Got null radioactivityUnits." << std::endl;
	//			return EXIT_FAILURE;
	//		}
	//		if( dose == 0.0 )
	//		{
	//			std::cerr << "ComputeSUV: Got NULL dose!" << std::endl;
	//			return EXIT_FAILURE;
	//		}
	//		if( weight == 0.0 )
	//		{
	//			std::cerr << "ComputeSUV: got zero weight!" << std::endl;
	//			return EXIT_FAILURE;
	//		}

	//		double tissueConversionFactor = ConvertRadioactivityUnits(1, list.radioactivityUnits.c_str(), "kBq");
	//		dose  = ConvertRadioactivityUnits( dose, list.radioactivityUnits.c_str(), "MBq");
	//		dose = DecayCorrection(list, dose);
	//		weight = ConvertWeightUnits( weight, list.weightUnits.c_str(), "kg");

	//		// --- check a possible multiply by slope -- take intercept into account?
	//		if( dose == 0.0 )
	//		{
	//			// oops, weight by dose is infinity. make a ridiculous number.
	//			suvmin = 99999999999999999.;
	//			suvmax = 99999999999999999.;
	//			suvmean = 99999999999999999.;
	//			std::cerr << "Warning: got an injected dose of 0.0. Results of SUV computation not valid." << std::endl;
	//		}
	//		else
	//		{
	//			double weightByDose = weight / dose;
	//			suvmax = (CPETmax * tissueConversionFactor) * weightByDose;
	//			suvmin = (CPETmin * tissueConversionFactor ) * weightByDose;
	//			suvmean = (CPETmean * tissueConversionFactor) * weightByDose;
	//		}
	//		// --- append to output return string file
	//		std::stringstream outputStringStream;
	//		//      outputStringStream << list.patientName << ", " << list.studyDate << ", " << list.injectedDose  << ", "  << i << ", " << suvmax << ", " << suvmean << ", " << labelName.c_str() << ", " << ", " << ", " << ", " << std::endl;
	//		std::string postfixStr = ", ";
	//		if (i == hi)
	//		{
	//			postfixStr = "";
	//		}
	//		outputStringStream.str("");
	//		outputStringStream << labelName.c_str() << postfixStr;
	//		outputLabelString += outputStringStream.str();
	//		outputStringStream.str("");
	//		outputStringStream  << i << postfixStr;
	//		outputLabelValueString += outputStringStream.str();
	//		outputStringStream.str("");
	//		outputStringStream  << suvmax << postfixStr;
	//		outputSUVMaxString += outputStringStream.str();
	//		outputStringStream.str("");
	//		outputStringStream  << suvmean << postfixStr;
	//		outputSUVMeanString += outputStringStream.str();
	//		outputStringStream.str("");
	//		outputStringStream << suvmin << postfixStr;
	//		outputSUVMinString += outputStringStream.str();

	//		// --- write output CSV file

	//		// open file containing suvs and append to it.
	//		ofile.open( outputFile.c_str(), ios::out | ios::app );
	//		if( !ofile.is_open() )
	//		{
	//			// report error, clean up, and get out.
	//			std::cerr << "ERROR: cannot open nuclear medicine output csv parameter file '" << outputFile.c_str() << "', see return strings for values" << std::endl;
	//		}
	//		else
	//		{
	//			// --- for each value..
	//			// --- format looks like:
	//			// patientID, studyDate, dose, labelID, suvmin, suvmax, suvmean, labelName
	//			// ...
	//			ss.str("");
	//			ss << list.patientName << ", " << list.studyDate << ", " << list.injectedDose  << ", "  << i << ", " << suvmin << ", " << suvmax
	//				<< ", " << suvmean << ", " << labelName.c_str() << std::endl;
	//			ofile << ss.str();
	//			ofile.close();
	//			std::cout << "Wrote output for label " << labelName.c_str() << " to " << outputFile.c_str() << std::endl;
	//		}
	//	}


	//	thresholder->Delete();
	//	labelstat->Delete();
	//}
	//// --- write output return string file
	//std::stringstream ss;
	//ss << outputLabelString << std::endl;
	//ss << outputLabelValueString << std::endl;
	//ss << outputSUVMaxString << std::endl;
	//ss << outputSUVMeanString << std::endl;
	//ss << outputSUVMinString << std::endl;
	//std::string stringOutput = ss.str();
	//stringFile.open(list.SUVOutputStringFile.c_str());
	//if (!stringFile.is_open() )
	//{
	//	// report error, clean up
	//	std::cerr << "ERROR: cannot open nuclear medicine output string parameter file '" << list.SUVOutputStringFile.c_str() << "', output string was:\n" << stringOutput.c_str() << std::endl;
	//	return EXIT_FAILURE;
	//}
	//stringFile << stringOutput;
	//stringFile.close();
	//std::cout << "Wrote return string to " << list.SUVOutputStringFile.c_str() << ": " << std::endl << stringOutput.c_str() << std::endl;

	//reader1->Delete();
	//reader2->Delete();

	//return EXIT_SUCCESS;
	if(parsingDICOM == 1)
		return true;
	else
		return false;
}


//TEST CODE
//bool PETDICOMReader::readDicomTags()
//{
//	//For debug
//	std::ofstream fout("D:/debug.txt");
//
//	const unsigned int InputDimension = 3;
//	const unsigned int OutputDimension = 2;
//
//	typedef signed short PixelType;
//
//	typedef itk::Image< PixelType, InputDimension >	InputImageType;
//	typedef itk::ImageSeriesReader< InputImageType > ReaderType;
//	typedef itk::GDCMImageIO ImageIOType;
//	typedef itk::GDCMSeriesFileNames InputNamesGeneratorType;
//	
//	////////////////////////////////////////////////  
//	// 1) Read the input series
//
//	////Get the directory name from fileName
//	//fout<<fileName<<std::endl;
//	//std::string tempStr = fileName;
//	////std::size_t pos = tempStr.find_last_of("/");
//	std::string dirName = m_DirectoryName;//tempStr;//tempStr.substr(0,pos);
//
//	ImageIOType::Pointer gdcmIO = ImageIOType::New();
//	gdcmIO->SetMaxSizeLoadEntry(0xffff);
//	
//	InputNamesGeneratorType::Pointer inputNames = InputNamesGeneratorType::New();
//	inputNames->SetInputDirectory( dirName.c_str() );
//
//	const ReaderType::FileNamesContainer & filenames = inputNames->GetInputFileNames();
//
//	ReaderType::Pointer reader = ReaderType::New();
//
//	reader->SetImageIO( gdcmIO );
//	reader->SetFileNames( filenames );
//	try
//	{
//		reader->Update();
//	}
//	catch (itk::ExceptionObject &excp)
//	{
//		fout << "Exception thrown while reading the series" << std::endl;
//		fout << excp << std::endl;
//		return EXIT_FAILURE;
//	}
//
//	typedef itk::MetaDataDictionary   DictionaryType;
//	const  DictionaryType & dictionary = gdcmIO->GetMetaDataDictionary();
//
//
//	// Since we are interested only in the DICOM tags that can be expressed in
//	// strings, we declare a MetaDataObject suitable for managing strings.
//
//	typedef itk::MetaDataObject< std::string > MetaDataStringType;
//
//	// We instantiate the iterators that will make possible to walk through all the
//	// entries of the MetaDataDictionary.
//	DictionaryType::ConstIterator itr = dictionary.Begin();
//	DictionaryType::ConstIterator end = dictionary.End();
//
//	
//	std::string yearstr;
//	std::string monthstr;
//	std::string daystr;
//	std::string hourstr;
//	std::string minutestr;
//	std::string secondstr;
//	int len;
//
//	// Nuclear Medicine DICOM info:
///*
//    0054,0016  Radiopharmaceutical Information Sequence:
//    0018,1072  Radionuclide Start Time: 090748.000000
//    0018,1074  Radionuclide Total Dose: 370500000
//    0018,1075  Radionuclide Half Life: 6586.2
//    0018,1076  Radionuclide Positron Fraction: 0
//*/
//    std::string tagKey = "0018|1072";
//    std::string labelId;
//    std::string tagValue;
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      fout << labelId << " (" << tagKey << "): ";
//      if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        fout << tagValue;
//        }
//      else
//        {
//        fout << "(No Value Found in File)";
//        }
//      fout << std::endl;
//      }
//    else
//      {
//      fout << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//	//--- expect A string of characters of the format hhmmss.frac;
//	//---where hh contains hours (range "00" - "23"), mm contains minutes
//	//---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
//	//---contains a fractional part of a second as small as 1 millionth of a
//	//---second (range "000000" - "999999"). A 24 hour clock is assumed.
//	//---Midnight can be represented by only "0000" since "2400" would
//	//---violate the hour range. The string may be padded with trailing
//	//---spaces. Leading and embedded spaces are not allowed. One
//	//---or more of the components mm, ss, or frac may be unspecified
//	//---as long as every component to the right of an unspecified
//	//---component is also unspecified. If frac is unspecified the preceding "."
//	//---may not be included. Frac shall be held to six decimal places or
//	//---less to ensure its format conforms to the ANSI 
//	//---Examples -
//	//---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
//	//---2. "1010" represents a time of 10 hours, and 10 minutes.
//	//---3. "021" is an invalid value. 
//	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//	{
//		this->SetRadiopharmaceuticalStartTime ("no value found");
//	}
//	else
//	{
//		len = tagValue.length();
//		hourstr.clear();
//		minutestr.clear();
//		secondstr.clear();
//		if ( len >= 2 )
//		{
//			hourstr = tagValue.substr(0, 2);
//		}
//		else
//		{
//			hourstr = "00";
//		}
//		if ( len >= 4 )
//		{
//			minutestr = tagValue.substr(2, 2);
//		}
//		else
//		{
//			minutestr = "00";
//		}
//		if ( len >= 6 )
//		{
//			secondstr = tagValue.substr(4);
//		}
//		else
//		{
//			secondstr = "00";
//		}
//		tagValue.clear();
//		tagValue = hourstr.c_str();
//		tagValue += ":";
//		tagValue += minutestr.c_str();
//		tagValue += ":";
//		tagValue += secondstr.c_str();
//		this->SetRadiopharmaceuticalStartTime( tagValue.c_str() );
//	}
//
//	tagKey = "0018|1074";
//	tagValue.clear();
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	else
//	{
//		fout << "Trying to access inexistant DICOM tag." << std::endl;
//	}
//
//	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//	{
//		this->SetInjectedDose( 0.0 );
//	}
//	else
//	{
//		this->SetInjectedDose( atof ( tagValue.c_str() ) );
//	}
//
//
//	//---
//	//--- RadionuclideHalfLife
//	tagKey = "0018|1075";
//	tagValue.clear();
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	else
//	{
//		fout << "Trying to access inexistant DICOM tag." << std::endl;
//	}
//
//	//--- Expect a Decimal String
//	//--- A string of characters representing either
//	//--- a fixed point number or a floating point number.
//	//--- A fixed point number shall contain only the characters 0-9
//	//--- with an optional leading "+" or "-" and an optional "." to mark
//	//--- the decimal point. A floating point number shall be conveyed
//	//--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
//	//--- of the exponent. Decimal Strings may be padded with leading
//	//--- or trailing spaces. Embedded spaces are not allowed. 
//	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//	{
//		this->SetRadionuclideHalfLife( "no value found" );
//	}
//	else
//	{
//		this->SetRadionuclideHalfLife(  tagValue.c_str() );
//	}
//
//	//---
//	//---Radionuclide Positron Fraction
//	tagValue.clear();
//	tagKey ="0054|1001";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	else
//	{
//		fout << "Trying to access inexistant DICOM tag." << std::endl;
//	}
//
//
//	//--
//	//--- UNITS: something like BQML:
//	//--- CNTS, NONE, CM2, PCNT, CPS, BQML,
//	//--- MGMINML, UMOLMINML, MLMING, MLG,
//	//--- 1CM, UMOLML, PROPCNTS, PROPCPS,
//	//--- MLMINML, MLML, GML, STDDEV      
//	//---
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- I think these are piled together. MBq ml... search for all.
//		std::string units = tagValue.c_str();
//		if ( ( units.find ("BQML") != std::string::npos) ||
//			( units.find ("BQML") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("Bq");
//			this->SetTissueRadioactivityUnits ("Bq");
//		}
//		else if ( ( units.find ("MBq") != std::string::npos) ||
//			( units.find ("MBQ") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("MBq");
//			this->SetTissueRadioactivityUnits ("MBq");
//		}
//		else if ( (units.find ("kBq") != std::string::npos) ||
//			(units.find ("kBQ") != std::string::npos) ||
//			(units.find ("KBQ") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("kBq");
//			this->SetTissueRadioactivityUnits ("kBq");
//		}
//		else if ( (units.find ("mBq") != std::string::npos) ||
//			(units.find ("mBQ") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("mBq");
//			this->SetTissueRadioactivityUnits ("mBq");
//		}
//		else if ( (units.find ("uBq") != std::string::npos) ||
//			(units.find ("uBQ") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("uBq");
//			this->SetTissueRadioactivityUnits ("uBq");
//		}
//		else if ( (units.find ("Bq") != std::string::npos) ||
//			(units.find ("BQ") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("Bq");
//			this->SetTissueRadioactivityUnits ("Bq");
//		}
//		else if ( (units.find ("MCi") != std::string::npos) ||
//			( units.find ("MCI") != std::string::npos) )
//		{
//			this->SetDoseRadioactivityUnits ("MCi");
//			this->SetTissueRadioactivityUnits ("MCi");
//
//		}
//		else if ( (units.find ("kCi") != std::string::npos) ||
//			(units.find ("kCI") != std::string::npos)  ||
//			(units.find ("KCI") != std::string::npos) )                
//		{
//			this->SetDoseRadioactivityUnits ("kCi");
//			this->SetTissueRadioactivityUnits ("kCi");
//		}
//		else if ( (units.find ("mCi") != std::string::npos) ||
//			(units.find ("mCI") != std::string::npos) )                
//		{
//			this->SetDoseRadioactivityUnits ("mCi");
//			this->SetTissueRadioactivityUnits ("mCi");
//		}
//		else if ( (units.find ("uCi") != std::string::npos) ||
//			(units.find ("uCI") != std::string::npos) )                
//		{
//			this->SetDoseRadioactivityUnits ("uCi");
//			this->SetTissueRadioactivityUnits ("uCi");
//		}
//		else if ( (units.find ("Ci") != std::string::npos) ||
//			(units.find ("CI") != std::string::npos) )                
//		{
//			this->SetDoseRadioactivityUnits ("Ci");
//			this->SetTissueRadioactivityUnits ("Ci");
//		}
//		this->SetVolumeUnits ( "ml" );
//	}
//	else
//	{
//		//--- default values.
//		this->SetDoseRadioactivityUnits( "MBq" );
//		this->SetTissueRadioactivityUnits( "MBq" );
//		this->SetVolumeUnits ( "ml");        
//	}
//
//	//---
//	//--- DecayCorrection
//	//--- Possible values are:
//	//--- NONE = no decay correction
//	//--- START= acquisition start time
//	//--- ADMIN = radiopharmaceutical administration time
//	//--- Frame Reference Time  is the time that the pixel values in the Image occurred. 
//	//--- It's defined as the time offset, in msec, from the Series Reference Time.
//	//--- Series Reference Time is defined by the combination of:
//	//--- Series Date (0008,0021) and
//	//--- Series Time (0008,0031).      
//	//--- We don't pull these out now, but can if we have to.
//	tagValue.clear();
//	tagKey ="0054|1102";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	else
//	{
//		fout << "Trying to access inexistant DICOM tag." << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//---A string of characters with leading or trailing spaces (20H) being non-significant. 
//		this->SetDecayCorrection( tagValue.c_str() );
//	}
//	else
//	{
//		this->SetDecayCorrection( "no value found" );
//	}
//
//	//---
//	//--- StudyDate
//	//this->ClearStudyDate();
//	tagValue.clear();
//	tagKey="0008|0021";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	else
//	{
//		fout << "Trying to access inexistant DICOM tag." << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
//	{
//		//--- YYYYMMDD
//		yearstr.clear();
//		daystr.clear();
//		monthstr.clear();
//		len = tagValue.length();
//		if ( len >= 4 )
//		{
//			yearstr = tagValue.substr(0, 4);
//			this->SetStudyDateYear(atoi(yearstr.c_str() ));
//		}
//		else
//		{
//			yearstr = "????";
//			this->SetStudyDateYear(0);
//		}
//		if ( len >= 6 )
//		{
//			monthstr = tagValue.substr(4, 2);
//			this->SetStudyDateMonth( atoi ( monthstr.c_str() ) );
//		}
//		else
//		{
//			monthstr = "??";
//			this->SetStudyDateMonth( 0 );
//		}
//		if ( len >= 8 )
//		{
//			daystr = tagValue.substr (6, 2);
//			this->SetStudyDateDay( atoi ( daystr.c_str() ));
//		}
//		else
//		{
//			daystr = "??";
//			this->SetStudyDateDay(0);
//		}
//		tagValue.clear();
//		tagValue = yearstr.c_str();
//		tagValue += "/";
//		tagValue += monthstr.c_str();
//		tagValue += "/";
//		tagValue += daystr.c_str();
//		this->SetStudyDate ( tagValue.c_str() );
//	}
//	else
//	{
//		this->SetStudyDate ( "no value found" );
//	}
//
//	//---
//	//--- PatientName
//	tagValue.clear();
//	tagKey="0010|0010";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
//	{
//		this->SetPatientName ( tagValue.c_str() );
//	}
//	else
//	{
//		this->SetPatientName ( "no value found");
//	}
//
//	//---
//	//--- DecayFactor
//	tagValue.clear();
//	tagKey="0054|1321";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- have to parse this out. what we have is
//		//---A string of characters representing either a fixed point number or a
//		//--- floating point number. A fixed point number shall contain only the
//		//---characters 0-9 with an optional leading "+" or "-" and an optional "."
//		//---to mark the decimal point. A floating point number shall be conveyed
//		//---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
//		//---exponent. Decimal Strings may be padded with leading or trailing spaces.
//		//---Embedded spaces are not allowed. or maybe atof does it already...
//		this->SetDecayFactor(  tagValue.c_str()  );
//	}
//	else
//	{
//		this->SetDecayFactor( "no value found" );
//	}
//
//
//	//---
//	//--- FrameReferenceTime
//	tagValue.clear();
//	tagKey="0054|1300";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- The time that the pixel values in the image
//		//--- occurred. Frame Reference Time is the
//		//--- offset, in msec, from the Series reference
//		//--- time.
//		this->SetFrameReferenceTime( tagValue.c_str() );
//	}
//	else
//	{
//		this->SetFrameReferenceTime( "no value found" );
//	}
//
//
//	//---
//	//--- SeriesTime
//	tagValue.clear();
//	tagKey="0008|0031";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		hourstr.clear();
//		minutestr.clear();
//		secondstr.clear();
//		len = tagValue.length();
//		if ( len >= 2 )
//		{
//			hourstr = tagValue.substr(0, 2);
//		}
//		else
//		{
//			hourstr = "00";
//		}
//		if ( len >= 4 )
//		{
//			minutestr = tagValue.substr(2, 2);
//		}
//		else
//		{
//			minutestr = "00";
//		}
//		if ( len >= 6 )
//		{
//			secondstr = tagValue.substr(4);
//		}
//		else
//		{
//			secondstr = "00";
//		}
//		tagValue.clear();
//		tagValue = hourstr.c_str();
//		tagValue += ":";
//		tagValue += minutestr.c_str();
//		tagValue += ":";
//		tagValue += secondstr.c_str();
//		this->SetSeriesTime( tagValue.c_str() );
//	}
//	else
//	{
//		this->SetSeriesTime( "no value found");
//	}
//
//
//	//---
//	//--- PatientWeight
//	tagValue.clear();
//	tagKey="0010|1030";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- Expect same format as RadionuclideHalfLife
//		this->SetPatientWeight( atof ( tagValue.c_str() ) );
//		this->SetWeightUnits ( "kg" );
//	}
//	else
//	{
//		this->SetPatientWeight( 0.0 );
//		this->SetWeightUnits ( "" );
//	}
//
//
//	//---
//	//--- CalibrationFactor
//	tagValue.clear();
//	tagKey="7053|1009";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- converts counts to Bq/cc. If Units = BQML then CalibrationFactor =1 
//		//--- I think we expect the same format as RadiopharmaceuticalStartTime
//		this->SetCalibrationFactor(  tagValue.c_str() );
//	}
//	else
//	{
//		this->SetCalibrationFactor( "no value found" );
//	}
//
//
//	//---
//	//--- PhilipsSUVFactor
//	tagValue.clear();
//	tagKey="7053|1000";
//	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//	{
//		fout << labelId << " (" << tagKey << "): ";
//		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
//		{
//			fout << tagValue;
//		}
//		else
//		{
//			fout << "(No Value Found in File)";
//		}
//		fout << std::endl;
//	}
//
//	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//	{
//		//--- I think we expect the same format as RadiopharmaceuticalStartTime
//		this->SetPhilipsSUVFactor(  tagValue.c_str() );
//	}
//	else
//	{
//		this->SetPhilipsSUVFactor( "no value found" );
//	}
//
//	// check.... did we get all params we need for computation?
//	if ( (this->GetInjectedDose() != 0.0) &&
//		(this->GetPatientWeight() != 0.0) &&
//		(this->GetSeriesTime() != NULL) &&
//		(this->GetRadiopharmaceuticalStartTime() != NULL) &&
//		(this->GetRadionuclideHalfLife() != NULL) )
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//
//}

//bool PETDICOMReader::readDicomTags()
//{
//	// read the DICOM dir to get the radiological data
//
//	typedef short PixelValueType;
//	typedef itk::Image< PixelValueType, 3 > VolumeType;
//	typedef itk::ImageSeriesReader< VolumeType > VolumeReaderType;
//	typedef itk::Image< PixelValueType, 2 > SliceType;
//	typedef itk::ImageFileReader< SliceType > SliceReaderType;
//	typedef itk::GDCMImageIO ImageIOType;
//	typedef itk::GDCMSeriesFileNames InputNamesGeneratorType;
//	typedef itk::VectorImage< PixelValueType, 3 > NRRDImageType;
//
//	//std::string tempStr = fileName;
//	////std::size_t pos = tempStr.find_last_of("/");
//	//std::string dirName = tempStr;//tempStr.substr(0,pos);
//	//m_DirectoryName = dirName;
//
//	InputNamesGeneratorType::Pointer inputNames = InputNamesGeneratorType::New();
//	inputNames->SetUseSeriesDetails(true);
//	inputNames->SetDirectory(m_DirectoryName);
//	itk::SerieUIDContainer seriesUIDs = inputNames->GetSeriesUIDs();
//
//	const VolumeReaderType::FileNamesContainer & filenames = inputNames->GetFileNames(seriesUIDs[0]);
//
//	std::string tag;
//	std::string yearstr;
//	std::string monthstr;
//	std::string daystr;
//	std::string hourstr;
//	std::string minutestr;
//	std::string secondstr;
//	int len;
//	short tagShort;
//	double tagDouble;
//
//	int parsingDICOM = 1;
//	itk::DCMTKFileReader fileReader;
//	fileReader.SetFileName(filenames[0]);
//	fileReader.LoadFile();
//
//	itk::DCMTKSequence seq;
//
//	if(fileReader.GetElementSQ(0x0054,0x0016,seq,false) == EXIT_SUCCESS)
//	{
//		//parsingDICOM = 1;
//		//      for(int i = 0; i < seq.card(); ++i)
//		//        {
//		//        itk::DCMTKSequence sqItem;
//		//        seq.GetSequence(i,sqItem);
//		//---
//		//--- Radiopharmaceutical Start Time
//		//        sqItem.GetElementTM(0x0018,0x1072,tag);
//		seq.GetElementTM(0x0018,0x1072,tag);
//		//--- expect A string of characters of the format hhmmss.frac;
//		//---where hh contains hours (range "00" - "23"), mm contains minutes
//		//---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
//		//---contains a fractional part of a second as small as 1 millionth of a
//		//---second (range "000000" - "999999"). A 24 hour clock is assumed.
//		//---Midnight can be represented by only "0000" since "2400" would
//		//---violate the hour range. The string may be padded with trailing
//		//---spaces. Leading and embedded spaces are not allowed. One
//		//---or more of the components mm, ss, or frac may be unspecified
//		//---as long as every component to the right of an unspecified
//		//---component is also unspecified. If frac is unspecified the preceding "."
//		//---may not be included. Frac shall be held to six decimal places or
//		//---less to ensure its format conforms to the ANSI
//		//---Examples -
//		//---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
//		//---2. "1010" represents a time of 10 hours, and 10 minutes.
//		//---3. "021" is an invalid value.
//		if ( tag.c_str() == NULL || *(tag.c_str()) == '\0' )
//		{
//			this->SetRadiopharmaceuticalStartTime ("no value found");
//		}
//		else
//		{
//			len = tag.length();
//			hourstr.clear();
//			minutestr.clear();
//			secondstr.clear();
//			if ( len >= 2 )
//			{
//				hourstr = tag.substr(0, 2);
//			}
//			else
//			{
//				hourstr = "00";
//			}
//			if ( len >= 4 )
//			{
//				minutestr = tag.substr(2, 2);
//			}
//			else
//			{
//				minutestr = "00";
//			}
//			if ( len >= 6 )
//			{
//				secondstr = tag.substr(4);
//			}
//			else
//			{
//				secondstr = "00";
//			}
//			tag.clear();
//			tag = hourstr.c_str();
//			tag += ":";
//			tag += minutestr.c_str();
//			tag += ":";
//			tag += secondstr.c_str();
//			//list.injectionTime = tag.c_str();
//			this->SetRadiopharmaceuticalStartTime(tag.c_str());
//		}
//
//		//---
//		//--- Radionuclide Total Dose
//		//        if(sqItem.GetElementDS(0x0018,0x1074,1,&list.injectedDose,false) != EXIT_SUCCESS)
//		if(seq.GetElementDS<double>(0x0018,0x1074,1,&tagDouble,false) != EXIT_SUCCESS)
//		{
//			//list.injectedDose = 0.0;
//			this->SetInjectedDose(0.0);
//		}
//		else
//			this->SetInjectedDose(tagDouble);
//
//		//---
//		//--- RadionuclideHalfLife
//		//--- Expect a Decimal String
//		//--- A string of characters representing either
//		//--- a fixed point number or a floating point number.
//		//--- A fixed point number shall contain only the characters 0-9
//		//--- with an optional leading "+" or "-" and an optional "." to mark
//		//--- the decimal point. A floating point number shall be conveyed
//		//--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
//		//--- of the exponent. Decimal Strings may be padded with leading
//		//--- or trailing spaces. Embedded spaces are not allowed.
//		//        if(sqItem.GetElementCS(0x0018,0x1075,list.radionuclideHalfLife,false) != EXIT_SUCCESS)
//		if(seq.GetElementDS(0x0018,0x1075,tag,false) != EXIT_SUCCESS)
//		{
//			//list.radionuclideHalfLife = "MODULE_INIT_NO_VALUE";
//			this->SetRadionuclideHalfLife("no value found");
//		}
//		else
//			this->SetRadionuclideHalfLife(tag.c_str());
//		//---
//		//---Radionuclide Positron Fraction
//		//--- not currently using this one?
//		std::string radioNuclidePositronFraction;
//		//        if(sqItem.GetElementCS(0x0018,0x1075,radioNuclidePositronFraction,false) != EXIT_SUCCESS)
//		if(seq.GetElementDS(0x0018,0x1075,radioNuclidePositronFraction,false) != EXIT_SUCCESS)
//		{
//			radioNuclidePositronFraction = "MODULE_INIT_NO_VALUE";
//		}
//	}
//
//		//--
//		//--- UNITS: something like BQML:
//		//--- CNTS, NONE, CM2, PCNT, CPS, BQML,
//		//--- MGMINML, UMOLMINML, MLMING, MLG,
//		//--- 1CM, UMOLML, PROPCNTS, PROPCPS,
//		//--- MLMINML, MLML, GML, STDDEV
//		//---
//
//		if(fileReader.GetElementCS(0x0054,0x1001,tag,false) == EXIT_SUCCESS)
//		{
//			//--- I think these are piled together. MBq ml... search for all.
//			std::string units = tag.c_str();
//			if ( ( units.find ("BQML") != std::string::npos) ||
//				( units.find ("BQML") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("Bq");
//				//list.radioactivityUnits= "Bq";
//				//list.tissueRadioactivityUnits = "Bq";
//			}
//			else if ( ( units.find ("MBq") != std::string::npos) ||
//				( units.find ("MBQ") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("MBq");
//				//list.radioactivityUnits = "MBq";
//				//list.tissueRadioactivityUnits = "MBq";
//			}
//			else if ( (units.find ("kBq") != std::string::npos) ||
//				(units.find ("kBQ") != std::string::npos) ||
//				(units.find ("KBQ") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("kBq");
//				//list.radioactivityUnits = "kBq";
//				//list.tissueRadioactivityUnits = "kBq";
//			}
//			else if ( (units.find ("mBq") != std::string::npos) ||
//				(units.find ("mBQ") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("mBq");
//				//list.radioactivityUnits = "mBq";
//				//list.tissueRadioactivityUnits = "mBq";
//			}
//			else if ( (units.find ("uBq") != std::string::npos) ||
//				(units.find ("uBQ") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("uBq");
//				//list.radioactivityUnits = "uBq";
//				//list.tissueRadioactivityUnits = "uBq";
//			}
//			else if ( (units.find ("Bq") != std::string::npos) ||
//				(units.find ("BQ") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("Bq");
//				//list.radioactivityUnits = "Bq";
//				//list.tissueRadioactivityUnits = "Bq";
//			}
//			else if ( (units.find ("MCi") != std::string::npos) ||
//				( units.find ("MCI") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("MCi");
//				//list.radioactivityUnits = "MCi";
//				//list.tissueRadioactivityUnits = "MCi";
//			}
//			else if ( (units.find ("kCi") != std::string::npos) ||
//				(units.find ("kCI") != std::string::npos)  ||
//				(units.find ("KCI") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("kCi");
//				//list.radioactivityUnits = "kCi";
//				//list.tissueRadioactivityUnits = "kCi";
//			}
//			else if ( (units.find ("mCi") != std::string::npos) ||
//				(units.find ("mCI") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("mCi");
//				//list.radioactivityUnits = "mCi";
//				//list.tissueRadioactivityUnits = "mCi";
//			}
//			else if ( (units.find ("uCi") != std::string::npos) ||
//				(units.find ("uCI") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("uCi");
//				//list.radioactivityUnits = "uCi";
//				//list.tissueRadioactivityUnits = "uCi";
//			}
//			else if ( (units.find ("Ci") != std::string::npos) ||
//				(units.find ("CI") != std::string::npos) )
//			{
//				this->SetDoseRadioactivityUnits("Ci");
//				//list.radioactivityUnits = "Ci";
//				//list.tissueRadioactivityUnits = "Ci";
//			}
//			//list.volumeUnits = "ml";
//			this->SetVolumeUnits("ml");
//		}
//		else
//		{
//			//--- default values.
//			this->SetDoseRadioactivityUnits("MBq");
//			this->SetVolumeUnits("ml");
//			//list.radioactivityUnits = "MBq";
//			//list.tissueRadioactivityUnits = "MBq";
//			//list.volumeUnits = "ml";
//		}
//
//
//		//---
//		//--- DecayCorrection
//		//--- Possible values are:
//		//--- NONE = no decay correction
//		//--- START= acquisition start time
//		//--- ADMIN = radiopharmaceutical administration time
//		//--- Frame Reference Time  is the time that the pixel values in the Image occurred.
//		//--- It's defined as the time offset, in msec, from the Series Reference Time.
//		//--- Series Reference Time is defined by the combination of:
//		//--- Series Date (0008,0021) and
//		//--- Series Time (0008,0031).
//		//--- We don't pull these out now, but can if we have to.
//		if(fileReader.GetElementCS(0x0054,0x1102,tag,false) == EXIT_SUCCESS)
//		{
//			//---A string of characters with leading or trailing spaces (20H) being non-significant.
//			//list.decayCorrection = tag.c_str();
//			this->SetDecayCorrection(tag.c_str());
//		}
//		else
//		{
//			//list.decayCorrection = "MODULE_INIT_NO_VALUE";
//			this->SetDecayCorrection("no value found");
//		}
//
//		//---
//		//--- StudyDate
//		//if(fileReader.GetElementDA(0x0008,0x0021,tag,false) == EXIT_SUCCESS)
//		if(fileReader.GetElementDA(0x0008,0x0020,tag,false) == EXIT_SUCCESS)
//		{
//			////--- YYYYMMDD
//			//yearstr.clear();
//			//daystr.clear();
//			//monthstr.clear();
//			//len = tag.length();
//			//if ( len >= 4 )
//			//{
//			//	yearstr = tag.substr(0, 4);
//			//	// this->Year = atoi(yearstr.c_str() );
//			//}
//			//else
//			//{
//			//	yearstr = "????";
//			//	// this->Year = 0;
//			//}
//			//if ( len >= 6 )
//			//{
//			//	monthstr = tag.substr(4, 2);
//			//	// this->Month = atoi ( monthstr.c_str() );
//			//}
//			//else
//			//{
//			//	monthstr = "??";
//			//	// this->Month = 0;
//			//}
//			//if ( len >= 8 )
//			//{
//			//	daystr = tag.substr (6, 2);
//			//	//            this->Day = atoi ( daystr.c_str() );
//			//}
//			//else
//			//{
//			//	daystr = "??";
//			//	//            this->Day = 0;
//			//}
//			//tag.clear();
//			//tag = yearstr.c_str();
//			//tag += "/";
//			//tag += monthstr.c_str();
//			//tag += "/";
//			//tag += daystr.c_str();
//			//list.studyDate = tag.c_str();
//			this->SetStudyDate(tag.c_str());
//		}
//		else
//		{
//			//list.studyDate = "MODULE_INIT_NO_VALUE";
//			this->SetStudyDate("no value found");
//		}
//
//		//---
//		//--- PatientName
//		if(fileReader.GetElementPN(0x0010,0x0010,tag,false) == EXIT_SUCCESS)
//		{
//			//list.patientName = tag.c_str();
//			this->SetPatientName(tag.c_str());
//		}
//		else
//		{
//			//list.patientName = "MODULE_INIT_NO_VALUE";
//			this->SetPatientName("no value found");
//		}
//
//		//---
//		//--- DecayFactor
//		if(fileReader.GetElementDS(0x0054,0x1321,tag,false) == EXIT_SUCCESS)
//		{
//			//--- have to parse this out. what we have is
//			//---A string of characters representing either a fixed point number or a
//			//--- floating point number. A fixed point number shall contain only the
//			//---characters 0-9 with an optional leading "+" or "-" and an optional "."
//			//---to mark the decimal point. A floating point number shall be conveyed
//			//---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
//			//---exponent. Decimal Strings may be padded with leading or trailing spaces.
//			//---Embedded spaces are not allowed. or maybe atof does it already...
//			//list.decayFactor =  tag.c_str() ;
//			this->SetDecayFactor(tag.c_str());
//		}
//		else
//		{
//			//list.decayFactor =  "MODULE_INIT_NO_VALUE" ;
//			this->SetDecayFactor("no value found");
//		}
//
//
//		//---
//		//--- FrameReferenceTime
//		if(fileReader.GetElementDS(0x0054,0x1300,tag,false) == EXIT_SUCCESS)
//		{
//			//--- The time that the pixel values in the image
//			//--- occurred. Frame Reference Time is the
//			//--- offset, in msec, from the Series reference
//			//--- time.
//			//list.frameReferenceTime = tag.c_str();
//			this->SetFrameReferenceTime(tag.c_str());
//		}
//		else
//		{
//			//list.frameReferenceTime = "MODULE_INIT_NO_VALUE";
//			this->SetFrameReferenceTime("no value found");
//		}
//
//
//		//---
//		//--- SeriesTime
//		if(fileReader.GetElementTM(0x0008,0x0031,tag,false) == EXIT_SUCCESS)
//		{
//			//hourstr.clear();
//			//minutestr.clear();
//			//secondstr.clear();
//			//len = tag.length();
//			//if ( len >= 2 )
//			//{
//			//	hourstr = tag.substr(0, 2);
//			//}
//			//else
//			//{
//			//	hourstr = "00";
//			//}
//			//if ( len >= 4 )
//			//{
//			//	minutestr = tag.substr(2, 2);
//			//}
//			//else
//			//{
//			//	minutestr = "00";
//			//}
//			//if ( len >= 6 )
//			//{
//			//	secondstr = tag.substr(4);
//			//}
//			//else
//			//{
//			//	secondstr = "00";
//			//}
//			//tag.clear();
//			//tag = hourstr.c_str();
//			//tag += ":";
//			//tag += minutestr.c_str();
//			//tag += ":";
//			//tag += secondstr.c_str();
//			//list.seriesReferenceTime = tag.c_str();
//			this->SetSeriesTime(tag.c_str());
//		}
//		else
//		{
//			//list.seriesReferenceTime = "MODULE_INIT_NO_VALUE";
//			this->SetSeriesTime("no value found");
//		}
//
//
//		//---
//		//--- PatientWeight
//		if(fileReader.GetElementDS<double>(0x0010,0x1030,1,&tagDouble,false) == EXIT_SUCCESS)
//		{
//			//--- Expect same format as RadionuclideHalfLife
//			//list.weightUnits = "kg";
//			this->SetWeightUnits("kg");
//			this->SetPatientWeight(tagDouble);
//		}
//		else
//		{
//			this->SetPatientWeight(0.0);
//			this->SetWeightUnits("no value found");
//			//list.patientWeight = 0.0;
//			//list.weightUnits = "";
//
//		}
//
//
//		//---
//		//--- CalibrationFactor
//		if(fileReader.GetElementDS<double>(0x7053,0x1009,1,
//			&tagDouble,false) != EXIT_SUCCESS)
//		{
//			//list.calibrationFactor =  0.0 ;
//			this->SetCalibrationFactor("0.0");
//		}
//		else
//		{
//			std::stringstream ss;
//			std::string s;
//			ss << tagDouble;
//			s = ss.str();
//			this->SetCalibrationFactor(s.c_str());
//		}
//	
//
//
//	//// check.... did we get all params we need for computation?
//	//if ( (parsingDICOM) &&
//	//	(list.injectedDose != 0.0) &&
//	//	(list.patientWeight != 0.0) &&
//	//	(list.seriesReferenceTime.compare("MODULE_INIT_NO_VALUE") != 0) &&
//	//	(list.injectionTime.compare("MODULE_INIT_NO_VALUE") != 0) &&
//	//	(list.radionuclideHalfLife.compare("MODULE_INIT_NO_VALUE") != 0) )
//	//{
//	//	std::cout << "Input parameters okay..." << std::endl;
//	//}
//	//else
//	//{
//	//	std::cerr << "Missing some parameters..." << std::endl;
//	//	std::cerr << "\tinjected Dose " << list.injectedDose << " should not be 0" << std::endl;
//	//	std::cerr << "\tpatientWeight " << list.patientWeight << " should not be 0" << std::endl;
//	//	std::cerr << "\tseriesReferenceTime " << list.seriesReferenceTime << " should not be MODULE_INIT_NO_VALUE" << std::endl;
//	//	std::cerr << "\tinjectionTime " << list.injectionTime<< " should not be MODULE_INIT_NO_VALUE" << std::endl;
//	//	std::cerr << "\tradionuclideHalfLife " << list.radionuclideHalfLife << " should not be MODULE_INIT_NO_VALUE" << std::endl;
//	//	return EXIT_FAILURE;
//	//}
//
//
//
//
//	//// convert from input units.
//	//if( list.radioactivityUnits.c_str() == NULL )
//	//{
//	//	std::cerr << "ComputeSUV: Got NULL radioactivity units. No computation done." << std::endl;
//	//	return EXIT_FAILURE;
//	//}
//	//if( list.weightUnits.c_str() == NULL )
//	//{
//	//	std::cerr << "ComputeSUV: Got NULL weight units. No computation could be done." << std::endl;
//	//	return EXIT_FAILURE;
//	//}
//
//	//double suvmax, suvmin, suvmean;
//
//	//// make up a string with output to return
//	//std::string outputLabelString = "OutputLabel = ";
//	//std::string outputLabelValueString = "OutputLabelValue = ";
//	//std::string outputSUVMaxString = "SUVMax = ";
//	//std::string outputSUVMeanString = "SUVMean = ";
//	//std::string outputSUVMinString = "SUVMin = ";
//
//	//// --- find the max and min label in mask
//	//vtkImageAccumulate *stataccum = vtkImageAccumulate::New();
//	//stataccum->SetInput( voiVolume );
//	//stataccum->Update();
//	//int lo = static_cast<int>(stataccum->GetMin()[0]);
//	//int hi = static_cast<int>(stataccum->GetMax()[0]);
//	//stataccum->Delete();
//
//	//std::string labelName;
//	//int         NumberOfVOIs = 0;
//	//for( int i = lo; i <= hi; i++ )
//	//{
//	//	std::stringstream ss;
//	//	if( i == 0 )
//	//	{
//	//		// --- eliminate 0 (background) label.
//	//		continue;
//	//	}
//
//	//	labelName.clear();
//	//	labelName = MapLabelIDtoColorName(i, list.VOIVolumeColorTableFile);
//	//	if( labelName.empty() )
//	//	{
//	//		labelName.clear();
//	//		labelName = "unknown";
//	//	}
//
//	//	// --- get label name from labelID
//
//	//	suvmax = 0.0;
//	//	suvmean = 0.0;
//
//	//	// create the binary volume of the label
//	//	vtkImageThreshold *thresholder = vtkImageThreshold::New();
//	//	thresholder->SetInput(voiVolume);
//	//	thresholder->SetInValue(1);
//	//	thresholder->SetOutValue(0);
//	//	thresholder->ReplaceOutOn();
//	//	thresholder->ThresholdBetween(i, i);
//	//	thresholder->SetOutputScalarType(petVolume->GetScalarType() );
//	//	thresholder->Update();
//
//	//	// use vtk's statistics class with the binary labelmap as a stencil
//	//	vtkImageToImageStencil *stencil = vtkImageToImageStencil::New();
//	//	stencil->SetInput(thresholder->GetOutput() );
//	//	stencil->ThresholdBetween(1, 1);
//
//	//	vtkImageAccumulate *labelstat = vtkImageAccumulate::New();
//	//	labelstat->SetInput(petVolume);
//	//	labelstat->SetStencil(stencil->GetOutput() );
//	//	labelstat->Update();
//
//	//	stencil->Delete();
//
//	//	// --- For how many labels was SUV computed?
//
//	//	int voxNumber = labelstat->GetVoxelCount();
//	//	if( voxNumber > 0 )
//	//	{
//	//		NumberOfVOIs++;
//
//	//		double CPETmin = (labelstat->GetMin() )[0];
//	//		double CPETmax = (labelstat->GetMax() )[0];
//	//		double CPETmean = (labelstat->GetMean() )[0];
//
//	//		// --- we want to use the following units as noted at file top:
//	//		// --- CPET(t) -- tissue radioactivity in pixels-- kBq/mlunits
//	//		// --- injectced dose-- MBq and
//	//		// --- patient weight-- kg.
//	//		// --- computed SUV should be in units g/ml
//	//		double weight = list.patientWeight;
//	//		double dose = list.injectedDose;
//
//	//		// --- do some error checking and reporting.
//	//		if( list.radioactivityUnits.c_str() == NULL )
//	//		{
//	//			std::cerr << "ComputeSUV: Got null radioactivityUnits." << std::endl;
//	//			return EXIT_FAILURE;
//	//		}
//	//		if( dose == 0.0 )
//	//		{
//	//			std::cerr << "ComputeSUV: Got NULL dose!" << std::endl;
//	//			return EXIT_FAILURE;
//	//		}
//	//		if( weight == 0.0 )
//	//		{
//	//			std::cerr << "ComputeSUV: got zero weight!" << std::endl;
//	//			return EXIT_FAILURE;
//	//		}
//
//	//		double tissueConversionFactor = ConvertRadioactivityUnits(1, list.radioactivityUnits.c_str(), "kBq");
//	//		dose  = ConvertRadioactivityUnits( dose, list.radioactivityUnits.c_str(), "MBq");
//	//		dose = DecayCorrection(list, dose);
//	//		weight = ConvertWeightUnits( weight, list.weightUnits.c_str(), "kg");
//
//	//		// --- check a possible multiply by slope -- take intercept into account?
//	//		if( dose == 0.0 )
//	//		{
//	//			// oops, weight by dose is infinity. make a ridiculous number.
//	//			suvmin = 99999999999999999.;
//	//			suvmax = 99999999999999999.;
//	//			suvmean = 99999999999999999.;
//	//			std::cerr << "Warning: got an injected dose of 0.0. Results of SUV computation not valid." << std::endl;
//	//		}
//	//		else
//	//		{
//	//			double weightByDose = weight / dose;
//	//			suvmax = (CPETmax * tissueConversionFactor) * weightByDose;
//	//			suvmin = (CPETmin * tissueConversionFactor ) * weightByDose;
//	//			suvmean = (CPETmean * tissueConversionFactor) * weightByDose;
//	//		}
//	//		// --- append to output return string file
//	//		std::stringstream outputStringStream;
//	//		//      outputStringStream << list.patientName << ", " << list.studyDate << ", " << list.injectedDose  << ", "  << i << ", " << suvmax << ", " << suvmean << ", " << labelName.c_str() << ", " << ", " << ", " << ", " << std::endl;
//	//		std::string postfixStr = ", ";
//	//		if (i == hi)
//	//		{
//	//			postfixStr = "";
//	//		}
//	//		outputStringStream.str("");
//	//		outputStringStream << labelName.c_str() << postfixStr;
//	//		outputLabelString += outputStringStream.str();
//	//		outputStringStream.str("");
//	//		outputStringStream  << i << postfixStr;
//	//		outputLabelValueString += outputStringStream.str();
//	//		outputStringStream.str("");
//	//		outputStringStream  << suvmax << postfixStr;
//	//		outputSUVMaxString += outputStringStream.str();
//	//		outputStringStream.str("");
//	//		outputStringStream  << suvmean << postfixStr;
//	//		outputSUVMeanString += outputStringStream.str();
//	//		outputStringStream.str("");
//	//		outputStringStream << suvmin << postfixStr;
//	//		outputSUVMinString += outputStringStream.str();
//
//	//		// --- write output CSV file
//
//	//		// open file containing suvs and append to it.
//	//		ofile.open( outputFile.c_str(), ios::out | ios::app );
//	//		if( !ofile.is_open() )
//	//		{
//	//			// report error, clean up, and get out.
//	//			std::cerr << "ERROR: cannot open nuclear medicine output csv parameter file '" << outputFile.c_str() << "', see return strings for values" << std::endl;
//	//		}
//	//		else
//	//		{
//	//			// --- for each value..
//	//			// --- format looks like:
//	//			// patientID, studyDate, dose, labelID, suvmin, suvmax, suvmean, labelName
//	//			// ...
//	//			ss.str("");
//	//			ss << list.patientName << ", " << list.studyDate << ", " << list.injectedDose  << ", "  << i << ", " << suvmin << ", " << suvmax
//	//				<< ", " << suvmean << ", " << labelName.c_str() << std::endl;
//	//			ofile << ss.str();
//	//			ofile.close();
//	//			std::cout << "Wrote output for label " << labelName.c_str() << " to " << outputFile.c_str() << std::endl;
//	//		}
//	//	}
//
//
//	//	thresholder->Delete();
//	//	labelstat->Delete();
//	//}
//	//// --- write output return string file
//	//std::stringstream ss;
//	//ss << outputLabelString << std::endl;
//	//ss << outputLabelValueString << std::endl;
//	//ss << outputSUVMaxString << std::endl;
//	//ss << outputSUVMeanString << std::endl;
//	//ss << outputSUVMinString << std::endl;
//	//std::string stringOutput = ss.str();
//	//stringFile.open(list.SUVOutputStringFile.c_str());
//	//if (!stringFile.is_open() )
//	//{
//	//	// report error, clean up
//	//	std::cerr << "ERROR: cannot open nuclear medicine output string parameter file '" << list.SUVOutputStringFile.c_str() << "', output string was:\n" << stringOutput.c_str() << std::endl;
//	//	return EXIT_FAILURE;
//	//}
//	//stringFile << stringOutput;
//	//stringFile.close();
//	//std::cout << "Wrote return string to " << list.SUVOutputStringFile.c_str() << ": " << std::endl << stringOutput.c_str() << std::endl;
//
//	//reader1->Delete();
//	//reader2->Delete();
//
//	//return EXIT_SUCCESS;
//if(parsingDICOM == 1)
//	return true;
//else
//	return false;
//}

//bool PETDICOMReader::readDicomTags1()
//{
//	const unsigned int InputDimension = 3;
//	const unsigned int OutputDimension = 2;
//
//	typedef signed short PixelType;
//
//	typedef itk::Image< PixelType, InputDimension >	InputImageType;
//	typedef itk::ImageSeriesReader< InputImageType > ReaderType;
//	typedef itk::GDCMImageIO ImageIOType;
//	typedef itk::GDCMSeriesFileNames InputNamesGeneratorType;
//
//	itk::GDCMSeriesFileNames::Pointer     m_FileNames;
//	itk::GDCMImageIO::Pointer             m_ImageIO;
//
//	ReaderType::Pointer        m_ImageSeriesReader;
//
//	// Create the DICOM GDCM file reader
//	m_FileNames = itk::GDCMSeriesFileNames::New();
//	m_FileNames->SetRecursive(false);
//	//m_FileNames->SetGlobalWarningDisplay(this->GetGlobalWarningDisplay());
//
//	// add more criteria to distinguish between different series
//	m_FileNames->SetUseSeriesDetails( true );
//
//	m_ImageIO = itk::GDCMImageIO::New();
//	//m_ImageIO->SetGlobalWarningDisplay(this->GetGlobalWarningDisplay());
//	m_ImageIO->SetMaxSizeLoadEntry(0xffff);
//	m_ImageSeriesReader = ReaderType::New();
//	m_ImageSeriesReader->SetImageIO( m_ImageIO );
//
//	//m_ValidDicomDirectory = false;
//	//m_DicomDataRead = false;
//
//
//	////For debug
//	//std::ofstream fout("D:/rkhare/temp.txt");
//
//
//	//
//	//////////////////////////////////////////////////  
//	//// 1) Read the input series
//
//	//Get the directory name from fileName
//	//fout<<fileName<<std::endl;
//	//std::string tempStr = fileName;
//	//std::size_t pos = tempStr.find_last_of("/");
//	std::string dirName = m_DirectoryName;//tempStr;//tempStr.substr(0,pos);
//	//m_DirectoryName = dirName;
//
//	//ImageIOType::Pointer gdcmIO = ImageIOType::New();
//	//gdcmIO->SetMaxSizeLoadEntry(0xffff);
//	//
//	//InputNamesGeneratorType::Pointer inputNames = InputNamesGeneratorType::New();
//	//inputNames->SetInputDirectory( dirName.c_str() );
//
//	//const ReaderType::FileNamesContainer & filenames = inputNames->GetInputFileNames();
//
//	//ReaderType::Pointer reader = ReaderType::New();
//
//	//reader->SetImageIO( gdcmIO );
//	//reader->SetFileNames( filenames );
//	//try
//	//{
//	//	reader->Update();
//	//}
//	//catch (itk::ExceptionObject &excp)
//	//{
//	//	fout << "Exception thrown while reading the series" << std::endl;
//	//	fout << excp << std::endl;
//	//	return EXIT_FAILURE;
//	//}
//
//	//typedef itk::MetaDataDictionary   DictionaryType;
//	//const  DictionaryType & dictionary = gdcmIO->GetMetaDataDictionary();
//
//
//	//// Since we are interested only in the DICOM tags that can be expressed in
//	//// strings, we declare a MetaDataObject suitable for managing strings.
//
//	//typedef itk::MetaDataObject< std::string > MetaDataStringType;
//
//	//// We instantiate the iterators that will make possible to walk through all the
//	//// entries of the MetaDataDictionary.
//	//DictionaryType::ConstIterator itr = dictionary.Begin();
//	//DictionaryType::ConstIterator end = dictionary.End();
//
//	{
//		m_FileNames->SetInputDirectory( m_DirectoryName );
//
//		const std::vector< std::string > & seriesUID = 
//			m_FileNames -> GetSeriesUIDs();
//
//		std::vector< std::string >::const_iterator iter = seriesUID.begin();
//
//		std::cout << "The directory contains the following series... " << std::endl; 
//		for (; iter != seriesUID.end(); iter++)
//		{
//			std::cout<< "\t" << (*iter) << "\n";
//		}
//
//		if ( seriesUID.empty() ) 
//		{
//			std::cerr << "No valid series in this directory" << std::endl;
//			return false;
//		} 
//
//		std::cout << "Reading series \t " << seriesUID.front().c_str() << std::endl; 
//		m_ImageSeriesReader->SetFileNames( m_FileNames->GetFileNames( 
//			seriesUID.front().c_str() )  );
//
//		try
//		{
//			m_ImageSeriesReader->Update();
//		}
//		catch( itk::ExceptionObject & excp )
//		{
//			std::cerr << "Exception thrown  " << excp.GetDescription() << std::endl;
//			return false;
//		}
//	}
//	
//	std::string yearstr;
//    std::string monthstr;
//    std::string daystr;
//    std::string hourstr;
//    std::string minutestr;
//    std::string secondstr;
//    int len;
//    
//// Nuclear Medicine DICOM info:
///*
//    0054,0016  Radiopharmaceutical Information Sequence:
//    0018,1072  Radionuclide Start Time: 090748.000000
//    0018,1074  Radionuclide Total Dose: 370500000
//    0018,1075  Radionuclide Half Life: 6586.2
//    0018,1076  Radionuclide Positron Fraction: 0
//*/
//    std::string tagKey = "0018|1072";
//    std::string labelId;
//    std::string tagValue;
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
// 
//    //--- expect A string of characters of the format hhmmss.frac;
//    //---where hh contains hours (range "00" - "23"), mm contains minutes
//    //---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
//    //---contains a fractional part of a second as small as 1 millionth of a
//    //---second (range "000000" - "999999"). A 24 hour clock is assumed.
//    //---Midnight can be represented by only "0000" since "2400" would
//    //---violate the hour range. The string may be padded with trailing
//    //---spaces. Leading and embedded spaces are not allowed. One
//    //---or more of the components mm, ss, or frac may be unspecified
//    //---as long as every component to the right of an unspecified
//    //---component is also unspecified. If frac is unspecified the preceding "."
//    //---may not be included. Frac shall be held to six decimal places or
//    //---less to ensure its format conforms to the ANSI 
//    //---Examples -
//    //---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
//    //---2. "1010" represents a time of 10 hours, and 10 minutes.
//    //---3. "021" is an invalid value. 
//    if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//      {
//       this->SetRadiopharmaceuticalStartTime ("no value found");
//      }
//    else
//      {
//      len = tagValue.length();
//      hourstr.clear();
//      minutestr.clear();
//      secondstr.clear();
//      if ( len >= 2 )
//        {
//        hourstr = tagValue.substr(0, 2);
//        }
//      else
//        {
//        hourstr = "00";
//        }
//      if ( len >= 4 )
//        {
//        minutestr = tagValue.substr(2, 2);
//        }
//      else
//        {
//        minutestr = "00";
//        }
//      if ( len >= 6 )
//        {
//        secondstr = tagValue.substr(4);
//        }
//      else
//        {
//        secondstr = "00";
//        }
//      tagValue.clear();
//      tagValue = hourstr.c_str();
//      tagValue += ":";
//      tagValue += minutestr.c_str();
//      tagValue += ":";
//      tagValue += secondstr.c_str();
//      this->SetRadiopharmaceuticalStartTime( tagValue.c_str() );
//      }
//
//    tagKey = "0018|1074";
//    tagValue.clear();
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//    if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//      {
//      this->SetInjectedDose( 0.0 );
//      }
//    else
//      {
//      this->SetInjectedDose( atof ( tagValue.c_str() ) );
//      }
//
//
//    //---
//    //--- RadionuclideHalfLife
//    tagKey = "0018|1075";
//    tagValue.clear();
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//    //--- Expect a Decimal String
//    //--- A string of characters representing either
//    //--- a fixed point number or a floating point number.
//    //--- A fixed point number shall contain only the characters 0-9
//    //--- with an optional leading "+" or "-" and an optional "." to mark
//    //--- the decimal point. A floating point number shall be conveyed
//    //--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
//    //--- of the exponent. Decimal Strings may be padded with leading
//    //--- or trailing spaces. Embedded spaces are not allowed. 
//    if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
//      {
//      this->SetRadionuclideHalfLife( "no value found" );
//      }
//    else
//      {
//      this->SetRadionuclideHalfLife(  tagValue.c_str() );
//      }
//
//    //---
//    //---Radionuclide Positron Fraction
//    tagValue.clear();
//    tagKey ="0054|1001";
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//
//    //--
//    //--- UNITS: something like BQML:
//    //--- CNTS, NONE, CM2, PCNT, CPS, BQML,
//    //--- MGMINML, UMOLMINML, MLMING, MLG,
//    //--- 1CM, UMOLML, PROPCNTS, PROPCPS,
//    //--- MLMINML, MLML, GML, STDDEV      
//    //---
//    if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//      {
//      //--- I think these are piled together. MBq ml... search for all.
//      std::string units = tagValue.c_str();
//      if ( ( units.find ("BQML") != std::string::npos) ||
//           ( units.find ("BQML") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("Bq");
//        this->SetTissueRadioactivityUnits ("Bq");
//        }
//      else if ( ( units.find ("MBq") != std::string::npos) ||
//                ( units.find ("MBQ") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("MBq");
//        this->SetTissueRadioactivityUnits ("MBq");
//        }
//      else if ( (units.find ("kBq") != std::string::npos) ||
//                (units.find ("kBQ") != std::string::npos) ||
//                (units.find ("KBQ") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("kBq");
//        this->SetTissueRadioactivityUnits ("kBq");
//        }
//      else if ( (units.find ("mBq") != std::string::npos) ||
//                (units.find ("mBQ") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("mBq");
//        this->SetTissueRadioactivityUnits ("mBq");
//        }
//      else if ( (units.find ("uBq") != std::string::npos) ||
//                (units.find ("uBQ") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("uBq");
//        this->SetTissueRadioactivityUnits ("uBq");
//        }
//      else if ( (units.find ("Bq") != std::string::npos) ||
//                (units.find ("BQ") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("Bq");
//        this->SetTissueRadioactivityUnits ("Bq");
//        }
//      else if ( (units.find ("MCi") != std::string::npos) ||
//                ( units.find ("MCI") != std::string::npos) )
//        {
//        this->SetDoseRadioactivityUnits ("MCi");
//        this->SetTissueRadioactivityUnits ("MCi");
//
//        }
//      else if ( (units.find ("kCi") != std::string::npos) ||
//                (units.find ("kCI") != std::string::npos)  ||
//                (units.find ("KCI") != std::string::npos) )                
//        {
//        this->SetDoseRadioactivityUnits ("kCi");
//        this->SetTissueRadioactivityUnits ("kCi");
//        }
//      else if ( (units.find ("mCi") != std::string::npos) ||
//                (units.find ("mCI") != std::string::npos) )                
//        {
//        this->SetDoseRadioactivityUnits ("mCi");
//        this->SetTissueRadioactivityUnits ("mCi");
//        }
//      else if ( (units.find ("uCi") != std::string::npos) ||
//                (units.find ("uCI") != std::string::npos) )                
//        {
//        this->SetDoseRadioactivityUnits ("uCi");
//        this->SetTissueRadioactivityUnits ("uCi");
//        }
//      else if ( (units.find ("Ci") != std::string::npos) ||
//                (units.find ("CI") != std::string::npos) )                
//        {
//        this->SetDoseRadioactivityUnits ("Ci");
//        this->SetTissueRadioactivityUnits ("Ci");
//        }
//      this->SetVolumeUnits ( "ml" );
//      }
//    else
//      {
//      //--- default values.
//      this->SetDoseRadioactivityUnits( "MBq" );
//      this->SetTissueRadioactivityUnits( "MBq" );
//      this->SetVolumeUnits ( "ml");        
//      }
//
//
//      //---
//      //--- DecayCorrection
//      //--- Possible values are:
//      //--- NONE = no decay correction
//      //--- START= acquisition start time
//      //--- ADMIN = radiopharmaceutical administration time
//      //--- Frame Reference Time  is the time that the pixel values in the Image occurred. 
//      //--- It's defined as the time offset, in msec, from the Series Reference Time.
//      //--- Series Reference Time is defined by the combination of:
//      //--- Series Date (0008,0021) and
//      //--- Series Time (0008,0031).      
//      //--- We don't pull these out now, but can if we have to.
//    tagValue.clear();
//    tagKey ="0054|1102";
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //---A string of characters with leading or trailing spaces (20H) being non-significant. 
//        this->SetDecayCorrection( tagValue.c_str() );
//        }
//      else
//        {
//        this->SetDecayCorrection( "no value found" );
//        }
//
//    //---
//    //--- StudyDate
//   // this->ClearStudyDate();
//    tagValue.clear();
//    tagKey="0008|0021";
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//    else
//      {
//      std::cerr << "Trying to access inexistant DICOM tag." << std::endl;
//      }
//
//      if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
//        {
//        //--- YYYYMMDD
//        yearstr.clear();
//        daystr.clear();
//        monthstr.clear();
//        len = tagValue.length();
//        if ( len >= 4 )
//          {
//          yearstr = tagValue.substr(0, 4);
//          this->SetStudyDateYear(atoi(yearstr.c_str() ));
//          }
//        else
//          {
//          yearstr = "????";
//          this->SetStudyDateYear(0);
//          }
//        if ( len >= 6 )
//          {
//          monthstr = tagValue.substr(4, 2);
//          this->SetStudyDateMonth( atoi ( monthstr.c_str() ) );
//          }
//        else
//          {
//          monthstr = "??";
//          this->SetStudyDateMonth( 0 );
//          }
//        if ( len >= 8 )
//          {
//          daystr = tagValue.substr (6, 2);
//          this->SetStudyDateDay( atoi ( daystr.c_str() ));
//          }
//        else
//          {
//          daystr = "??";
//          this->SetStudyDateDay(0);
//          }
//        tagValue.clear();
//        tagValue = yearstr.c_str();
//        tagValue += "/";
//        tagValue += monthstr.c_str();
//        tagValue += "/";
//        tagValue += daystr.c_str();
//        this->SetStudyDate ( tagValue.c_str() );
//        }
//      else
//        {
//        this->SetStudyDate ( "no value found" );
//        }
//
//      //---
//      //--- PatientName
//    tagValue.clear();
//    tagKey="0010|0010";
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
// 
//      if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
//        {
//        this->SetPatientName ( tagValue.c_str() );
//        }
//      else
//        {
//        this->SetPatientName ( "no value found");
//        }
//
//      //---
//      //--- DecayFactor
//    tagValue.clear();
//    tagKey="0054|1321";
//    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
// 
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //--- have to parse this out. what we have is
//        //---A string of characters representing either a fixed point number or a
//        //--- floating point number. A fixed point number shall contain only the
//        //---characters 0-9 with an optional leading "+" or "-" and an optional "."
//        //---to mark the decimal point. A floating point number shall be conveyed
//        //---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
//        //---exponent. Decimal Strings may be padded with leading or trailing spaces.
//        //---Embedded spaces are not allowed. or maybe atof does it already...
//        this->SetDecayFactor(  tagValue.c_str()  );
//        }
//      else
//        {
//        this->SetDecayFactor( "no value found" );
//        }
//
//  
//      //---
//      //--- FrameReferenceTime
//      tagValue.clear();
//      tagKey="0054|1300";
//      if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //--- The time that the pixel values in the image
//        //--- occurred. Frame Reference Time is the
//        //--- offset, in msec, from the Series reference
//        //--- time.
//        this->SetFrameReferenceTime( tagValue.c_str() );
//        }
//      else
//        {
//        this->SetFrameReferenceTime( "no value found" );
//        }
//
//
//      //---
//      //--- SeriesTime
//      tagValue.clear();
//      tagKey="0008|0031";
//      if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        hourstr.clear();
//        minutestr.clear();
//        secondstr.clear();
//        len = tagValue.length();
//        if ( len >= 2 )
//          {
//          hourstr = tagValue.substr(0, 2);
//          }
//        else
//          {
//          hourstr = "00";
//          }
//        if ( len >= 4 )
//          {
//          minutestr = tagValue.substr(2, 2);
//          }
//        else
//          {
//          minutestr = "00";
//          }
//        if ( len >= 6 )
//          {
//          secondstr = tagValue.substr(4);
//          }
//        else
//          {
//          secondstr = "00";
//          }
//        tagValue.clear();
//        tagValue = hourstr.c_str();
//        tagValue += ":";
//        tagValue += minutestr.c_str();
//        tagValue += ":";
//        tagValue += secondstr.c_str();
//        this->SetSeriesTime( tagValue.c_str() );
//        }
//      else
//        {
//        this->SetSeriesTime( "no value found");
//        }
//
//
//      //---
//      //--- PatientWeight
//      tagValue.clear();
//      tagKey="0010|1030";
//      if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
// 
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //--- Expect same format as RadionuclideHalfLife
//        this->SetPatientWeight( atof ( tagValue.c_str() ) );
//        this->SetWeightUnits ( "kg" );
//        }
//      else
//        {
//        this->SetPatientWeight( 0.0 );
//        this->SetWeightUnits ( "" );
//        }
//
//
//      //---
//      //--- CalibrationFactor
//      tagValue.clear();
//      tagKey="7053|1009";
//      if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
// 
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //--- converts counts to Bq/cc. If Units = BQML then CalibrationFactor =1 
//        //--- I think we expect the same format as RadiopharmaceuticalStartTime
//        this->SetCalibrationFactor(  tagValue.c_str() );
//        }
//      else
//        {
//        this->SetCalibrationFactor( "no value found" );
//        }
//
//
//      //---
//      //--- PhilipsSUVFactor
//      tagValue.clear();
//      tagKey="7053|1000";
//      if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
//      {
//      std::cout << labelId << " (" << tagKey << "): ";
//      if( m_ImageIO->GetValueFromTag(tagKey, tagValue) )
//        {
//        std::cout << tagValue;
//        }
//      else
//        {
//        std::cout << "(No Value Found in File)";
//        }
//      std::cout << std::endl;
//      }
// 
//      if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
//        {
//        //--- I think we expect the same format as RadiopharmaceuticalStartTime
//        this->SetPhilipsSUVFactor(  tagValue.c_str() );
//        }
//      else
//        {
//        this->SetPhilipsSUVFactor( "no value found" );
//        }
//
//    // check.... did we get all params we need for computation?
//    if ( (this->GetInjectedDose() != 0.0) &&
//         (this->GetPatientWeight() != 0.0) &&
//         (this->GetSeriesTime() != NULL) &&
//         (this->GetRadiopharmaceuticalStartTime() != NULL) &&
//         (this->GetRadionuclideHalfLife() != NULL) )
//      {
//      return 1;
//      }
//    else
//      {
//      return 0;
//      }
////	std::string yearstr;
////	std::string monthstr;
////	std::string daystr;
////	std::string hourstr;
////	std::string minutestr;
////	std::string secondstr;
////	int len;
////
////	// Nuclear Medicine DICOM info:
/////*
////    0054,0016  Radiopharmaceutical Information Sequence:
////    0018,1072  Radionuclide Start Time: 090748.000000
////    0018,1074  Radionuclide Total Dose: 370500000
////    0018,1075  Radionuclide Half Life: 6586.2
////    0018,1076  Radionuclide Positron Fraction: 0
////*/
////    std::string tagKey = "0018|1072";
////    std::string labelId;
////    std::string tagValue;
////    if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////      {
////      fout << labelId << " (" << tagKey << "): ";
////      if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////        {
////        fout << tagValue;
////        }
////      else
////        {
////        fout << "(No Value Found in File)";
////        }
////      fout << std::endl;
////      }
////    else
////      {
////      fout << "Trying to access inexistant DICOM tag." << std::endl;
////      }
////
////	//--- expect A string of characters of the format hhmmss.frac;
////	//---where hh contains hours (range "00" - "23"), mm contains minutes
////	//---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
////	//---contains a fractional part of a second as small as 1 millionth of a
////	//---second (range "000000" - "999999"). A 24 hour clock is assumed.
////	//---Midnight can be represented by only "0000" since "2400" would
////	//---violate the hour range. The string may be padded with trailing
////	//---spaces. Leading and embedded spaces are not allowed. One
////	//---or more of the components mm, ss, or frac may be unspecified
////	//---as long as every component to the right of an unspecified
////	//---component is also unspecified. If frac is unspecified the preceding "."
////	//---may not be included. Frac shall be held to six decimal places or
////	//---less to ensure its format conforms to the ANSI 
////	//---Examples -
////	//---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
////	//---2. "1010" represents a time of 10 hours, and 10 minutes.
////	//---3. "021" is an invalid value. 
////	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
////	{
////		this->SetRadiopharmaceuticalStartTime ("no value found");
////	}
////	else
////	{
////		len = tagValue.length();
////		hourstr.clear();
////		minutestr.clear();
////		secondstr.clear();
////		if ( len >= 2 )
////		{
////			hourstr = tagValue.substr(0, 2);
////		}
////		else
////		{
////			hourstr = "00";
////		}
////		if ( len >= 4 )
////		{
////			minutestr = tagValue.substr(2, 2);
////		}
////		else
////		{
////			minutestr = "00";
////		}
////		if ( len >= 6 )
////		{
////			secondstr = tagValue.substr(4);
////		}
////		else
////		{
////			secondstr = "00";
////		}
////		tagValue.clear();
////		tagValue = hourstr.c_str();
////		tagValue += ":";
////		tagValue += minutestr.c_str();
////		tagValue += ":";
////		tagValue += secondstr.c_str();
////		this->SetRadiopharmaceuticalStartTime( tagValue.c_str() );
////	}
////
////	tagKey = "0018|1074";
////	tagValue.clear();
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	else
////	{
////		fout << "Trying to access inexistant DICOM tag." << std::endl;
////	}
////
////	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
////	{
////		this->SetInjectedDose( 0.0 );
////	}
////	else
////	{
////		this->SetInjectedDose( atof ( tagValue.c_str() ) );
////	}
////
////
////	//---
////	//--- RadionuclideHalfLife
////	tagKey = "0018|1075";
////	tagValue.clear();
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	else
////	{
////		fout << "Trying to access inexistant DICOM tag." << std::endl;
////	}
////
////	//--- Expect a Decimal String
////	//--- A string of characters representing either
////	//--- a fixed point number or a floating point number.
////	//--- A fixed point number shall contain only the characters 0-9
////	//--- with an optional leading "+" or "-" and an optional "." to mark
////	//--- the decimal point. A floating point number shall be conveyed
////	//--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
////	//--- of the exponent. Decimal Strings may be padded with leading
////	//--- or trailing spaces. Embedded spaces are not allowed. 
////	if ( tagValue.c_str() == NULL || *(tagValue.c_str()) == '\0' )
////	{
////		this->SetRadionuclideHalfLife( "no value found" );
////	}
////	else
////	{
////		this->SetRadionuclideHalfLife(  tagValue.c_str() );
////	}
////
////	//---
////	//---Radionuclide Positron Fraction
////	tagValue.clear();
////	tagKey ="0054|1001";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	else
////	{
////		fout << "Trying to access inexistant DICOM tag." << std::endl;
////	}
////
////
////	//--
////	//--- UNITS: something like BQML:
////	//--- CNTS, NONE, CM2, PCNT, CPS, BQML,
////	//--- MGMINML, UMOLMINML, MLMING, MLG,
////	//--- 1CM, UMOLML, PROPCNTS, PROPCPS,
////	//--- MLMINML, MLML, GML, STDDEV      
////	//---
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- I think these are piled together. MBq ml... search for all.
////		std::string units = tagValue.c_str();
////		if ( ( units.find ("BQML") != std::string::npos) ||
////			( units.find ("BQML") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("Bq");
////			this->SetTissueRadioactivityUnits ("Bq");
////		}
////		else if ( ( units.find ("MBq") != std::string::npos) ||
////			( units.find ("MBQ") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("MBq");
////			this->SetTissueRadioactivityUnits ("MBq");
////		}
////		else if ( (units.find ("kBq") != std::string::npos) ||
////			(units.find ("kBQ") != std::string::npos) ||
////			(units.find ("KBQ") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("kBq");
////			this->SetTissueRadioactivityUnits ("kBq");
////		}
////		else if ( (units.find ("mBq") != std::string::npos) ||
////			(units.find ("mBQ") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("mBq");
////			this->SetTissueRadioactivityUnits ("mBq");
////		}
////		else if ( (units.find ("uBq") != std::string::npos) ||
////			(units.find ("uBQ") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("uBq");
////			this->SetTissueRadioactivityUnits ("uBq");
////		}
////		else if ( (units.find ("Bq") != std::string::npos) ||
////			(units.find ("BQ") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("Bq");
////			this->SetTissueRadioactivityUnits ("Bq");
////		}
////		else if ( (units.find ("MCi") != std::string::npos) ||
////			( units.find ("MCI") != std::string::npos) )
////		{
////			this->SetDoseRadioactivityUnits ("MCi");
////			this->SetTissueRadioactivityUnits ("MCi");
////
////		}
////		else if ( (units.find ("kCi") != std::string::npos) ||
////			(units.find ("kCI") != std::string::npos)  ||
////			(units.find ("KCI") != std::string::npos) )                
////		{
////			this->SetDoseRadioactivityUnits ("kCi");
////			this->SetTissueRadioactivityUnits ("kCi");
////		}
////		else if ( (units.find ("mCi") != std::string::npos) ||
////			(units.find ("mCI") != std::string::npos) )                
////		{
////			this->SetDoseRadioactivityUnits ("mCi");
////			this->SetTissueRadioactivityUnits ("mCi");
////		}
////		else if ( (units.find ("uCi") != std::string::npos) ||
////			(units.find ("uCI") != std::string::npos) )                
////		{
////			this->SetDoseRadioactivityUnits ("uCi");
////			this->SetTissueRadioactivityUnits ("uCi");
////		}
////		else if ( (units.find ("Ci") != std::string::npos) ||
////			(units.find ("CI") != std::string::npos) )                
////		{
////			this->SetDoseRadioactivityUnits ("Ci");
////			this->SetTissueRadioactivityUnits ("Ci");
////		}
////		this->SetVolumeUnits ( "ml" );
////	}
////	else
////	{
////		//--- default values.
////		this->SetDoseRadioactivityUnits( "MBq" );
////		this->SetTissueRadioactivityUnits( "MBq" );
////		this->SetVolumeUnits ( "ml");        
////	}
////
////	//---
////	//--- DecayCorrection
////	//--- Possible values are:
////	//--- NONE = no decay correction
////	//--- START= acquisition start time
////	//--- ADMIN = radiopharmaceutical administration time
////	//--- Frame Reference Time  is the time that the pixel values in the Image occurred. 
////	//--- It's defined as the time offset, in msec, from the Series Reference Time.
////	//--- Series Reference Time is defined by the combination of:
////	//--- Series Date (0008,0021) and
////	//--- Series Time (0008,0031).      
////	//--- We don't pull these out now, but can if we have to.
////	tagValue.clear();
////	tagKey ="0054|1102";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	else
////	{
////		fout << "Trying to access inexistant DICOM tag." << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//---A string of characters with leading or trailing spaces (20H) being non-significant. 
////		this->SetDecayCorrection( tagValue.c_str() );
////	}
////	else
////	{
////		this->SetDecayCorrection( "no value found" );
////	}
////
////	//---
////	//--- StudyDate
////	//this->ClearStudyDate();
////	tagValue.clear();
////	tagKey="0008|0021";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	else
////	{
////		fout << "Trying to access inexistant DICOM tag." << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
////	{
////		//--- YYYYMMDD
////		yearstr.clear();
////		daystr.clear();
////		monthstr.clear();
////		len = tagValue.length();
////		if ( len >= 4 )
////		{
////			yearstr = tagValue.substr(0, 4);
////			this->SetStudyDateYear(atoi(yearstr.c_str() ));
////		}
////		else
////		{
////			yearstr = "????";
////			this->SetStudyDateYear(0);
////		}
////		if ( len >= 6 )
////		{
////			monthstr = tagValue.substr(4, 2);
////			this->SetStudyDateMonth( atoi ( monthstr.c_str() ) );
////		}
////		else
////		{
////			monthstr = "??";
////			this->SetStudyDateMonth( 0 );
////		}
////		if ( len >= 8 )
////		{
////			daystr = tagValue.substr (6, 2);
////			this->SetStudyDateDay( atoi ( daystr.c_str() ));
////		}
////		else
////		{
////			daystr = "??";
////			this->SetStudyDateDay(0);
////		}
////		tagValue.clear();
////		tagValue = yearstr.c_str();
////		tagValue += "/";
////		tagValue += monthstr.c_str();
////		tagValue += "/";
////		tagValue += daystr.c_str();
////		this->SetStudyDate ( tagValue.c_str() );
////	}
////	else
////	{
////		this->SetStudyDate ( "no value found" );
////	}
////
////	//---
////	//--- PatientName
////	tagValue.clear();
////	tagKey="0010|0010";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp (tagValue.c_str(), "" ) )
////	{
////		this->SetPatientName ( tagValue.c_str() );
////	}
////	else
////	{
////		this->SetPatientName ( "no value found");
////	}
////
////	//---
////	//--- DecayFactor
////	tagValue.clear();
////	tagKey="0054|1321";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- have to parse this out. what we have is
////		//---A string of characters representing either a fixed point number or a
////		//--- floating point number. A fixed point number shall contain only the
////		//---characters 0-9 with an optional leading "+" or "-" and an optional "."
////		//---to mark the decimal point. A floating point number shall be conveyed
////		//---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
////		//---exponent. Decimal Strings may be padded with leading or trailing spaces.
////		//---Embedded spaces are not allowed. or maybe atof does it already...
////		this->SetDecayFactor(  tagValue.c_str()  );
////	}
////	else
////	{
////		this->SetDecayFactor( "no value found" );
////	}
////
////
////	//---
////	//--- FrameReferenceTime
////	tagValue.clear();
////	tagKey="0054|1300";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- The time that the pixel values in the image
////		//--- occurred. Frame Reference Time is the
////		//--- offset, in msec, from the Series reference
////		//--- time.
////		this->SetFrameReferenceTime( tagValue.c_str() );
////	}
////	else
////	{
////		this->SetFrameReferenceTime( "no value found" );
////	}
////
////
////	//---
////	//--- SeriesTime
////	tagValue.clear();
////	tagKey="0008|0031";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		hourstr.clear();
////		minutestr.clear();
////		secondstr.clear();
////		len = tagValue.length();
////		if ( len >= 2 )
////		{
////			hourstr = tagValue.substr(0, 2);
////		}
////		else
////		{
////			hourstr = "00";
////		}
////		if ( len >= 4 )
////		{
////			minutestr = tagValue.substr(2, 2);
////		}
////		else
////		{
////			minutestr = "00";
////		}
////		if ( len >= 6 )
////		{
////			secondstr = tagValue.substr(4);
////		}
////		else
////		{
////			secondstr = "00";
////		}
////		tagValue.clear();
////		tagValue = hourstr.c_str();
////		tagValue += ":";
////		tagValue += minutestr.c_str();
////		tagValue += ":";
////		tagValue += secondstr.c_str();
////		this->SetSeriesTime( tagValue.c_str() );
////	}
////	else
////	{
////		this->SetSeriesTime( "no value found");
////	}
////
////
////	//---
////	//--- PatientWeight
////	tagValue.clear();
////	tagKey="0010|1030";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- Expect same format as RadionuclideHalfLife
////		this->SetPatientWeight( atof ( tagValue.c_str() ) );
////		this->SetWeightUnits ( "kg" );
////	}
////	else
////	{
////		this->SetPatientWeight( 0.0 );
////		this->SetWeightUnits ( "" );
////	}
////
////
////	//---
////	//--- CalibrationFactor
////	tagValue.clear();
////	tagKey="7053|1009";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- converts counts to Bq/cc. If Units = BQML then CalibrationFactor =1 
////		//--- I think we expect the same format as RadiopharmaceuticalStartTime
////		this->SetCalibrationFactor(  tagValue.c_str() );
////	}
////	else
////	{
////		this->SetCalibrationFactor( "no value found" );
////	}
////
////
////	//---
////	//--- PhilipsSUVFactor
////	tagValue.clear();
////	tagKey="7053|1000";
////	if( itk::GDCMImageIO::GetLabelFromTag( tagKey, labelId ) )
////	{
////		fout << labelId << " (" << tagKey << "): ";
////		if( gdcmIO->GetValueFromTag(tagKey, tagValue) )
////		{
////			fout << tagValue;
////		}
////		else
////		{
////			fout << "(No Value Found in File)";
////		}
////		fout << std::endl;
////	}
////
////	if ( tagValue.c_str() != NULL && strcmp(tagValue.c_str(), "" ) )
////	{
////		//--- I think we expect the same format as RadiopharmaceuticalStartTime
////		this->SetPhilipsSUVFactor(  tagValue.c_str() );
////	}
////	else
////	{
////		this->SetPhilipsSUVFactor( "no value found" );
////	}
////
////	// check.... did we get all params we need for computation?
////	if ( (this->GetInjectedDose() != 0.0) &&
////		(this->GetPatientWeight() != 0.0) &&
////		(this->GetSeriesTime() != NULL) &&
////		(this->GetRadiopharmaceuticalStartTime() != NULL) &&
////		(this->GetRadionuclideHalfLife() != NULL) )
////	{
////		return 1;
////	}
////	else
////	{
////		return 0;
////	}
//
//}

//bool PETDICOMReader::readDicomTags2()
//{
//	const unsigned int InputDimension = 3;
//	const unsigned int OutputDimension = 2;
//
//	typedef signed short PixelType;
//
//	typedef itk::Image< PixelType, InputDimension >	InputImageType;
//	typedef itk::ImageSeriesReader< InputImageType > ReaderType;
//	typedef itk::GDCMImageIO ImageIOType;
//	typedef itk::GDCMSeriesFileNames InputNamesGeneratorType;
//
//	itk::GDCMSeriesFileNames::Pointer     m_FileNames;
//	itk::GDCMImageIO::Pointer             m_ImageIO;
//
//	ReaderType::Pointer        m_ImageSeriesReader;
//	// Create the DICOM GDCM file reader
//	m_FileNames = itk::GDCMSeriesFileNames::New();
//	m_FileNames->SetRecursive(false);
//	//m_FileNames->SetGlobalWarningDisplay(this->GetGlobalWarningDisplay());
//
//	// add more criteria to distinguish between different series
//	m_FileNames->SetUseSeriesDetails( true );
//
//	m_ImageIO = itk::GDCMImageIO::New();
//	//m_ImageIO->SetGlobalWarningDisplay(this->GetGlobalWarningDisplay());
//	m_ImageSeriesReader = ReaderType::New();
//	m_ImageSeriesReader->SetImageIO( m_ImageIO );
//
//	//m_ValidDicomDirectory = false;
//	//m_DicomDataRead = false;
//
//	//std::string tempStr = fileName;
//	//std::size_t pos = tempStr.find_last_of("/");
//	std::string dirName = m_DirectoryName;//tempStr;//tempStr.substr(0,pos);
//	//m_DirectoryName = dirName;
//
//	{
//		m_FileNames->SetInputDirectory( m_DirectoryName );
//
//		const std::vector< std::string > & seriesUID = 
//			m_FileNames -> GetSeriesUIDs();
//
//		std::vector< std::string >::const_iterator iter = seriesUID.begin();
//
//		std::cout << "The directory contains the following series... " << std::endl; 
//		for (; iter != seriesUID.end(); iter++)
//		{
//			std::cout<< "\t" << (*iter) << "\n";
//		}
//
//		if ( seriesUID.empty() ) 
//		{
//			std::cerr << "No valid series in this directory" << std::endl;
//			return false;
//		} 
//
//		std::cout << "Reading series \t " << seriesUID.front().c_str() << std::endl; 
//		m_ImageSeriesReader->SetFileNames( m_FileNames->GetFileNames( 
//			seriesUID.front().c_str() )  );
//
//		try
//		{
//			m_ImageSeriesReader->Update();
//			//m_DicomDataRead = true;
//		}
//		catch( itk::ExceptionObject & excp )
//		{
//			std::cerr << "Exception thrown  " << excp.GetDescription() << std::endl;
//			return false;
//		}
//	}
//	
//	const std::vector< std::string > & seriesUID = 
//		m_FileNames -> GetSeriesUIDs();
//
//
//	const ReaderType::FileNamesContainer & 
//		filenames = m_FileNames->GetFileNames( seriesUID.front().c_str());
//
//	std::string tag;
//    std::string yearstr;
//    std::string monthstr;
//    std::string daystr;
//    std::string hourstr;
//    std::string minutestr;
//    std::string secondstr;
//    int len;
//    
//// Nuclear Medicine DICOM info:
///*
//    0054,0016  Radiopharmaceutical Information Sequence:
//    0018,1072  Radionuclide Start Time: 090748.000000
//    0018,1074  Radionuclide Total Dose: 370500000
//    0018,1075  Radionuclide Half Life: 6586.2
//    0018,1076  Radionuclide Positron Fraction: 0
//*/
//    int parsingDICOM = 0;
//    gdcm::File *f = new gdcm::File();
//    if ( f != NULL )
//      {
//      const char *fn = filenames[0].c_str();
//      f->SetFileName( fn );
//      //bool res = f->Load();   // FIXME: commented out for now to avoid compile warnings
//      f->Load();   // FIXME: handle res
//
//      gdcm::SeqEntry *seq = f->GetSeqEntry(0x0054,0x0016);
//      if ( seq != NULL )
//        {
//        parsingDICOM = 1;
//        gdcm::SQItem *sqItem = seq->GetFirstSQItem();
//        while ( sqItem )
//          {
//          //---
//          //--- Radiopharmaceutical Start Time
//          tag.clear();
//          tag = sqItem->GetEntryValue(0x0018,0x1072);
//          //--- expect A string of characters of the format hhmmss.frac;
//          //---where hh contains hours (range "00" - "23"), mm contains minutes
//          //---(range "00" - "59"), ss contains seconds (range "00" - "59"), and frac
//          //---contains a fractional part of a second as small as 1 millionth of a
//          //---second (range "000000" - "999999"). A 24 hour clock is assumed.
//          //---Midnight can be represented by only "0000" since "2400" would
//          //---violate the hour range. The string may be padded with trailing
//          //---spaces. Leading and embedded spaces are not allowed. One
//          //---or more of the components mm, ss, or frac may be unspecified
//          //---as long as every component to the right of an unspecified
//          //---component is also unspecified. If frac is unspecified the preceding "."
//          //---may not be included. Frac shall be held to six decimal places or
//          //---less to ensure its format conforms to the ANSI 
//          //---Examples -
//          //---1. "070907.0705" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
//          //---2. "1010" represents a time of 10 hours, and 10 minutes.
//          //---3. "021" is an invalid value. 
//          if ( tag.c_str() == NULL || *(tag.c_str()) == '\0' )
//            {
//             this->SetRadiopharmaceuticalStartTime ("no value found");
//            }
//          else
//            {
//            len = tag.length();
//            hourstr.clear();
//            minutestr.clear();
//            secondstr.clear();
//            if ( len >= 2 )
//              {
//              hourstr = tag.substr(0, 2);
//              }
//            else
//              {
//              hourstr = "00";
//              }
//            if ( len >= 4 )
//              {
//              minutestr = tag.substr(2, 2);
//              }
//            else
//              {
//              minutestr = "00";
//              }
//            if ( len >= 6 )
//              {
//              secondstr = tag.substr(4);
//              }
//            else
//              {
//              secondstr = "00";
//              }
//            tag.clear();
//            tag = hourstr.c_str();
//            tag += ":";
//            tag += minutestr.c_str();
//            tag += ":";
//            tag += secondstr.c_str();
//            this->SetRadiopharmaceuticalStartTime( tag.c_str() );
//            }
//
//          //---
//          //--- Radionuclide Total Dose 
//          tag.clear();
//          tag = sqItem->GetEntryValue(0x0018,0x1074);
//          if ( tag.c_str() == NULL || *(tag.c_str()) == '\0' )
//            {
//            this->SetInjectedDose( 0.0 );
//            }
//          else
//            {
//            this->SetInjectedDose( atof ( tag.c_str() ) );
//            }
//
//
//          //---
//          //--- RadionuclideHalfLife
//          tag.clear();
//          tag = sqItem->GetEntryValue(0x0018,0x1075);
//          //--- Expect a Decimal String
//          //--- A string of characters representing either
//          //--- a fixed point number or a floating point number.
//          //--- A fixed point number shall contain only the characters 0-9
//          //--- with an optional leading "+" or "-" and an optional "." to mark
//          //--- the decimal point. A floating point number shall be conveyed
//          //--- as defined in ANSI X3.9, with an "E" or "e" to indicate the start
//          //--- of the exponent. Decimal Strings may be padded with leading
//          //--- or trailing spaces. Embedded spaces are not allowed. 
//          if ( tag.c_str() == NULL || *(tag.c_str()) == '\0' )
//            {
//            this->SetRadionuclideHalfLife( "no value found" );
//            }
//          else
//            {
//            this->SetRadionuclideHalfLife(  tag.c_str() );
//            }
//
//          //---
//          //---Radionuclide Positron Fraction
//          tag.clear();
//          tag = sqItem->GetEntryValue(0x0018,0x1076);
//          //--- not currently using this one?
//
//          sqItem = seq->GetNextSQItem();
//          }
//
//        //--
//        //--- UNITS: something like BQML:
//        //--- CNTS, NONE, CM2, PCNT, CPS, BQML,
//        //--- MGMINML, UMOLMINML, MLMING, MLG,
//        //--- 1CM, UMOLML, PROPCNTS, PROPCPS,
//        //--- MLMINML, MLML, GML, STDDEV      
//        //---
//        tag.clear();
//        tag = f->GetEntryValue (0x0054,0x1001);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          //--- I think these are piled together. MBq ml... search for all.
//          std::string units = tag.c_str();
//          if ( ( units.find ("BQML") != std::string::npos) ||
//               ( units.find ("BQML") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("Bq");
//            this->SetTissueRadioactivityUnits ("Bq");
//            }
//          else if ( ( units.find ("MBq") != std::string::npos) ||
//                    ( units.find ("MBQ") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("MBq");
//            this->SetTissueRadioactivityUnits ("MBq");
//            }
//          else if ( (units.find ("kBq") != std::string::npos) ||
//                    (units.find ("kBQ") != std::string::npos) ||
//                    (units.find ("KBQ") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("kBq");
//            this->SetTissueRadioactivityUnits ("kBq");
//            }
//          else if ( (units.find ("mBq") != std::string::npos) ||
//                    (units.find ("mBQ") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("mBq");
//            this->SetTissueRadioactivityUnits ("mBq");
//            }
//          else if ( (units.find ("uBq") != std::string::npos) ||
//                    (units.find ("uBQ") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("uBq");
//            this->SetTissueRadioactivityUnits ("uBq");
//            }
//          else if ( (units.find ("Bq") != std::string::npos) ||
//                    (units.find ("BQ") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("Bq");
//            this->SetTissueRadioactivityUnits ("Bq");
//            }
//          else if ( (units.find ("MCi") != std::string::npos) ||
//                    ( units.find ("MCI") != std::string::npos) )
//            {
//            this->SetDoseRadioactivityUnits ("MCi");
//            this->SetTissueRadioactivityUnits ("MCi");
//
//            }
//          else if ( (units.find ("kCi") != std::string::npos) ||
//                    (units.find ("kCI") != std::string::npos)  ||
//                    (units.find ("KCI") != std::string::npos) )                
//            {
//            this->SetDoseRadioactivityUnits ("kCi");
//            this->SetTissueRadioactivityUnits ("kCi");
//            }
//          else if ( (units.find ("mCi") != std::string::npos) ||
//                    (units.find ("mCI") != std::string::npos) )                
//            {
//            this->SetDoseRadioactivityUnits ("mCi");
//            this->SetTissueRadioactivityUnits ("mCi");
//            }
//          else if ( (units.find ("uCi") != std::string::npos) ||
//                    (units.find ("uCI") != std::string::npos) )                
//            {
//            this->SetDoseRadioactivityUnits ("uCi");
//            this->SetTissueRadioactivityUnits ("uCi");
//            }
//          else if ( (units.find ("Ci") != std::string::npos) ||
//                    (units.find ("CI") != std::string::npos) )                
//            {
//            this->SetDoseRadioactivityUnits ("Ci");
//            this->SetTissueRadioactivityUnits ("Ci");
//            }
//          this->SetVolumeUnits ( "ml" );
//          }
//        else
//          {
//          //--- default values.
//          this->SetDoseRadioactivityUnits( "MBq" );
//          this->SetTissueRadioactivityUnits( "MBq" );
//          this->SetVolumeUnits ( "ml");        
//          }
//
//    
//        //---
//        //--- DecayCorrection
//        //--- Possible values are:
//        //--- NONE = no decay correction
//        //--- START= acquisition start time
//        //--- ADMIN = radiopharmaceutical administration time
//        //--- Frame Reference Time  is the time that the pixel values in the Image occurred. 
//        //--- It's defined as the time offset, in msec, from the Series Reference Time.
//        //--- Series Reference Time is defined by the combination of:
//        //--- Series Date (0008,0021) and
//        //--- Series Time (0008,0031).      
//        //--- We don't pull these out now, but can if we have to.
//        tag.clear();
//        tag = f->GetEntryValue (0x0054,0x1102);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          //---A string of characters with leading or trailing spaces (20H) being non-significant. 
//          this->SetDecayCorrection( tag.c_str() );
//          }
//        else
//          {
//          this->SetDecayCorrection( "no value found" );
//          }
//
//        //---
//        //--- StudyDate
//        //this->ClearStudyDate();
//        tag.clear();
//        tag = f->GetEntryValue (0x0008,0x0021);
//        if ( tag.c_str() != NULL && strcmp (tag.c_str(), "" ) )
//          {
//          //--- YYYYMMDD
//          yearstr.clear();
//          daystr.clear();
//          monthstr.clear();
//          len = tag.length();
//          if ( len >= 4 )
//            {
//            yearstr = tag.substr(0, 4);
//            this->SetStudyDateYear(atoi(yearstr.c_str() ));
//            }
//          else
//            {
//            yearstr = "????";
//            this->SetStudyDateYear(0);
//            }
//          if ( len >= 6 )
//            {
//            monthstr = tag.substr(4, 2);
//            this->SetStudyDateMonth( atoi ( monthstr.c_str() ) );
//            }
//          else
//            {
//            monthstr = "??";
//            this->SetStudyDateMonth( 0 );
//            }
//          if ( len >= 8 )
//            {
//            daystr = tag.substr (6, 2);
//            this->SetStudyDateDay( atoi ( daystr.c_str() ));
//            }
//          else
//            {
//            daystr = "??";
//            this->SetStudyDateDay(0);
//            }
//          tag.clear();
//          tag = yearstr.c_str();
//          tag += "/";
//          tag += monthstr.c_str();
//          tag += "/";
//          tag += daystr.c_str();
//          this->SetStudyDate ( tag.c_str() );
//          }
//        else
//          {
//          this->SetStudyDate ( "no value found" );
//          }
//
//        //---
//        //--- PatientName
//        tag.clear();
//        tag = f->GetEntryValue (0x0010,0x0010);
//        if ( tag.c_str() != NULL && strcmp (tag.c_str(), "" ) )
//          {
//          this->SetPatientName ( tag.c_str() );
//          }
//        else
//          {
//          this->SetPatientName ( "no value found");
//          }
//
//        //---
//        //--- DecayFactor
//        tag.clear();
//        tag = f->GetEntryValue (0x0054,0x1321);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          //--- have to parse this out. what we have is
//          //---A string of characters representing either a fixed point number or a
//          //--- floating point number. A fixed point number shall contain only the
//          //---characters 0-9 with an optional leading "+" or "-" and an optional "."
//          //---to mark the decimal point. A floating point number shall be conveyed
//          //---as defined in ANSI X3.9, with an "E" or "e" to indicate the start of the
//          //---exponent. Decimal Strings may be padded with leading or trailing spaces.
//          //---Embedded spaces are not allowed. or maybe atof does it already...
//          this->SetDecayFactor(  tag.c_str()  );
//          }
//        else
//          {
//          this->SetDecayFactor( "no value found" );
//          }
//
//    
//        //---
//        //--- FrameReferenceTime
//        tag.clear();
//        tag = f->GetEntryValue (0x0054,0x1300);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          //--- The time that the pixel values in the image
//          //--- occurred. Frame Reference Time is the
//          //--- offset, in msec, from the Series reference
//          //--- time.
//          this->SetFrameReferenceTime( tag.c_str() );
//          }
//        else
//          {
//          this->SetFrameReferenceTime( "no value found" );
//          }
//
//  
//        //---
//        //--- SeriesTime
//        tag.clear();
//        tag = f->GetEntryValue (0x0008,0x0031);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          hourstr.clear();
//          minutestr.clear();
//          secondstr.clear();
//          len = tag.length();
//          if ( len >= 2 )
//            {
//            hourstr = tag.substr(0, 2);
//            }
//          else
//            {
//            hourstr = "00";
//            }
//          if ( len >= 4 )
//            {
//            minutestr = tag.substr(2, 2);
//            }
//          else
//            {
//            minutestr = "00";
//            }
//          if ( len >= 6 )
//            {
//            secondstr = tag.substr(4);
//            }
//          else
//            {
//            secondstr = "00";
//            }
//          tag.clear();
//          tag = hourstr.c_str();
//          tag += ":";
//          tag += minutestr.c_str();
//          tag += ":";
//          tag += secondstr.c_str();
//          this->SetSeriesTime( tag.c_str() );
//          }
//        else
//          {
//          this->SetSeriesTime( "no value found");
//          }
//
//
//        //---
//        //--- PatientWeight
//        tag.clear();
//        tag = f->GetEntryValue (0x0010,0x1030);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) )
//          {
//          //--- Expect same format as RadionuclideHalfLife
//          this->SetPatientWeight( atof ( tag.c_str() ) );
//          this->SetWeightUnits ( "kg" );
//          }
//        else
//          {
//          this->SetPatientWeight( 0.0 );
//          this->SetWeightUnits ( "" );
//          }
//
//
//        //---
//        //--- CalibrationFactor
//        tag.clear();
//        tag = f->GetEntryValue (0x7053,0x1009);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) && strcmp(tag.c_str(), gdcm::GDCM_UNFOUND.c_str()) )
//          {
//          //--- converts counts to Bq/cc. If Units = BQML then CalibrationFactor =1 
//          //--- I think we expect the same format as RadiopharmaceuticalStartTime
//          this->SetCalibrationFactor(  tag.c_str() );
//          }
//        else
//          {
//          this->SetCalibrationFactor( "no value found" );
//          }
//
//
//        //---
//        //--- PhilipsSUVFactor
//        tag.clear();
//        tag = f->GetEntryValue (0x7053,0x1000);
//        if ( tag.c_str() != NULL && strcmp(tag.c_str(), "" ) && strcmp(tag.c_str(), gdcm::GDCM_UNFOUND.c_str()) )
//          {
//          //--- I think we expect the same format as RadiopharmaceuticalStartTime
//          this->SetPhilipsSUVFactor(  tag.c_str() );
//          }
//        else
//          {
//          this->SetPhilipsSUVFactor( "no value found" );
//          }
//        }
//      }
//    //END TEST
//    delete f;
//
//
//    // check.... did we get all params we need for computation?
//    if ( (parsingDICOM) &&
//         (this->GetInjectedDose() != 0.0) &&
//         (this->GetPatientWeight() != 0.0) &&
//         (this->GetSeriesTime() != NULL) &&
//         (this->GetRadiopharmaceuticalStartTime() != NULL) &&
//         (this->GetRadionuclideHalfLife() != NULL) )
//      {
//      return 1;
//      }
//    else
//      {
//      return 0;
//      }
//}



