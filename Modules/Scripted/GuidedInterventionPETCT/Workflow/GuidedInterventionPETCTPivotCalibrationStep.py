import os
import qt, ctk, vtk, slicer

import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTPivotCalibrationStep( ctk.ctkWorkflowWidgetStep ) :

  def __init__( self, stepid, pivotCalibrationGUI):
    self.initialize( stepid )
    self.pivotCalibrationGUI = pivotCalibrationGUI

    self.setName( '4. Pivot Calibration' )
    self.setDescription('Do pivot calibration of the tracking device')

    qt.QTimer.singleShot( 0, self.killButton )


  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren( text='Tracker' )
    if len(bl):
      bl[0].hide()


  def createUserInterface( self ):
    layout = qt.QVBoxLayout( self )

    # Get the widget form the Pivot Calibration module
    layout.addWidget(self.pivotCalibrationGUI)

    # Set the node Selector to the tracker transform for pivot calibration
    trackerSelector = workflowFunctions.get( self.pivotCalibrationGUI, "TransformNodeSelector" )
    trackerSelector.setCurrentNode(workflowFunctions.getTracker())


  def validate( self, desiredBranchId ):
    # Check if calibration result was generated before going to next step
    calibrationNode = slicer.mrmlScene.GetFirstNodeByName( "CalibrationResultTransform" )
    if calibrationNode is None:
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Calibration Transform was not generated' )
      super( GuidedInterventionPETCTPivotCalibrationStep, self ).validate(False, desiredBranchId )
    else:
      super( GuidedInterventionPETCTPivotCalibrationStep, self ).validate(True, desiredBranchId )


  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super( GuidedInterventionPETCTPivotCalibrationStep, self ).onEntry( comingFrom, transitionType )
    qt.QTimer.singleShot( 0, self.killButton )
    self.setCBCTVolumeAsBackground()

  def onExit( self, goingTo, transitionType ):
    goingToId = "None"
    if goingTo: goingToId = goingTo.id()
    super( GuidedInterventionPETCTPivotCalibrationStep, self ).onExit( goingTo, transitionType )

  def setCBCTVolumeAsBackground(self):
    cbctVolumeNode = slicer.util.getNode('CBCTVolume')
    if cbctVolumeNode != None:
      workflowFunctions.setActiveVolume(cbctVolumeNode)
