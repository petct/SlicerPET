import os
import qt, ctk, vtk, slicer

import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTPathPlannerStep( ctk.ctkWorkflowWidgetStep ) :
  # ------------------------------------------------------
  def __init__( self, stepid):
    self.initialize( stepid )

    self.markupsLogic = slicer.modules.markups.logic()
    self.annotationsLogic = slicer.modules.annotations.logic()

    self.setName( '6. PathPlanner' )
    self.setDescription('Simulating a path for an entry and target point')

    qt.QTimer.singleShot(0, self.killButton)
    self.Observations = []

  # ------------------------------------------------------
  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()


  # ------------------------------------------------------
  def createUserInterface( self ):
    # Get the PathPlanner module
    #layout.addWidget(self.pathPlannerGUI)
    #workflowFunctions.centerViews()

    # Create widget from UI file
    layout = qt.QVBoxLayout(self)
    self.widget = workflowFunctions.loadUI('GuidedInterventionPETCTPathPlanner.ui')
    layout.addWidget(self.widget)
    workflowFunctions.setScene(self.widget)
    self.createMarkerNodes()

    # Link buttons to functions
    workflowFunctions.get(self.widget, "PlaceTargetFiducialPushButton").connect('clicked(bool)', self.placeTargetFiducial)
    workflowFunctions.get(self.widget, "PlaceEntryFiducialPushButton").connect('clicked(bool)', self.placeEntryFiducial)
    workflowFunctions.get(self.widget, "EntryFromNeedleTipButton").connect('clicked()', self.entryFromNeedleTip)
    workflowFunctions.get(self.widget, "TargetFiducialRASPushButton").connect('clicked()', self.jumpToTargetFiducial)
    workflowFunctions.get(self.widget, "EntryFiducialRASPushButton").connect('clicked()', self.jumpToEntryFiducial)
    workflowFunctions.get(self.widget, "GuidanceLineThicknessSlider").connect('valueChanged(double)', self.setGuidanceThickness)
    workflowFunctions.get(self.widget, "TargetSizeSlider").connect('valueChanged(double)', self.setTargetSize)
    workflowFunctions.get(self.widget, "TargetOpacitySlider").connect('valueChanged(double)', self.setTargetOpacity)
    workflowFunctions.get(self.widget, "ResliceCheckBox").connect('clicked(bool)', self.reslice)


  # ------------------------------------------------------
  def validate( self, desiredBranchId ):
    rulerNode = slicer.mrmlScene.GetFirstNodeByName("Path")
    validation=True
    if not rulerNode:
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Please create a guiding path' )
      validation=False
    super( GuidedInterventionPETCTPathPlannerStep, self ).validate(validation,desiredBranchId)

  # ------------------------------------------------------
  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super( GuidedInterventionPETCTPathPlannerStep, self ).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)
    self.setSlicesVisible(True)
    self.setPETVolumeAsForeground()

  # ------------------------------------------------------
  def onExit(self, goingTo, transitionType):
    goingToId = "None"
    if goingTo: goingToId = goingTo.id()
    super( GuidedInterventionPETCTPathPlannerStep, self ).onExit(goingTo, transitionType)


  # ------------------------------------------------------
  def setSlicesVisible(self, show):
    redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    yellowSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
    greenSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')

    redSlice.SetSliceVisible(show)
    yellowSlice.SetSliceVisible(show)
    greenSlice.SetSliceVisible(show)


  # ------------------------------------------------------
  def createMarkerNodes(self):
    # create the nodes
    self.targetMarkerNode()
    self.entryMarkerNode()

    # Make the Target marker node (arbitrary) active.
    self.markupsLogic.SetActiveListID( self.targetMarkerNode() )

  # ------------------------------------------------------
  # Target coordinates
  def targetMarkerNode(self):
    markerNode = slicer.mrmlScene.GetFirstNodeByName("TargetMarker")
    if not markerNode:
      markerNode = slicer.mrmlScene.GetNodeByID(self.markupsLogic.AddNewFiducialNode("TargetMarker"))
      workflowFunctions.addObserver(self, markerNode, 'ModifiedEvent', self.onTargetMarkerModified)
      self.setTargetSize(workflowFunctions.get(self.widget, "TargetSizeSlider").value)
    return markerNode

  # ------------------------------------------------------
  # Entry coordinates
  def entryMarkerNode(self):
    markerNode = slicer.mrmlScene.GetFirstNodeByName("EntryMarker")
    if not markerNode:
      markerNode = slicer.mrmlScene.GetNodeByID(self.markupsLogic.AddNewFiducialNode("EntryMarker"))
      workflowFunctions.addObserver(self, markerNode, 'ModifiedEvent', self.onEntryMarkerModified)
    return markerNode

  # ------------------------------------------------------
  def placeTargetFiducial(self, place):
    interactionNode =  slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    if place:
      self.stopPlacingEntryFiducial(interactionNode, None)
      self.markupsLogic.SetActiveListID(self.targetMarkerNode())
      self.targetMarkerNode().RemoveAllMarkups()
      self.markupsLogic.StartPlaceMode(False)
      workflowFunctions.addObserver(self, interactionNode, interactionNode.EndPlacementEvent, self.stopPlacingTargetFiducial)
    else:
      interactionNode.SetCurrentInteractionMode(slicer.vtkMRMLInteractionNode.ViewTransform)

  # ------------------------------------------------------
  def stopPlacingTargetFiducial(self, interactionNode, event):
    workflowFunctions.get(self.widget, "PlaceTargetFiducialPushButton").checked = False;


  # ------------------------------------------------------
  def placeEntryFiducial(self, place):
    interactionNode =  slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    if place:
      self.stopPlacingTargetFiducial(interactionNode, None)
      self.markupsLogic.SetActiveListID(self.entryMarkerNode())
      self.entryMarkerNode().RemoveAllMarkups()
      self.markupsLogic.StartPlaceMode(False)
      workflowFunctions.addObserver(self, interactionNode, interactionNode.EndPlacementEvent, self.stopPlacingEntryFiducial)
    else:
      interactionNode.SetCurrentInteractionMode(slicer.vtkMRMLInteractionNode.ViewTransform)

  # ------------------------------------------------------
  def entryFromNeedleTip(self):
      # Get the matrix of the tracker
    trackerNode = workflowFunctions.getTracker()
    transformMatrix = vtk.vtkMatrix4x4()
    trackerNode.GetMatrixTransformToWorld(transformMatrix)
    tipPos = [0.,0.,0.,1.]
    transformMatrix.MultiplyPoint(tipPos, tipPos)

    interactionNode =  slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    self.stopPlacingTargetFiducial(interactionNode, None)
    self.stopPlacingEntryFiducial(interactionNode, None)
    interactionNode.SetCurrentInteractionMode(interactionNode.ViewTransform)
    self.markupsLogic.SetActiveListID(self.entryMarkerNode())
    self.entryMarkerNode().RemoveAllMarkups()
    self.entryMarkerNode().AddFiducial(tipPos[0], tipPos[1], tipPos[2])

  # ------------------------------------------------------
  def stopPlacingEntryFiducial(self, interactionNode, event):
    workflowFunctions.get(self.widget, "PlaceEntryFiducialPushButton").checked = False;


  # ------------------------------------------------------
  def onTargetMarkerModified(self, markerNode, event):
    if markerNode.GetNumberOfFiducials() == 0:
      return
    # Only the 1st fiducial is being used.
    fiducialId = 0
    # Update the RAS coordinates
    position = [0.,0.,0.]
    markerNode.GetNthFiducialPosition(fiducialId, position)
    buttonText = '({:.2f}, {:.2f}, {:.2f})'.format( position[0], position[1], position[2] )
    workflowFunctions.get(self.widget, "TargetFiducialRASPushButton").text = buttonText
    markerNode.SetNthMarkupLabel(fiducialId, "Target")
    markerNode.GetDisplayNode().SetColor(1., 0., 0.)
    markerNode.GetDisplayNode().SetSelectedColor(1., 0., 0.)
    markerNode.GetDisplayNode().SetGlyphScale(10)
    markerNode.GetDisplayNode().SetTextScale(5)
    markerNode.GetDisplayNode().SliceProjectionOutlinedBehindSlicePlaneOff()
    self.updateRuler()


  # ------------------------------------------------------
  def onEntryMarkerModified(self, markerNode, event):
    if markerNode.GetNumberOfFiducials() == 0:
      return
    # Only the 1st fiducial is being used.
    fiducialId = 0
    # Update the RAS coordinates
    position = [0.,0.,0.]
    # Only the 1st fiducial is being used.
    markerNode.GetNthFiducialPosition(fiducialId, position) #GetNumberOfFiducials
    buttonText = '({:.2f}, {:.2f}, {:.2f})'.format( position[0], position[1], position[2] )
    workflowFunctions.get(self.widget, "EntryFiducialRASPushButton").text = buttonText
    markerNode.SetNthMarkupLabel(fiducialId, "Entry") #GetNumberOfFiducials
    markerNode.GetDisplayNode().SetColor(0., 0., 1.)
    markerNode.GetDisplayNode().SetSelectedColor(0., 0., 1.)
    markerNode.GetDisplayNode().SetGlyphScale(5)
    markerNode.GetDisplayNode().SetTextScale(5)
    self.updateRuler()


  # ------------------------------------------------------
  def jumpToTargetFiducial(self):
    self.markupsLogic.JumpSlicesToNthPointInMarkup(
      self.targetMarkerNode().GetID(), 0, False)


  # ------------------------------------------------------
  def jumpToEntryFiducial(self):
    self.markupsLogic.JumpSlicesToNthPointInMarkup(
      self.entryMarkerNode().GetID(), 0, False)


  # ------------------------------------------------------
  def updateRuler(self):
    # Can a ruler node be created ?
    if (self.targetMarkerNode().GetNumberOfFiducials() == 0 or
        self.entryMarkerNode().GetNumberOfFiducials() == 0):
      slicer.mrmlScene.RemoveNode(self.trajectoryMarkerNode())
      return

    fiducialId = 0
    entryPosition = [0.,0.,0.]
    self.entryMarkerNode().GetNthFiducialPosition(fiducialId, entryPosition)
    targetPosition = [0.,0.,0.]
    self.targetMarkerNode().GetNthFiducialPosition(fiducialId, targetPosition)

    if not self.trajectoryMarkerNode(): # Create the ruler if none exist yet
      self.createTrajectoryRulerNode(entryPosition, targetPosition)
    else: # otherwise just update its position
      self.trajectoryMarkerNode().SetPosition1(entryPosition)
      self.trajectoryMarkerNode().SetPosition2(targetPosition)

    pathText = '{:.2f} mm'.format( self.trajectoryDistance() )
    workflowFunctions.get(self.widget, "PathDistanceLabel").text = pathText

  # ------------------------------------------------------
  def trajectoryMarkerNode(self):
    rulerNode = slicer.mrmlScene.GetFirstNodeByName("Path")
    return rulerNode

  # ------------------------------------------------------
  def createTrajectoryRulerNode(self, entryPosition, targetPosition):
    rulerNode = slicer.vtkMRMLAnnotationRulerNode()
    rulerNode.SetName('Path')
    rulerNode.SetPosition1(entryPosition)
    rulerNode.SetPosition2(targetPosition)
    rulerNode.Initialize(slicer.mrmlScene)
    rulerNode.SetLocked(1)
    rulerDisplayNode = rulerNode.GetDisplayNode()
    rulerDisplayNode.SetColor(1., 1., 0.)
    rulerDisplayNode.SetLineThickness(3)
    rulerDisplayNode.SetLineWidth(3.0)
    rulerDisplayNode.SetMaxTicks(0)
    rulerDisplayNode.SetProjectedColor(1., 0.5, 0.) #orange
    rulerDisplayNode.SetProjectedOpacity(0.9)
    rulerDisplayNode.SliceProjectionOn()
    rulerDisplayNode.SliceProjectionDashedOff()
    rulerDisplayNode.SliceProjectionUseRulerColorOff()
    rulerDisplayNode.SliceProjectionColoredWhenParallelOff()
    rulerDisplayNode.SetOverLineThickness(2.5)
    rulerDisplayNode.SetUnderLineThickness(2.0)
    rulerDisplayNode.SetLabelPosition(0.5)
    rulerDisplayNode.SetOpacity(1.0)
    rulerDisplayNode.SetLabelVisibility(0)
    rulerNode.GetAnnotationPointDisplayNode().SetOpacity(0) # disables cross-hairs
    rulerNode.GetAnnotationTextDisplayNode().SetColor(1.,1.,0)
    return rulerNode

  # ------------------------------------------------------
  def trajectoryDistance(self):
    if (self.targetMarkerNode().GetNumberOfFiducials() == 0 or
        self.entryMarkerNode().GetNumberOfFiducials() == 0):
      return 0
    fiducialId = 0
    entryPosition = [0.,0.,0.]
    self.entryMarkerNode().GetNthFiducialPosition(fiducialId, entryPosition)
    targetPosition = [0.,0.,0.]
    self.targetMarkerNode().GetNthFiducialPosition(fiducialId, targetPosition)
    pathNormal = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, entryPosition, pathNormal)
    distance = vtk.vtkMath.Norm(pathNormal)
    return distance

  def setTargetSize(self, size):
    node = self.targetMarkerNode()
    displayNode = node.GetDisplayNode()
    displayNode.SetGlyphScale(size)

  def setTargetOpacity(self, opacity):
    node = self.targetMarkerNode()
    displayNode = node.GetDisplayNode()
    displayNode.SetOpacity(opacity)
    displayNode.SetSliceProjectionOpacity(opacity)

  # ------------------------------------------------------
  def setGuidanceThickness(self, thickness):
    # has a ruler node be created ?
    if not self.trajectoryMarkerNode():
      return
    displayNode = self.trajectoryMarkerNode().GetDisplayNode()
    displayNode.SetLineThickness(thickness)
    displayNode.SetLineWidth(thickness)
    displayNode.SetOverLineThickness(thickness*0.8)
    displayNode.SetUnderLineThickness(thickness*0.65)

  # ------------------------------------------------------
  def pathTransform(self):
    entryPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition1(entryPosition)
    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)
    pathNormal = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, entryPosition, pathNormal)
    vtk.vtkMath.Normalize(pathNormal)
    return workflowFunctions.transformFromNormal(pathNormal, targetPosition, False)

  # ------------------------------------------------------
  def pathSliceTransforms(self):
    entryPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition1(entryPosition)
    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)
    pathNormal = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, entryPosition, pathNormal)
    vtk.vtkMath.Normalize(pathNormal)
    return workflowFunctions.transformsFromNormal(pathNormal, targetPosition)

  # ------------------------------------------------------
  def reslice(self, enable):
    redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    yellowSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
    greenSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
    if (enable):
      transforms = self.pathSliceTransforms()
      redSlice.SetOrientationToReformat()
      redSlice.GetSliceToRAS().DeepCopy( transforms[2].GetMatrix() )
      redSlice.UpdateMatrices()
      #transform.RotateX( -90 )
      #transform.Update()
      yellowSlice.SetOrientationToReformat()
      yellowSlice.GetSliceToRAS().DeepCopy( transforms[0].GetMatrix() )
      yellowSlice.UpdateMatrices()
      #transform.RotateY( 90 )
      #transform.Update()
      greenSlice.SetOrientationToReformat()
      greenSlice.GetSliceToRAS().DeepCopy( transforms[1].GetMatrix() )
      greenSlice.UpdateMatrices()
    else:
      redSlice.SetOrientationToAxial()
      yellowSlice.SetOrientationToSagittal()
      greenSlice.SetOrientationToCoronal()

    redLogic = slicer.app.layoutManager().sliceWidget("Red").sliceLogic()
    yellowLogic = slicer.app.layoutManager().sliceWidget("Yellow").sliceLogic()
    greenLogic = slicer.app.layoutManager().sliceWidget("Green").sliceLogic()

    redLogic.FitSliceToAll(-1,-1)
    yellowLogic.FitSliceToAll(-1,-1)
    greenLogic.FitSliceToAll(-1,-1)

    self.jumpToTargetFiducial()

  # ------------------------------------------------------
  def setPETVolumeAsForeground(self):
    petVolumeNode = slicer.util.getNode('PETVolume')
    if petVolumeNode != None:
      workflowFunctions.setActiveVolume(petVolumeNode, 1, 0.5)
