﻿import qt, ctk, vtk, slicer
import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTRegistrationStep( ctk.ctkWorkflowWidgetStep ) :

  def __init__( self, stepid ):
    self.initialize( stepid )
    self.setName( '2. Registration' )
    self.setDescription('Register Motion compensated CT-CBCT')
    self.CLINode = None
    self.Observations = []
    qt.QTimer.singleShot(0, self.killButton)


  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()


  def createUserInterface( self ):
    layout =  qt.QVBoxLayout(self)

    #Get widget from ui file and set to layout
    self.widget = workflowFunctions.loadUI('GuidedInterventionPETCTRegistration.ui');
    self.CLIProgressBar = slicer.qSlicerCLIProgressBar()
    layout.addWidget(self.widget)
    layout.addWidget(self.CLIProgressBar)
    workflowFunctions.setScene(self.widget)
    workflowFunctions.get(self.widget,"RegistrationButton").connect('clicked()',self.rigidRegistration)
    workflowFunctions.get(self.widget,"xTranslation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"yTranslation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"zTranslation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"xRotation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"yRotation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"zRotation").connect('valueChanged(int)',self.buildInitialTransform)
    workflowFunctions.get(self.widget,"SpatialSamplesSliderWidget").setDecimals(4)
    self.setSliderMaximums()

  def validate( self, desiredBranchId ):

    if self.CLINode is None:
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Please start registration process' )
      validation = False
    else:
      CLIstatus = self.CLINode.GetStatusString()
      validation = True
      if CLIstatus != "Completed":
        messageBox = qt.QMessageBox.warning( self, 'Error', 'Please wait end of registration' )
        validation = False
    super( GuidedInterventionPETCTRegistrationStep, self ).validate(validation,desiredBranchId)
    self.setCBCTVolumeAsBackground()


  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"

    self.PETVolume = slicer.mrmlScene.GetFirstNodeByName("PETVolume")
    self.CTVolume = slicer.mrmlScene.GetFirstNodeByName("CTVolume")
    self.CBCTVolume = slicer.mrmlScene.GetFirstNodeByName("CBCTVolume")

    self.reg = slicer.mrmlScene.CreateNodeByClass("vtkMRMLLinearTransformNode")
    self.reg.SetName("imageRegistrationTransform")
    slicer.mrmlScene.AddNode(self.reg)
    self.CTVolume.SetAndObserveTransformNodeID(self.reg.GetID())
    self.PETVolume.SetAndObserveTransformNodeID(self.reg.GetID())
    workflowFunctions.setActiveVolume(self.CTVolume, 1, 0.5)

    # calculate extents so we can set proper translation slider sizes for manual initialization
    bounds=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    self.CBCTVolume.GetRASBounds(bounds)
    cbct0=[bounds[0], bounds[2], bounds[4], 0.0] # origin corner
    cbct1=[bounds[1], bounds[3], bounds[5], 0.0] # opposite corner
    self.cbctCenter=[(cbct0[0]+cbct1[0])/2, (cbct0[1]+cbct1[1])/2, (cbct0[2]+cbct1[2])/2]

    self.CTVolume.GetRASBounds(bounds)
    ct0=[bounds[0], bounds[2], bounds[4], 0.0] # origin corner
    ct1=[bounds[1], bounds[3], bounds[5], 0.0] # opposite corner
    self.ctCenter=[(ct0[0]+ct1[0])/2, (ct0[1]+ct1[1])/2, (ct0[2]+ct1[2])/2, 0.0]

    initialTranslation = vtk.vtkMatrix4x4()
    initialTranslation.Identity()
    for i in range(0,3):
        initialTranslation.SetElement(i,3, self.cbctCenter[i]-self.ctCenter[i])
    self.reg.SetMatrixTransformToParent(initialTranslation)

    xHalf=min(ct1[0]-ct0[0],cbct1[0]-cbct0[0])/2
    yHalf=min(ct1[1]-ct0[1],cbct1[1]-cbct0[1])/2
    zHalf=min(ct1[2]-ct0[2],cbct1[2]-cbct0[2])/2
    self.extentHalves=[xHalf, yHalf, zHalf]
    self.setSliderMaximums()

    if comingFrom: comingFromId = comingFrom.id()
    super(GuidedInterventionPETCTRegistrationStep, self).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)

  def buildInitialTransform(self, sliderValue):
    initialT = vtk.vtkTransform()
    initialT.PostMultiply()
    sliders=[workflowFunctions.get(self.widget,"xTranslation").value,
             workflowFunctions.get(self.widget,"yTranslation").value,
             workflowFunctions.get(self.widget,"zTranslation").value]
    initialT.Translate(-self.ctCenter[0], -self.ctCenter[1], -self.ctCenter[2])
    initialT.RotateX(workflowFunctions.get(self.widget,"xRotation").value)
    initialT.RotateY(workflowFunctions.get(self.widget,"yRotation").value)
    initialT.RotateZ(workflowFunctions.get(self.widget,"zRotation").value)
    initialT.Translate(self.cbctCenter)
    initialT.Translate(sliders)
    self.reg.SetMatrixTransformToParent(initialT.GetMatrix())


  def setSliderMaximums(self):
    if hasattr(self, 'widget'):
        workflowFunctions.get(self.widget,"xTranslation").minimum=-self.extentHalves[0]
        workflowFunctions.get(self.widget,"xTranslation").maximum=self.extentHalves[0]
        workflowFunctions.get(self.widget,"xTranslation").value=0
        workflowFunctions.get(self.widget,"yTranslation").minimum=-self.extentHalves[1]
        workflowFunctions.get(self.widget,"yTranslation").maximum=self.extentHalves[1]
        workflowFunctions.get(self.widget,"yTranslation").value=0
        workflowFunctions.get(self.widget,"zTranslation").minimum=-self.extentHalves[2]
        workflowFunctions.get(self.widget,"zTranslation").maximum=self.extentHalves[2]
        workflowFunctions.get(self.widget,"zTranslation").value=0

  def onExit(self, goingTo, transitionType):
    goingToId = "None"
    if goingTo: goingToId = goingTo.id()
    # execute the transition
    super(GuidedInterventionPETCTRegistrationStep, self).onExit(goingTo, transitionType)

  def downsampleVolumes(self, spacing):
    # print("downsampleVolumes: ", spacing)
    rsv=slicer.modules.resamplescalarvolume
    rsvPar={}
    rsvPar["interpolationType"]="linear"
    rsvPar["outputPixelSpacing"]=spacing
    rsvPar["InputVolume"]=self.CTVolume
    rsvPar["OutputVolume"]=self.tempCT
    slicer.cli.run(rsv, None, rsvPar, True)
    rsvPar["InputVolume"]=self.CBCTVolume
    rsvPar["OutputVolume"]=self.tempCBCT
    slicer.cli.run(rsv, None, rsvPar, True)

  def rigidRegistration(self):
    self.tempCT=slicer.mrmlScene.CreateNodeByClass("vtkMRMLScalarVolumeNode")
    self.tempCT.SetName("tempCT")
    slicer.mrmlScene.AddNode(self.tempCT)
    self.tempCBCT=slicer.mrmlScene.CreateNodeByClass("vtkMRMLScalarVolumeNode")
    self.tempCBCT.SetName("tempCBCT")
    slicer.mrmlScene.AddNode(self.tempCBCT)
    self.downsampleVolumes("3,3,4")

    self.rigidPar={}
    self.rigidPar["initializeTransformMode"] = "Off"
    self.rigidPar["initialTransform"]=self.reg
    self.rigidPar["numberOfIterations"] = 10000
    self.rigidPar["samplingPercentage"] = workflowFunctions.get(self.widget,"SpatialSamplesSliderWidget").value / 100.0
    self.rigidPar["samplingPercentage"] = self.rigidPar["samplingPercentage"] * 5
    if self.rigidPar["samplingPercentage"]>1.0:
        self.rigidPar["samplingPercentage"]=1.0
    self.rigidPar["backgroundFillValue"] = -1000 # air
    self.rigidPar["costMetric"] = "MSE"
    self.rigidPar["fixedVolume"] = self.tempCBCT
    self.rigidPar["movingVolume"] = self.tempCT
    self.rigidPar["outputVolume"] = slicer.mrmlScene.CreateNodeByClass("vtkMRMLScalarVolumeNode")
    self.rigidPar["outputVolume"].SetName("RegistrationResult")
    slicer.mrmlScene.AddNode(self.rigidPar["outputVolume"])
    self.rigidPar["useRigid"] = True
    self.rigidPar["linearTransform"] = self.reg
    self.CLINode = slicer.cli.createNode(slicer.modules.brainsfit)
    self.CLIProgressBar.setCommandLineModuleNode(self.CLINode)
    workflowFunctions.addObserver(self, self.CLINode, self.CLINode.StatusModifiedEvent, self.rigidPhase2)
    slicer.cli.run(slicer.modules.brainsfit, self.CLINode, self.rigidPar, wait_for_completion = False)

  def rigidPhase2(self, cliNode, event):
    if not cliNode.IsBusy():
      workflowFunctions.removeObservers(self, self.rigidPhase2)

      if cliNode.GetStatusString() == 'Completed' and workflowFunctions.get(self.widget,"preciseRigidPhase").checked:
        self.downsampleVolumes("2,2,3")
        self.setCBCTVolumeAsBackground()
        self.rigidPar["fixedVolume"] = self.tempCBCT
        self.rigidPar["movingVolume"] = self.tempCT
        self.CLINode = slicer.cli.createNode(slicer.modules.brainsfit)
        self.CLIProgressBar.setCommandLineModuleNode(self.CLINode)
        workflowFunctions.addObserver(self, self.CLINode, self.CLINode.StatusModifiedEvent, self.onRigidRegistrationCompleted)
        slicer.cli.run(slicer.modules.brainsfit, self.CLINode, self.rigidPar, wait_for_completion = False)
      elif cliNode.GetStatusString() == 'Completed' and workflowFunctions.get(self.widget,"BSplineGroupBox").checked:
        self.bSplineRegistration()
      else:
        self.deformableRegistrationCompleted(cliNode, event)


  def onRigidRegistrationCompleted(self, cliNode, event):
    if not cliNode.IsBusy():
      workflowFunctions.removeObservers(self, self.onRigidRegistrationCompleted)
      self.setCBCTVolumeAsBackground()
      if cliNode.GetStatusString() == 'Completed' and workflowFunctions.get(self.widget,"BSplineGroupBox").checked:
        self.bSplineRegistration()
      else:
        self.deformableRegistrationCompleted(cliNode, event)


  def bSplineRegistration(self):
    BSplineParameters = {}
    BSplineParameters["initialTransform"]=self.reg
    BSplineParameters["initialTransform"].SetName("initialRegistrationTransform")
    BSplineParameters["numberOfIterations"] = 1000
    BSplineParameters["samplingPercentage"] = workflowFunctions.get(self.widget,"SpatialSamplesSliderWidget").value / 100.0
    # BSplineParameters["samplingPercentage"] = BSplineParameters["samplingPercentage"] * 5
    # if BSplineParameters["samplingPercentage"] > 1.0:
        # BSplineParameters["samplingPercentage"] = 1.0
    BSplineParameters["backgroundFillValue"] = -1000 # air
    BSplineParameters["fixedVolume"] = self.tempCBCT
    BSplineParameters["movingVolume"] = self.tempCT
    BSplineParameters["outputVolume"] = slicer.mrmlScene.GetFirstNodeByName("RegistrationResult")

    BSplineParameters["transformType"] = "BSpline"
    BSplineParameters["bsplineTransform"] = slicer.mrmlScene.CreateNodeByClass("vtkMRMLBSplineTransformNode")
    BSplineParameters["bsplineTransform"].SetName("imageRegistrationTransform")
    slicer.mrmlScene.AddNode(BSplineParameters["bsplineTransform"])
    scalarGS = int(workflowFunctions.get(self.widget,"GridSizeSliderWidget").value)
    BSplineParameters["splineGridSize"] = str(scalarGS) + "," + str(scalarGS) + "," + str(scalarGS)
    # BSplineParameters["numberOfHistogramBins"] = 20
    BSplineParameters["maxBSplineDisplacement"] = 25
    BSplineParameters["costFunctionConvergenceFactor"] = 10
    BSplineParameters["costMetric"] = "MSE"

    #slicer.cli.removeNode(self.CLINode)
    self.CLINode = slicer.cli.createNode(slicer.modules.brainsfit)
    self.CLIProgressBar.setCommandLineModuleNode(self.CLINode)
    # on success, apply the same transform to the PET volume
    workflowFunctions.addObserver(self, self.CLINode, self.CLINode.StatusModifiedEvent, self.deformableRegistrationCompleted)
    # print ("BSplineParameters: ", BSplineParameters)
    slicer.cli.run(slicer.modules.brainsfit, self.CLINode, BSplineParameters, wait_for_completion = False)


  def deformableRegistrationCompleted(self, cliNode, event):
    if not cliNode.IsBusy():
      workflowFunctions.removeObservers(self, self.deformableRegistrationCompleted)
      id=slicer.mrmlScene.GetFirstNodeByName("imageRegistrationTransform").GetID()
      self.CTVolume.SetAndObserveTransformNodeID(id)
      self.PETVolume.SetAndObserveTransformNodeID(id)
      slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetFirstNodeByName("initialRegistrationTransform"))
      slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetFirstNodeByName("tempCBCT"))
      slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetFirstNodeByName("tempCT"))
      self.setCBCTVolumeAsBackground()

  def setCBCTVolumeAsBackground(self):
    if self.CBCTVolume != None:
      workflowFunctions.setActiveVolume(self.CBCTVolume)
