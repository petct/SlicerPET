import os
import qt, ctk, vtk, slicer

import workflowFunctions as workflowFunctions


class GuidedInterventionPETCTRegisterEMCBCTStep( ctk.ctkWorkflowWidgetStep ) :

  # ------------------------------------------------------
  def __init__( self, stepid, markupsGUI):
    self.initialize( stepid )
    # \todo not needed anymore as parameter
    self.markupsGUI = markupsGUI

    self.markupsLogic = self.markupsGUI.module().logic()

    self.setName( '5. Register EM and CBCT' )
    self.setDescription("""Step to register EM Hardware and CBCT Spaces.

First select an ordered set of points to be used for registration using the tracker device.
Next specify the points from the CBCT image that correspond to the tracking points.
""")

    qt.QTimer.singleShot(0, self.killButton)
    self.Observations = []


  # ------------------------------------------------------
  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()


  # ------------------------------------------------------
  def createUserInterface( self ):
    self.createMarkerNodes()

    # Create widget from UI file
    layout = qt.QVBoxLayout(self)
    self.widget = workflowFunctions.loadUI('GuidedInterventionPETCTRegisterEMCBCT.ui')
    layout.addWidget(self.widget)
    workflowFunctions.setScene(self.widget)

    workflowFunctions.get(self.widget, "TrackerFiducialControlWidget").visible = False
    workflowFunctions.get(self.widget, "PhantomFiducialControlWidget").visible = False

    # set icons
    deleteIcon = self.widget.style().standardIcon(qt.QStyle.SP_TrashIcon)
    moveUpIcon = self.widget.style().standardIcon(qt.QStyle.SP_ArrowUp)
    moveDownIcon = self.widget.style().standardIcon(qt.QStyle.SP_ArrowDown)

    workflowFunctions.get(self.widget, "DeleteTrackerFiducialToolButton").icon = deleteIcon
    workflowFunctions.get(self.widget, "MoveUpTrackerFiducialToolButton").icon = moveUpIcon
    workflowFunctions.get(self.widget, "MoveDownTrackerFiducialToolButton").icon = moveDownIcon
    workflowFunctions.get(self.widget, "DeletePhantomFiducialToolButton").icon = deleteIcon
    workflowFunctions.get(self.widget, "MoveUpPhantomFiducialToolButton").icon = moveUpIcon
    workflowFunctions.get(self.widget, "MoveDownPhantomFiducialToolButton").icon = moveDownIcon

    # Link buttons to functions
    ## Tracker fiducials
    workflowFunctions.get(self.widget, "AddTrackerFiducialPushButton").connect('clicked()', self.addTrackerFiducial)
    workflowFunctions.get(self.widget, "DeleteTrackerFiducialToolButton").connect('clicked()', self.deleteTrackerFiducial)
    workflowFunctions.get(self.widget, "MoveUpTrackerFiducialToolButton").connect('clicked()', self.moveUpTrackerFiducial)
    workflowFunctions.get(self.widget, "MoveDownTrackerFiducialToolButton").connect('clicked()', self.moveDownTrackerFiducial)
    workflowFunctions.get(self.widget, "TrackerListWidget").connect('currentRowChanged(int)', self.onTrackerCurrentRowChanged)

    ## Phantom fiducials
    workflowFunctions.get(self.widget, "AddPhantomFiducialPushButton").connect('clicked(bool)', self.addPhantomFiducial)
    workflowFunctions.get(self.widget, "DeletePhantomFiducialToolButton").connect('clicked()', self.deletePhantomFiducial)
    workflowFunctions.get(self.widget, "MoveUpPhantomFiducialToolButton").connect('clicked()', self.moveUpPhantomFiducial)
    workflowFunctions.get(self.widget, "MoveDownPhantomFiducialToolButton").connect('clicked()', self.moveDownPhantomFiducial)
    workflowFunctions.get(self.widget, "PhantomListWidget").connect('currentRowChanged(int)', self.onPhantomCurrentRowChanged)

    ## Register
    workflowFunctions.get(self.widget, "RegisterFiducialsPushButton").connect('clicked()', self.startRegistration)

    workflowFunctions.get(self.widget, "RegistrationResultsTextEdit").visible = False
    self.onTrackerCurrentRowChanged(-1)
    self.onPhantomCurrentRowChanged(-1)

  # ------------------------------------------------------
  def validate( self, desiredBranchId ):
    # When going to the next step apply the calibration transform to the tracker transform
    pivotCalibrationNode = slicer.mrmlScene.GetFirstNodeByName("CalibrationResultTransform")
    trackerNode = workflowFunctions.getTracker()
    if trackerNode:
      trackerNode.SetAndObserveTransformNodeID(pivotCalibrationNode.GetID())

    registrationTransformNode = slicer.mrmlScene.GetFirstNodeByName('registrationTransform')
    if registrationTransformNode != None:
      pivotCalibrationNode.SetAndObserveTransformNodeID(registrationTransformNode.GetID())
    else:
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Please Register the tracker with the image first.' )
    super( GuidedInterventionPETCTRegisterEMCBCTStep, self ).validate(registrationTransformNode != None, desiredBranchId)
    #super( GuidedInterventionPETCTRegisterEMCBCTStep, self ).validate(True, desiredBranchId)

  def connectionStatusOK(self):
    connector = slicer.mrmlScene.GetFirstNodeByName("IGTLConnector")
    if (not connector is None ) and (connector.GetState() == connector.STATE_CONNECTED) :
      return True
    else:
      return False

  # ------------------------------------------------------
  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super( GuidedInterventionPETCTRegisterEMCBCTStep, self ).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)
    # Show the hidden fiducials when re-entering the module
    self.setPhantomFiducialsVisibility(True)


  # ------------------------------------------------------
  def onExit(self, goingTo, transitionType):
    super( GuidedInterventionPETCTRegisterEMCBCTStep, self ).onExit(goingTo, transitionType)
    # Hide the fiducials when leaving the module, they would cluter the views.
    self.setPhantomFiducialsVisibility(False)

  # ------------------------------------------------------
  # Create annotation hierarchy to prepare to add fiducials in two list
  def createMarkerNodes(self):
    # --- for coordinates in CT image space
    self.createCTMarkerNode()
    # --- for coordinates in Tracker space
    self.createTrackerMarkerNode()

  # ------------------------------------------------------
  def createCTMarkerNode(self):
    markerNode = slicer.mrmlScene.GetNodeByID(self.markupsLogic.AddNewFiducialNode("CTMarkers"))
    workflowFunctions.addObserver(self, markerNode, 'ModifiedEvent', self.updatePhantomWidgetList)
    workflowFunctions.addObserver(self, markerNode, 'DeleteEvent', self.updatePhantomWidgetList)
    return markerNode

  # ------------------------------------------------------
  def createTrackerMarkerNode(self):
    markerNode =  slicer.mrmlScene.GetNodeByID(self.markupsLogic.AddNewFiducialNode("TrackerMarkers"))
    workflowFunctions.addObserver(self, markerNode, 'ModifiedEvent', self.updateTrackerWidgetList)
    workflowFunctions.addObserver(self, markerNode, 'DeleteEvent', self.updateTrackerWidgetList)
    return markerNode

  # ------------------------------------------------------
  def ctMarkerNode(self):
    markerNode = slicer.mrmlScene.GetFirstNodeByName("CTMarkers")
    if markerNode is None:
      markerNode =  self.createCTMarkerNode()
    return markerNode

  # ------------------------------------------------------
  def trackerMarkerNode(self):
    markerNode = slicer.mrmlScene.GetFirstNodeByName("TrackerMarkers")
    if markerNode is None:
      markerNode =  self.createTrackerMarkerNode()
    return markerNode

  # ------------------------------------------------------
  # Creating fiducials according to the position of the tracker device.
  #    This proc is entered as a result of the GUI button press.
  #
  # The coordinate is read from the tracker device and added as a
  # "fudicialNode" to our scene.
  #
  def updateTrackerWidgetList(self, trackerNode, event):
    listWidget = workflowFunctions.get(self.widget, "TrackerListWidget")
    self.updateMarkerWidgetList(listWidget, trackerNode)

  # ------------------------------------------------------
  def addTrackerFiducial(self):
    # create "TrackerMarkers" node if not already there
    # There should always be a "TrackerMarkers" node. Created in createMarkerNodes()
    markerNode = self.trackerMarkerNode()

    # make sure tracker is on-line
    if not self.connectionStatusOK() :
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Tracker is not connected.' )
      return

    # Use the TrackerMarkers fiducial list
    self.markupsLogic.SetActiveListID(markerNode)

    # Get the matrix of the tracker
    self.trackerNode = workflowFunctions.getTracker()
    transformMatrix = vtk.vtkMatrix4x4()
    self.trackerNode.GetMatrixTransformToParent(transformMatrix)

    # Get the matrix from the tracker marker to the tip of the tracker (result of pivot calibration)
    pivotCalibrationNode = slicer.mrmlScene.GetFirstNodeByName("CalibrationResultTransform")
    pivotCalibrationMatrix = vtk.vtkMatrix4x4()
    pivotCalibrationNode.GetMatrixTransformToParent(pivotCalibrationMatrix)

    # Apply the pivot calibration transform to the current position of the tracker
    resultingTransform = slicer.vtkMRMLLinearTransformNode()
    resultingTransform.ApplyTransformMatrix(transformMatrix)
    resultingTransform.ApplyTransformMatrix(pivotCalibrationMatrix)

    resultingMatrix = vtk.vtkMatrix4x4()
    resultingTransform.GetMatrixTransformToParent(resultingMatrix)
    x = resultingMatrix.GetElement(0, 3)
    y = resultingMatrix.GetElement(1, 3)
    z = resultingMatrix.GetElement(2, 3)

    # Add a new fiducial to the currently active list (tracker markers)
    self.markupsLogic.AddFiducial( x, y, z )
    # Hide the new fiducials (all the tracker fiducials should be hidden)
    self.setFiducialsVisibility(markerNode, False)

    # Update current row
    listWidget = workflowFunctions.get(self.widget, "TrackerListWidget")
    listWidget.currentRow = listWidget.count - 1

  def deleteTrackerFiducial(self):
    listWidget = workflowFunctions.get(self.widget, "TrackerListWidget")
    markerNode = self.trackerMarkerNode()
    self.deleteFiducial(markerNode, listWidget.currentRow)

  def moveUpTrackerFiducial(self):
    self.moveTrackerFiducial(-1)
  def moveDownTrackerFiducial(self):
    self.moveTrackerFiducial(1)

  def moveTrackerFiducial(self, offset):
    listWidget = workflowFunctions.get(self.widget, "TrackerListWidget")
    markerNode = self.trackerMarkerNode()
    self.moveFiducial(markerNode, listWidget.currentRow, listWidget.currentRow+offset)
    listWidget.currentRow = listWidget.currentRow + offset

  def onTrackerCurrentRowChanged(self, row):
    enableFiducialAction = (row >= 0)
    workflowFunctions.get(self.widget, "DeleteTrackerFiducialToolButton").enabled = enableFiducialAction;
    workflowFunctions.get(self.widget, "MoveUpTrackerFiducialToolButton").enabled = enableFiducialAction;
    workflowFunctions.get(self.widget, "MoveDownTrackerFiducialToolButton").enabled = enableFiducialAction;

  # ------------------------------------------------------
  def updatePhantomWidgetList(self, phantomNode, event):
    listWidget = workflowFunctions.get(self.widget, "PhantomListWidget")
    self.updateMarkerWidgetList(listWidget, phantomNode)

  def addPhantomFiducial(self, place):
    interactionNode =  slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    if place:
      markerNode = self.ctMarkerNode()
      self.markupsLogic.SetActiveListID(markerNode)
      self.markupsLogic.StartPlaceMode(False)
      workflowFunctions.addObserver(self, interactionNode, interactionNode.EndPlacementEvent, self.stopAddPhantomFiducial)
    else:
      interactionNode.SetCurrentInteractionMode(slicer.vtkMRMLInteractionNode.ViewTransform)

  def stopAddPhantomFiducial(self, interactionNode, event):
    workflowFunctions.get(self.widget, "AddPhantomFiducialPushButton").checked = False;

  def deletePhantomFiducial(self):
    listWidget = workflowFunctions.get(self.widget, "PhantomListWidget")
    markerNode = self.ctMarkerNode()
    self.deleteFiducial(markerNode, listWidget.currentRow)

  def moveUpPhantomFiducial(self):
    self.movePhantomFiducial(-1)
  def moveDownPhantomFiducial(self):
    self.movePhantomFiducial(1)

  def movePhantomFiducial(self, offset):
    listWidget = workflowFunctions.get(self.widget, "PhantomListWidget")
    markerNode = self.ctMarkerNode()
    self.moveFiducial(markerNode, listWidget.currentRow, listWidget.currentRow+offset)
    listWidget.currentRow = listWidget.currentRow + offset

  def onPhantomCurrentRowChanged(self, row):
    markerNode = self.ctMarkerNode()
    if markerNode and row>=0:
      self.markupsLogic.JumpSlicesToNthPointInMarkup(markerNode.GetID(), row, False)

    enableFiducialAction = (row >= 0)
    workflowFunctions.get(self.widget, "DeletePhantomFiducialToolButton").enabled = enableFiducialAction;
    workflowFunctions.get(self.widget, "MoveUpPhantomFiducialToolButton").enabled = enableFiducialAction;
    workflowFunctions.get(self.widget, "MoveDownPhantomFiducialToolButton").enabled = enableFiducialAction;

  # ------------------------------------------------------
  def updateMarkerWidgetList(self, listWidget, markerNode):
    previousCurrentRow = listWidget.currentRow
    numberOfFiducials = 0
    if markerNode and markerNode.GetScene():
      numberOfFiducials = markerNode.GetNumberOfFiducials()
    wasPointAdded = (listWidget.count < numberOfFiducials)

    listWidget.clear()
    for i in range(numberOfFiducials):
      position = [0.,0.,0.]
      markerNode.GetNthFiducialPosition(i, position)
      itemText = '({:.2f}, {:.2f}, {:.2f})'.format( position[0], position[1], position[2] )
      listWidget.addItem(itemText)

    if wasPointAdded:
      previousCurrentRow = listWidget.count - 1
    listWidget.currentRow = previousCurrentRow

  def deleteFiducial(self, markerNode, row):
    if markerNode:
      markerNode.RemoveMarkup(row)

  def moveFiducial(self, markerNode, oldRow, newRow):
    if markerNode:
      markerNode.SwapMarkups(oldRow, newRow)

  def setPhantomFiducialsVisibility(self, show):
    markerNode = self.ctMarkerNode()
    self.setFiducialsVisibility(markerNode, show)

  def setFiducialsVisibility(self, markerNode, show):
    if markerNode:
      self.markupsLogic.SetAllMarkupsVisibility(markerNode, show)

  # ------------------------------------------------------
  # Calls CLI module that runs fiducial registration
  #    Called from GUI button push
  #
  def startRegistration(self):

    # validate the point sets
    if not self.validPoints():
        messageBox = qt.QMessageBox.warning( self, 'Error', 'Too few points or lists have different length' )
        return

    # remove any old "registrationTransform" nodes
    transform = slicer.mrmlScene.GetFirstNodeByName('registrationTransform')
    if transform:
      slicer.mrmlScene.RemoveNode(transform)

    #todo make sure there are the same number of points in both lists

    fiducialRegistrationParameters = {}
    fiducialRegistrationParameters['movingLandmarks'] = self.trackerMarkerNode()
    fiducialRegistrationParameters['fixedLandmarks'] = self.ctMarkerNode()
    fiducialRegistrationParameters['saveTransform'] = slicer.mrmlScene.CreateNodeByClass('vtkMRMLLinearTransformNode')
    fiducialRegistrationParameters['saveTransform'].SetName('registrationTransform')
    slicer.mrmlScene.AddNode(fiducialRegistrationParameters['saveTransform'])
    fiducialRegistrationParameters['transformType'] = "Rigid"

    content = "Raw input points\n"
    content += self.formatPoints() # to help debug lists of points
    workflowFunctions.get(self.widget, "RegistrationResultsTextEdit").setPlainText( content )
    workflowFunctions.get(self.widget, "RegistrationResultsTextEdit").visible = True

    # call registration module (CLI Module)
    node  = slicer.cli.createNode( slicer.modules.fiducialregistration )
    slicer.cli.run( slicer.modules.fiducialregistration, node, fiducialRegistrationParameters, True )
    status = node.GetParameterAsString('outputMessage')
    rms = node.GetParameterAsString('rms')

    print "Registration results: ", status
    print "Registration rms: ", rms 

    if not status == 'Success':
      text = 'Registration operation failed: '
      text += status
      messageBox = qt.QMessageBox.warning( self, 'Error', text )
      return

    content = "\n-------------------------\nRegistration RMS=" + rms  + "\n" 
    workflowFunctions.get(self.widget, "RegistrationResultsTextEdit").append( content )

  # ------------------------------------------------------
  def validPoints(self):
    # Make sure the point sets we have to work with
    # there must be the same number of points in each list and
    # that number must be at least 4
    #
    # Returns TRUE if point sets are valid

    # Get all the fiducials of the TrackerMarkers list
    trackerNode = self.trackerMarkerNode()
    trackerCount = trackerNode.GetNumberOfFiducials()

    # Get all the fiducials of the CTMarkers list
    ctNode = self.ctMarkerNode()
    ctCount = ctNode.GetNumberOfFiducials()

    print "Tracker: %d fiducials" % trackerCount
    print "Phantom: %d fiducials" % ctCount
    return (trackerCount == ctCount) and (trackerCount >= 4)


  # ------------------------------------------------------
  def formatPoints(self):
    # Get all the fiducials of the TrackerMarkers list
    trackerNode = self.trackerMarkerNode()
    trackerCount = trackerNode.GetNumberOfFiducials()

    # Get all the fiducials of the CTMarkers list
    ctNode = self.ctMarkerNode()
    ctCount = ctNode.GetNumberOfFiducials()

    maxSize = max( trackerCount, ctCount )

    coord = [0, 0, 0]

    content = "Point number -- Tracker coord -- CT coord --\n"

    # --- iterate over all points in tracker list
    for row in xrange( maxSize ):
      # new label widget
      temp =  'pt {:d}   '.format( row+1 )
      content += temp

      if row < trackerCount :
        trackerNode.GetNthFiducialPosition( row, coord )
        temp =  'TR[{:.2f}, {:.2f}, {:.2f}]   '.format( coord[0], coord[1], coord[2] )
        content += temp
      else:
        content += '  ---- not available --- '

      if row < ctCount:
        ctNode.GetNthFiducialPosition( row, coord )
        temp =  'CT[{:.2f}, {:.2f}, {:.2f}]\n'.format( coord[0], coord[1], coord[2] )
        content += temp
      else:
        content += '  ---- not available --- \n'

    return content
