import os
import datetime
import csv
import qt, ctk, vtk, slicer

import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTGuidanceStep( ctk.ctkWorkflowWidgetStep ) :
  # ------------------------------------------------------
  def __init__( self, stepid ):
    self.initialize( stepid )
    # \todo not needed anymore as parameter
    #self.GuidanceGUI = GuidanceGUI

    self.markupsLogic = slicer.modules.markups.logic()
    self.annotationsLogic = slicer.modules.annotations.logic()

    self.setName( '7. Guidance' )
    self.setDescription('Simulating a path for an entry and target point')

    qt.QTimer.singleShot(0, self.killButton)
    self.Observations = []

    self.rotationAmount = 0

  # ------------------------------------------------------
  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()


  # ------------------------------------------------------
  def createUserInterface( self ):

    # Get the Guidance module
    #layout.addWidget(self.GuidanceGUI)
    #workflowFunctions.centerViews()

    # Create widget from UI file
    layout = qt.QVBoxLayout(self)
    self.widget = workflowFunctions.loadUI('GuidedInterventionPETCTGuidance.ui')
    layout.addWidget(self.widget)
    workflowFunctions.setScene(self.widget)

    # Link buttons to slots
    workflowFunctions.get(self.widget, "FollowTrackerCheckBox").connect('clicked()', self.reslice)
    workflowFunctions.get(self.widget, "NeedleLineThicknessSlider").connect('valueChanged(double)', self.setNeedleThickness)
    workflowFunctions.get(self.widget, "NeedleHubSizeSlider").connect('valueChanged(double)', self.setHubSize)
    workflowFunctions.get(self.widget, "NeedleHubOpacitySlider").connect('valueChanged(double)', self.setHubOpacity)
    workflowFunctions.get(self.widget, "NeedleTipSizeSlider").connect('valueChanged(double)', self.setNeedleTipSize)
    workflowFunctions.get(self.widget, "NeedleTipOpacitySlider").connect('valueChanged(double)', self.setNeedleTipOpacity)
    workflowFunctions.get(self.widget, "RotateBirdsEyeViewButton").connect('clicked()', self.rotateBirdsEyeView)
    workflowFunctions.get(self.widget, "SaveDataButton").connect('clicked()', self.saveData)
    self.createTrackerLayout()
    self.observeTracker()

    entryPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition1(entryPosition)
    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)
    trajectory = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, entryPosition, trajectory)
    self.trajectoryDistance = vtk.vtkMath.Norm(trajectory)
    workflowFunctions.get(self.widget, "DistanceToTargetProgressBar").maximum = self.trajectoryDistance

    hubNode = self.needleHub()
    displayNode = hubNode.GetDisplayNode()
    displayNode.SetGlyphType(13) # Sphere3D
    tipNode = self.needleTip()
    displayNode = tipNode.GetDisplayNode()
    displayNode.SetGlyphType(13) # 12=StarBurst2D

  # ------------------------------------------------------
  def validate( self, desiredBranchId ):
    super( GuidedInterventionPETCTGuidanceStep, self ).validate(True,desiredBranchId)

  # ------------------------------------------------------
  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super( GuidedInterventionPETCTGuidanceStep, self ).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)
    self.setTrackerVisible(True)
    self.setTrackerLayout(True)
    # the blue slice view must be created before setting the pet volume
    self.setPETVolumeAsForeground()
    self.startTime=datetime.datetime.now() # we will use this later

  def makeRulersRed(self):
    # Update the DataProbe scale ruler color
    dataProbeInstance = slicer.modules.DataProbeInstance
    infoWidget = dataProbeInstance.infoWidget

    # if the data probe isn't instantiated yet, come back later
    if not infoWidget:
      qt.QTimer.singleShot(0, self.makeRulersRed)
      return

    sliceAnnotations = infoWidget.sliceAnnotations
    sliceViewNames = slicer.app.layoutManager().sliceViewNames()
    for sliceViewName in sliceViewNames:
      rulerActor = sliceAnnotations.rulerActors[sliceViewName]
      rulerActor.GetProperty().SetColor(1, 0, 0)
      textActor = sliceAnnotations.rulerTextActors[sliceViewName]
      textActor.GetProperty().SetColor(1, 0, 0)

  # ------------------------------------------------------
  def onExit(self, goingTo, transitionType):
    goingToId = "None"
    if goingTo: goingToId = goingTo.id()
    self.setTrackerVisible(False)
    self.setTrackerLayout(False)
    super( GuidedInterventionPETCTGuidanceStep, self ).onExit(goingTo, transitionType)

  # ------------------------------------------------------
  def rotateBirdsEyeView(self):
    self.rotationAmount += 90;
    if (self.rotationAmount >= 360):
      self.rotationAmount -= 360;
    self.reslice()

  # ------------------------------------------------------
  def saveData(self):
    fieldNames=['Entry Point', 'Target', 'Entry To Target', 'Needle Tip', 'Needle Tip To Target', 'Start Time', 'End Time']
    savePath = os.path.join(os.path.expanduser("~"),"SlicerPET")
    if os.path.isfile(savePath+'.csv'):
      csvFile=open(savePath+'.csv', 'ab')
      writer=csv.DictWriter(csvFile, fieldNames)
    else:
      csvFile=open(savePath+'.csv', 'wb')
      writer=csv.DictWriter(csvFile, fieldNames)
      writer.writeheader()
    row={}

    entryPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition1(entryPosition)
    row['Entry Point']=entryPosition

    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)
    row['Target']=targetPosition

    entryToTarget = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, entryPosition, entryToTarget)
    pathLength = vtk.vtkMath.Norm(entryToTarget)
    row['Entry To Target']=pathLength

    trackerTipPosition = self.trackerPosition()
    row['Needle Tip']=trackerTipPosition

    trackerToTarget = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, trackerTipPosition, trackerToTarget)
    trackerDistance = vtk.vtkMath.Norm(trackerToTarget)
    row['Needle Tip To Target']=trackerDistance

    row['Start Time']=self.startTime.isoformat(' ')
    endTime=datetime.datetime.now()
    row['End Time']=endTime.isoformat(' ')

    writer.writerow(row)
    csvFile.close()
    print("Data written to: ", savePath+'.csv')

    qp=qt.QPixmap()
    pngPath=savePath+endTime.strftime('_%Y-%m-%d_%H-%M-%S_%f')+'.png'
    qp.grabWidget(slicer.app.activeWindow()).save(pngPath)
    print("Screenshot written to: ", pngPath)

    scenePath=os.path.join(os.path.expanduser("~"),"SlicerPET",'scene_'+endTime.strftime('%Y-%m-%d_%H-%M-%S_%f'))
    # slicer.modules.sceneviews.showSceneViewDialog() # showing a dialog is annoying
    # slicer.util.saveScene(scenePath) # takes too long


  # ------------------------------------------------------
  def isTrackerActive(self):
    connectorNode = slicer.mrmlScene.GetFirstNodeByName("IGTLConnector")
    if connectorNode:
      incomingNode = connectorNode.GetIncomingMRMLNode(0)
      return incomingNode is not None
    return False

  # ------------------------------------------------------
  def setTrackerVisible(self, show):
    connectorNode = slicer.mrmlScene.GetFirstNodeByName("IGTLConnector")
    if connectorNode:
      incomingNode = connectorNode.GetIncomingMRMLNode(0)
      incomingNode.SetAttribute('IGTLVisible', 'false') # only shown in green slice, does not add value
      connectorNode.InvokeEvent(connectorNode.DeviceModifiedEvent)

    #trackerNode = slicer.mrmlScene.GetFirstNodeByName("Locator_Tracker")
    #if trackerNode:
    #  trackerDisplayNode = trackerNode.GetDisplayNode()
    #  trackerDisplayNode.SetSliceIntersectionVisibility(show)

    # The tracker is made of:
    #   * a green ruler to represent the needle
    #   * a green point at the base of the needle
    #   * a yellow point to represent the needle tip
    needleNode = self.needleNode()
    if needleNode:
      needleNode.GetDisplayNode().SetVisibility(show)

    hubNode = self.needleHub()
    if hubNode:
      hubNode.GetDisplayNode().SetVisibility(show)

    tipNode = self.needleTip()
    if tipNode:
      tipNode.GetDisplayNode().SetVisibility(show)

  # ------------------------------------------------------
  def setNeedleThickness(self, thickness):
    needleDisplayNode=self.needleNode().GetDisplayNode()
    needleDisplayNode.SetLineThickness(thickness)
    needleDisplayNode.SetLineWidth(thickness)
    needleDisplayNode.SetOverLineThickness(thickness*0.8)
    needleDisplayNode.SetUnderLineThickness(thickness*0.65)

  # ------------------------------------------------------
  def setHubSize(self, size): # rename to updateHubVisuals and add transparency control?
    hubNode = self.needleHub()
    displayNode = hubNode.GetDisplayNode()
    displayNode.SetGlyphScale(size)

  def setHubOpacity(self, opacity):
    node = self.needleHub()
    displayNode = node.GetDisplayNode()
    displayNode.SetOpacity(opacity)
    displayNode.SetSliceProjectionOpacity(opacity)

  # ------------------------------------------------------
  def setNeedleTipSize(self, size):
    tipNode = self.needleTip()
    displayNode = tipNode.GetDisplayNode()
    displayNode.SetGlyphScale(size)

  def setNeedleTipOpacity(self, opacity):
    node = self.needleTip()
    displayNode = node.GetDisplayNode()
    displayNode.SetOpacity(opacity)
    displayNode.SetSliceProjectionOpacity(opacity)

  # ------------------------------------------------------
  def createLine(self, name = 'Needle', color = [0., 1., 0.], projectedColor = [0.25, 0.7, 0.]):
    lineNode = slicer.vtkMRMLAnnotationRulerNode()
    lineNode.SetName(name)
    lineNode.SetPosition1(0.,0.,-1.)
    lineNode.SetPosition2(0.,0.,-70.)
    lineNode.Initialize(slicer.mrmlScene)
    lineNode.SetLocked(1)
    lineDisplayNode = lineNode.GetDisplayNode()
    lineDisplayNode.SetColor(color)
    lineDisplayNode.SetLineThickness(3)
    lineDisplayNode.SetLineWidth(3.0)
    lineDisplayNode.SetUnderLineThickness(2.0)
    lineDisplayNode.SetOverLineThickness(2.5)
    lineDisplayNode.SetMaxTicks(0)
    lineDisplayNode.SetProjectedColor(projectedColor)
    lineDisplayNode.SetProjectedOpacity(0.9)
    lineDisplayNode.SliceProjectionOn()
    lineDisplayNode.SliceProjectionDashedOff()
    lineDisplayNode.SliceProjectionUseRulerColorOff()
    lineDisplayNode.SliceProjectionColoredWhenParallelOff()
    lineDisplayNode.SetLabelPosition(0.9)
    lineDisplayNode.SetOpacity(1.0)
    lineDisplayNode.SetLabelVisibility(0)

    lineNode.GetAnnotationTextDisplayNode().SetOpacity(0.0)
    lineNode.GetAnnotationPointDisplayNode().SetOpacity(0) # disables cross-hairs
    #lineNode.GetAnnotationPointDisplayNode().SetGlyphScale(0)
    return lineNode

  # ------------------------------------------------------
  def createPoint(self, name = 'NeedleHub', size = 3, opacity = 0, filled = 0, color = [0., 1., 0.], projectedColor = [0.25, 0.7, 0.]):
    markupsNode = slicer.vtkMRMLMarkupsFiducialNode()
    slicer.mrmlScene.AddNode(markupsNode)
    markupsNode.SetName(name)
    markupsNode.AddFiducial(0,0,0)
    markupsNode.SetNthFiducialLabel(0,'')
    markupsNode.SetLocked(1)
    markupsDisplayNode = markupsNode.GetDisplayNode()
    markupsDisplayNode.SetColor(color)
    markupsDisplayNode.SetSelectedColor(color)
    markupsDisplayNode.SliceProjectionOn()
    markupsDisplayNode.SetSliceProjectionColor(projectedColor)
    markupsDisplayNode.SliceProjectionUseFiducialColorOff()
    markupsDisplayNode.SliceProjectionOutlinedBehindSlicePlaneOff()
    markupsDisplayNode.SetSliceProjectionOpacity(1.)
    markupsDisplayNode.SetOpacity(opacity)
    markupsDisplayNode.SetLineWidth(3.)
    markupsDisplayNode.SetSliceIntersectionThickness(3)
    markupsDisplayNode.SetGlyphScale(size)
    # markupsDisplayNode.SetGlyphFilled(filled)
    return markupsNode

  # ------------------------------------------------------
  def needleNode(self):
    lineNode = slicer.mrmlScene.GetFirstNodeByName("Needle")
    if not lineNode:
      lineNode = self.createLine()
      # apply the tracker transform to follow the tracker
      lineNode.SetAndObserveTransformNodeID(self.trackerTransformNode().GetID())
    return lineNode

  # ------------------------------------------------------
  def needleHub(self):
    hubNode = slicer.mrmlScene.GetFirstNodeByName("NeedleHub")
    if not hubNode:
      hubNode = self.createPoint('NeedleHub', workflowFunctions.get(self.widget, "NeedleHubSizeSlider").value)
      hubNode.SetNthFiducialPosition(0, 0., 0., -70)
      hubNode.SetAndObserveTransformNodeID(self.trackerTransformNode().GetID())
      hubNode.GetDisplayNode().AddViewNodeID('vtkMRMLSliceNodeBlue')
    return hubNode

  # ------------------------------------------------------
  def needleTip(self):
    tipNode = slicer.mrmlScene.GetFirstNodeByName("NeedleTip")
    if not tipNode:
      tipNode = self.createPoint('NeedleTip', 3, 1., 0, [1., 0.85, 0.], [0.75, 0.65, 0.])
      tipNode.SetNthFiducialPosition(0, 0., 0., 0.)
      tipNode.SetAndObserveTransformNodeID(self.trackerTransformNode().GetID())
    return tipNode

  # ------------------------------------------------------
  def createTrackerLayout(self):
    layoutNode = slicer.mrmlScene.GetFirstNodeByName('Layout')
    layout = """<layout type=\"vertical\">
  <item>
  <layout type=\"horizontal\">
   <item>
    <view class=\"vtkMRMLSliceNode\" singletontag=\"Red\">
     <property name=\"orientation\" action=\"default\">Axial</property>
     <property name=\"viewlabel\" action=\"default\">R</property>
     <property name=\"viewcolor\" action=\"default\">#F34A33</property>
    </view>
   </item>
   <item>
    <view class=\"vtkMRMLSliceNode\" singletontag=\"Yellow\">
     <property name=\"orientation\" action=\"default\">Sagittal</property>
     <property name=\"viewlabel\" action=\"default\">Y</property>
     <property name=\"viewcolor\" action=\"default\">#EDD54C</property>
    </view>
   </item>
  </layout>
 </item>
 <item>
  <layout type=\"horizontal\">
   <item>
    <view class=\"vtkMRMLSliceNode\" singletontag=\"Green\">
     <property name=\"orientation\" action=\"default\">Coronal</property>
     <property name=\"viewlabel\" action=\"default\">G</property>
     <property name=\"viewcolor\" action=\"default\">#6EB04B</property>
    </view>
   </item>
   <item>
    <view class=\"vtkMRMLSliceNode\" singletontag=\"Blue\">
     <property name=\"orientation\" action=\"default\">Axial</property>
     <property name=\"viewlabel\" action=\"default\">B</property>
     <property name=\"viewcolor\" action=\"default\">#7483E9</property>
    </view>
   </item>
  </layout>
 </item>
</layout>
"""
    layoutNode.AddLayoutDescription(layoutNode.SlicerLayoutUserView, layout)


  # ------------------------------------------------------
  def setTrackerLayout(self, enable):
    # Set the layout previously registered in createTrackerLayout()
    layoutNode = slicer.mrmlScene.GetFirstNodeByName('Layout')

    if hasattr(self, 'blueSliceProgressBar'):
      self.blueSliceProgressBar.visible = enable

    markerNode = slicer.mrmlScene.GetFirstNodeByName("TargetMarker")
    if markerNode:
      if enable:
        markerNode.GetDisplayNode().SliceProjectionOn()
      else:
        markerNode.GetDisplayNode().SliceProjectionOff()

    if not enable:
      layoutNode.SetViewArrangement(layoutNode.SlicerLayoutFourUpView)
      return

    if not self.isTrackerActive():
      return

    layoutNode.SetViewArrangement(layoutNode.SlicerLayoutUserView)

    # Copy the volumes into the new blue slice
    redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    redLogic = slicer.app.layoutManager().sliceWidget("Red").sliceLogic()

    blueSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeBlue')
    blueLogic = slicer.app.layoutManager().sliceWidget("Blue").sliceLogic()

    blueLogic.GetSliceCompositeNode().SetBackgroundVolumeID(
      redLogic.GetSliceCompositeNode().GetBackgroundVolumeID())
    blueLogic.GetSliceCompositeNode().SetForegroundVolumeID(
      redLogic.GetSliceCompositeNode().GetForegroundVolumeID())
    blueLogic.GetSliceCompositeNode().SetLabelVolumeID(
      redLogic.GetSliceCompositeNode().GetLabelVolumeID())

    self.makeRulersRed()

    # Set bird's eye view. We can't do it in the reslice as it would prevent
    # the user from changing the field of view. 5cm by default.
    blueSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeBlue')
    fov = blueSlice.GetFieldOfView()
    blueSlice.SetFieldOfView(50., 50. * fov[1] / fov[0], fov[2])

    if not hasattr(self, 'blueSliceProgressBar'):
      self.blueSliceProgressBar = ctk.ctkSliderWidget()
      self.blueSliceProgressBar.maximum = workflowFunctions.get(self.widget, "DistanceToTargetProgressBar").maximum
      self.blueSliceProgressBar.invertedAppearance = True
      self.blueSliceProgressBar.styleSheet = """
        QWidget:disabled {color:white; background-color:#5F5F71}
        QSpinBox:disabled {color:black; background-color:white}
        QSlider::sub-page:horizontal:disabled {
        background: qlineargradient(x1: 0, y1: 0,    x2: 1, y2: 0,
            stop: 0 #88f, stop: 1 #00f); }
        QSlider::add-page:horizontal:disabled { background:white; }
        QSlider::handle:horizontal:disabled {
        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
            stop:0 #fff, stop:1 #888); }
      """
      self.blueSliceProgressBar.enabled = False
      self.blueSliceProgressBar.connect(workflowFunctions.get(self.widget, "DistanceToTargetProgressBar"), "valueChanged(double)", self.blueSliceProgressBar, "setValue(double)")
      w = slicer.app.layoutManager().sliceWidget("Blue")
      w.layout().addWidget(self.blueSliceProgressBar)
      # w.styleSheet = "QWidget {color:black; background-color:white} QWidget:disabled {color:black; background-color:white}"

    # Align the views to the trajectory planes
    self.reslice()

  # ------------------------------------------------------
  def trackerTransformNode(self):
    return slicer.mrmlScene.GetFirstNodeByName("Tracker")

  # ------------------------------------------------------
  def trajectoryMarkerNode(self):
    return slicer.mrmlScene.GetFirstNodeByName("Path")

  # ------------------------------------------------------
  def observeTracker(self):
    workflowFunctions.addObserver(self, self.trackerTransformNode(), 'ModifiedEvent', self.updateFromTracker)

  # ------------------------------------------------------
  def updateFromTracker(self, transformNode, event):
    if (not workflowFunctions.get(self.widget, "FollowTrackerCheckBox").checked or
       not self.isTrackerActive()):
      return
    # Tracker RAS
    trackerTipPosition = self.trackerPosition()
    coordinates = '{:.2f}, {:.2f}, {:.2f}'.format( trackerTipPosition[0], trackerTipPosition[1], trackerTipPosition[2] )
    workflowFunctions.get(self.widget, "TrackerRASCoordinatesWidget").coordinates = coordinates

    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)

    trackerToTarget = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition, trackerTipPosition, trackerToTarget)
    trackerDistance = vtk.vtkMath.Norm(trackerToTarget)

    maxDist=max(trackerDistance, self.trajectoryDistance)
    workflowFunctions.get(self.widget, "DistanceToTargetProgressBar").maximum = maxDist
    if hasattr(self, 'blueSliceProgressBar'):
      self.blueSliceProgressBar.maximum = maxDist
    workflowFunctions.get(self.widget, "DistanceToTargetProgressBar").value = trackerDistance

    # Jump to tracker position
    self.reslice()

  # ------------------------------------------------------
  # \tbd merge with PathPlanner step
  def pathTransform(self):
    matrix = vtk.vtkMatrix4x4()
    entryPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition1(entryPosition)
    targetPosition = [0.,0.,0.]
    self.trajectoryMarkerNode().GetPosition2(targetPosition)
    pathNormal = [0.,0.,0.]
    vtk.vtkMath.Subtract(targetPosition,entryPosition,pathNormal)
    vtk.vtkMath.Normalize(pathNormal)
    return workflowFunctions.transformFromNormal(pathNormal, targetPosition)

  # ------------------------------------------------------
  def reslice(self):
    if (not workflowFunctions.get(self.widget, "FollowTrackerCheckBox").checked or
        not self.isTrackerActive()):
      return

    redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    yellowSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
    greenSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
    blueSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeBlue')

    if not blueSlice: # too early, the view does not exist yet
      return

    transform = self.pathTransform()
    transform.PreMultiply()
    transform.RotateZ(self.rotationAmount) # 0,90,180,270
    blueSlice.SetOrientationToReformat()
    blueSlice.GetSliceToRAS().DeepCopy( transform.GetMatrix() )
    blueSlice.UpdateMatrices()

    #transforms = self.trackerTransforms()
    redSlice.SetOrientationToAxial()
    #redSlice.SetOrientationToReformat()
    #redSlice.GetSliceToRAS().DeepCopy( transforms[2].GetMatrix() )
    #redSlice.UpdateMatrices()
    ##transform.RotateX( -90 )
    ##transform.Update()
    yellowSlice.SetOrientationToSagittal()
    #yellowSlice.SetOrientationToReformat()
    #yellowSlice.GetSliceToRAS().DeepCopy( transforms[0].GetMatrix() )
    #yellowSlice.UpdateMatrices()
    ##transform.RotateY( 90 )
    ##transform.Update()
    #greenSlice.SetOrientationToReformat()
    #greenSlice.GetSliceToRAS().DeepCopy( transforms[1].GetMatrix() )
    #greenSlice.UpdateMatrices()
    offAxialTransform = self.trackerOffAxialTransform()
    greenSlice.SetOrientationToReformat()
    greenSlice.GetSliceToRAS().DeepCopy( offAxialTransform.GetMatrix() )

    #redLogic = slicer.app.layoutManager().sliceWidget("Red").sliceLogic()
    #yellowLogic = slicer.app.layoutManager().sliceWidget("Yellow").sliceLogic()
    #greenLogic = slicer.app.layoutManager().sliceWidget("Green").sliceLogic()
    #blueLogic = slicer.app.layoutManager().sliceWidget("Blue").sliceLogic()

    #redLogic.FitSliceToAll(-1,-1)
    #yellowLogic.FitSliceToAll(-1,-1)
    #greenLogic.FitSliceToAll(-1,-1)
    #blueLogic.FitSliceToAll(-1,-1)

    self.jumpToTracker()


  # ------------------------------------------------------
  def jumpToTracker(self):
    trackerTipPosition = self.trackerPosition()
    self.markupsLogic.JumpSlicesToLocation(
      trackerTipPosition[0], trackerTipPosition[1], trackerTipPosition[2], False)

  # ------------------------------------------------------
  def trackerWorldMatrix(self):
    # Get the matrix of the tracker
    trackerNode = workflowFunctions.getTracker()
    transformMatrix = vtk.vtkMatrix4x4()
    trackerNode.GetMatrixTransformToWorld(transformMatrix)
    return transformMatrix

  # ------------------------------------------------------
  def trackerTransforms(self):
    transformMatrix = self.trackerWorldMatrix()
    normal = [0,0,0]
    normal[0] = transformMatrix.GetElement(0,2)
    normal[1] = transformMatrix.GetElement(1,2)
    normal[2] = transformMatrix.GetElement(2,2)
    origin = [0,0,0]
    origin[0] = transformMatrix.GetElement(0,3)
    origin[1] = transformMatrix.GetElement(1,3)
    origin[2] = transformMatrix.GetElement(2,3)
    return workflowFunctions.transformsFromNormal(normal, origin)

  def trackerOffAxialTransform(self):
    transformMatrix = self.trackerWorldMatrix()
    trackerNormal = [0,0,0]
    trackerNormal[0] = transformMatrix.GetElement(0,2)
    trackerNormal[1] = transformMatrix.GetElement(1,2)
    trackerNormal[2] = transformMatrix.GetElement(2,2)
    axial = [0., 0., 1.]
    vup = [0., 1., 0.]
    offAxial = workflowFunctions.findNormalPerpendicularAndClosestToOtherNormals(trackerNormal, axial, vup)
    origin = [0,0,0]
    origin[0] = transformMatrix.GetElement(0,3)
    origin[1] = transformMatrix.GetElement(1,3)
    origin[2] = transformMatrix.GetElement(2,3)
    return workflowFunctions.transformsFromNormal(offAxial, origin)[2]

  # ------------------------------------------------------
  def trackerTransform(self):
    transform = vtk.vtkTransform()
    transform.SetMatrix(self.transformMatrix())
    transform.Update()
    return transform

  # ------------------------------------------------------
  def trackerPosition(self):
    matrix = self.trackerWorldMatrix()

    x = matrix.GetElement(0, 3)
    y = matrix.GetElement(1, 3)
    z = matrix.GetElement(2, 3)

    return [x,y,z]

  # ------------------------------------------------------
  def setPETVolumeAsForeground(self):
    petVolumeNode = slicer.util.getNode('PETVolume')
    if petVolumeNode != None:
      workflowFunctions.setActiveVolume(petVolumeNode, 1, 0.5)
