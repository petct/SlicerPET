import os
import qt, ctk, slicer

import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTLoadDataStep( ctk.ctkWorkflowWidgetStep ) :

  def __init__( self, stepid ):
    self.initialize( stepid )
    self.setName( '1. Load input scans' )
    self.setDescription('Load MC PET and MC CT as well as Combined CT')

    qt.QTimer.singleShot(0, self.killButton)

  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()

  def createUserInterface( self ):
    layout = qt.QVBoxLayout(self)

    #Get widget from ui file and set to layout
    self.widget = workflowFunctions.loadUI('GuidedInterventionPETCTLoadData.ui')
    layout.addWidget(self.widget)
    workflowFunctions.setScene(self.widget)
    workflowFunctions.get(self.widget, 'CBCTVolumeComboBox').connect(
      'currentNodeChanged(vtkMRMLNode*)', self.setCBCTVolumeAsBackground)

  def validate( self, desiredBranchId ):

    #Check if the three volumes selected are different before going to next step
    # vtlMRMLScalarVolumeNode is returned
    petScans = workflowFunctions.get(self.widget, 'PETVolumeComboBox').currentNode()
    ctScans = workflowFunctions.get(self.widget, 'CTVolumeComboBox').currentNode()
    cbctScans = workflowFunctions.get(self.widget, 'CBCTVolumeComboBox').currentNode()

    if petScans != None and ctScans != None and cbctScans != None:

      # Each volume must be unique
      if petScans is ctScans or ctScans is cbctScans or cbctScans is petScans:
        messageBox = qt.QMessageBox.warning( self, 'Error', 'Please select different volumes' )
        super( GuidedInterventionPETCTLoadDataStep, self ).validate(False, desiredBranchId)

      else:
        # rename selected volumes
        petScans.SetName("PETVolume")
        ctScans.SetName("CTVolume")
        cbctScans.SetName("CBCTVolume")
        self.setHeatMapToPETVolume(petScans)
        super( GuidedInterventionPETCTLoadDataStep, self ).validate(True, desiredBranchId)

    else:
        messageBox = qt.QMessageBox.warning( self, 'Error', 'Please select the volumes for PET, CT, CBCT ' )
        super( GuidedInterventionPETCTLoadDataStep, self ).validate( False, desiredBranchId )

  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super( GuidedInterventionPETCTLoadDataStep, self ).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)
    self.setCBCTVolumeAsBackground()

  def onExit(self, goingTo, transitionType):
    goingToId = "None" # does not appear to be used. Is this needed?
    if goingTo: goingToId = goingTo.id()
    # execute the transition
    super( GuidedInterventionPETCTLoadDataStep, self ).onExit(goingTo, transitionType)

  def setCBCTVolumeAsBackground(self, cbctVolumeNode = None):
    if cbctVolumeNode == None:
      cbctVolumeNode = workflowFunctions.get(self.widget, 'CBCTVolumeComboBox').currentNode()
    if cbctVolumeNode != None:
      workflowFunctions.setActiveVolume(cbctVolumeNode)

  def setHeatMapToPETVolume(self, petVolumeNode):
    if petVolumeNode != None:
      d = petVolumeNode.GetDisplayNode()
      d.SetAndObserveColorNodeID('vtkMRMLPETProceduralColorNodePET-Heat')
      d.SetAutoWindowLevel(True) # if window/level information is found in dicom tags, this is off
      r = [0,1]
      d.GetDisplayScalarRange(r)
      d.SetThreshold(r[0] + (r[1]-r[0]) * 0.15, r[1])
      d.SetApplyThreshold(1)
