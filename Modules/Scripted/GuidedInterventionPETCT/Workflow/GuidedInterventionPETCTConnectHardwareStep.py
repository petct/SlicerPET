import qt, ctk, slicer

import workflowFunctions as workflowFunctions

class GuidedInterventionPETCTConnectHardwareStep(ctk.ctkWorkflowWidgetStep) :

  def __init__( self, stepid, openIGTGUI ):
    self.initialize( stepid )
    self.openIGTGUI = openIGTGUI
    self.setName( '3. Connect Hardware' )
    self.setDescription('Connect electro-magnetic hardware for the tracking system (OpenIGTLinkIF Module)')

    qt.QTimer.singleShot(0, self.killButton)


  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Tracker')
    if len(bl):
      bl[0].hide()


  def createUserInterface( self ):
    layout = qt.QVBoxLayout(self)
    layout.setContentsMargins(0,0,0,0)
    layout.addWidget(self.openIGTGUI)

  '''
    # add watcher to this connection node
    connector = slicer.mrmlScene. GetFirstNodeByName("IGTLConnector" )
    connector.AddObserver( connector.ConnectedEvent, self.onConnected ) # returns id
    connector.AddObserver( connector.DisconnectedEvent, self.onDisconnected )


  def onConnected(self, obj, event):
    print "Connection changed :", event


  def onDisconnected(self, obj, event):
    print "Disconnection changed :", event
  '''


  def connectionStatusOK(self):
    connector = slicer.mrmlScene.GetFirstNodeByName("IGTLConnector")
    if connector.GetState() == connector.STATE_CONNECTED:
      return true
    else:
      return false


  def validate( self, desiredBranchId ):
    activeTracker = workflowFunctions.get(self.openIGTGUI,'ConnectorStateCheckBox')
    if activeTracker.isChecked():
      super(GuidedInterventionPETCTConnectHardwareStep, self).validate(True, desiredBranchId)
    else:
      messageBox = qt.QMessageBox.warning( self, 'Error', 'Please establish link between tracker and SlicerPET ' )
      super(GuidedInterventionPETCTConnectHardwareStep, self ).validate( False, desiredBranchId )


  def onEntry(self, comingFrom, transitionType):
    comingFromId = "None"
    if comingFrom: comingFromId = comingFrom.id()
    super(GuidedInterventionPETCTConnectHardwareStep, self).onEntry(comingFrom, transitionType)
    qt.QTimer.singleShot(0, self.killButton)
    workflowFunctions.setActiveVolume(None, 1, 0.0)

  def onExit(self, goingTo, transitionType):
    goingToId = "None"
    if goingTo: goingToId = goingTo.id()
    super(GuidedInterventionPETCTConnectHardwareStep, self).onExit(goingTo, transitionType)
