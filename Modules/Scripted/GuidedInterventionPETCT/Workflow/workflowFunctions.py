import sys,os
import vtk, slicer, qt

#Get object of a widget by a given name
def get(widget, objectName):
    if widget.objectName == objectName:
        return widget
    else:
        for w in widget.children():
            resulting_widget = get(w, objectName)
            if resulting_widget:
                return resulting_widget
        return None

#Set the given widget and children to the Slicer MRML Scene if possible
def setScene(widget):
    try:
       widget.setMRMLScene(slicer.mrmlScene)
    except AttributeError:
        for w in widget.children():
            resulting_widget = setScene(w)
            if resulting_widget:
                try:
                    resulting_widget.setMRMLScene(slicer.mrmlScene)
                except AttributeError:
                    continue

#Load UI from files for the GuidedInterventionPETCT Module
def loadUI(UIFileName):
    loader = qt.QUiLoader()
    moduleName = 'GuidedInterventionPETCT'
    scriptedModulesPath = eval('slicer.modules.%s.path' % moduleName.lower())
    scriptedModulesPath = os.path.dirname(scriptedModulesPath)
    path = os.path.join(scriptedModulesPath, 'Resources', 'UI', UIFileName)

    qfile = qt.QFile(path)
    qfile.open(qt.QFile.ReadOnly)
    widget = loader.load(qfile)
    return widget

#Get the tracker Node from the OpenIGTLinkIF Module
def getTracker():
    igtlNodeCollection = slicer.mrmlScene.GetNodesByClass("vtkMRMLIGTLConnectorNode")
    igtlNodeCollection.UnRegister(slicer.mrmlScene)
    for i in xrange(igtlNodeCollection.GetNumberOfItems()):
      igtlNode = igtlNodeCollection.GetItemAsObject(i)
      for j in xrange(igtlNode.GetNumberOfIncomingMRMLNodes()):
        incomingNode = igtlNode.GetIncomingMRMLNode(j)
        if incomingNode.IsA("vtkMRMLLinearTransformNode"):
          return incomingNode
    return None

#Center Slices and 3D view in SlicerPET
def centerViews():

    #Center Slices
    appLogic = slicer.app.applicationLogic()
    appLogic.FitSliceToAll()

    #Center 3D view
    layoutManager = slicer.app.layoutManager()
    threeDWidget = layoutManager.threeDWidget(0)

    threeDViewController = slicer.qMRMLThreeDViewControllerWidget()
    threeDViewController.setThreeDView(threeDWidget.threeDView())
    threeDViewController.resetFocalPoint()

def removeObservers(self, method):
  for o, e, m, g, t in self.Observations:
    if method == m:
      o.RemoveObserver(t)
      self.Observations.remove([o, e, m, g, t])

def addObserver(self, object, event, method, group = 'none'):
  if object == None:
    return
  if hasObserver(self, object, event, method):
    # print (self, 'already has observer', object, event, method, group)
    return
  tag = object.AddObserver(event, method)
  self.Observations.append([object, event, method, group, tag])

def hasObserver(self, object, event, method):
  for o, e, m, g, t in self.Observations:
    if o == object and e == event and m == method:
      return True
  return False

def observer(self, event, method):
  for o, e, m, g, t in self.Observations:
    if e == event and m == method:
      return o
  return None

# ------------------------------------------------------
def setActiveVolume(volumeNode, layer = 0, opacity = 1.0):
  selectionNode = slicer.mrmlScene.GetNthNodeByClass(0, 'vtkMRMLSelectionNode')
  # Set the volume as the active volume
  {
  0: selectionNode.SetActiveVolumeID,
  1: selectionNode.SetSecondaryVolumeID,
  2: selectionNode.SetActiveLabelVolumeID
  }[layer](volumeNode.GetID() if volumeNode != None else "")

  # Propagate the volume to all the slice composite nodes
  slicer.app.applicationLogic().PropagateVolumeSelection()

  # Not sure why the foreground opacity is 0. by default.
  sliceCompositeNodes = slicer.mrmlScene.GetNodesByClass("vtkMRMLSliceCompositeNode")
  sliceCompositeNodes.UnRegister(slicer.mrmlScene)
  for i in xrange(sliceCompositeNodes.GetNumberOfItems()):
    sliceCompositeNode = sliceCompositeNodes.GetItemAsObject(i)
    if layer != 0:
      {
      1: sliceCompositeNode.SetForegroundOpacity,
      2: sliceCompositeNode.SetLabelOpacity
      }[layer](opacity)

# ------------------------------------------------------
# invertNormal to be in the same direction than the closest orientation
def transformFromNormal(normal, origin, invertNormal = True):
  # View up vector
  s = [0.,1.,0.]
  # Left-Right vector
  t = [1.,0.,0.]
  # Find what is the direction the normal is closest to.
  axial = vtk.vtkMath.Dot(normal, [0., 0., 1.])
  sagittal = vtk.vtkMath.Dot(normal, [1., 0., 0.])
  coronal = vtk.vtkMath.Dot(normal, [0., 1., 0.])
  invert = False
  invertT = False
  if (abs(axial) >= abs(sagittal) and abs(axial) >= abs(coronal)):
    invert = (axial < 0.)
    s = [0., 1. , 0.]
  elif (abs(sagittal) >= abs(axial) and abs(sagittal) >= abs(coronal)):
    invert = (sagittal < 0.)
    s = [0., 0., 1.]
  else:
    invert = (coronal < 0.)
    s = [0., 0., 1.]
    invertT = True

  n = normal
  if invert and invertNormal:
    n[0] = -normal[0]
    n[1] = -normal[1]
    n[2] = -normal[2]

  vtk.vtkMath.Cross(n, s, t)
  vtk.vtkMath.Cross(t, n, s)

  vtk.vtkMath.Normalize(s)
  vtk.vtkMath.Normalize(t)

  # Make sure the left-right is the same than the one for the closest
  # direction (axial, coronal or sagittal) left-right.
  if invert and invertNormal:
    t[0] = -t[0]
    t[1] = -t[1]
    t[2] = -t[2]

  matrix = vtk.vtkMatrix4x4()
  matrix.SetElement(0,0, t[0])
  matrix.SetElement(1,0, t[1])
  matrix.SetElement(2,0, t[2])
  matrix.SetElement(0,1, s[0])
  matrix.SetElement(1,1, s[1])
  matrix.SetElement(2,1, s[2])
  matrix.SetElement(0,2, n[0])
  matrix.SetElement(1,2, n[1])
  matrix.SetElement(2,2, n[2])
  matrix.SetElement(0,3, origin[0])
  matrix.SetElement(1,3, origin[1])
  matrix.SetElement(2,3, origin[2])

  transform = vtk.vtkTransform()
  transform.SetMatrix(matrix)
  transform.Update()
  return transform

# ------------------------------------------------------
def transformsFromNormal(normal, origin):
  transformZ = transformFromNormal(normal, origin)
  normal = [0., 0., 1.]
  normal[0] = transformZ.GetMatrix().GetElement(0,0)
  normal[1] = transformZ.GetMatrix().GetElement(1,0)
  normal[2] = transformZ.GetMatrix().GetElement(2,0)
  transformX = transformFromNormal(normal, origin)
  normal[0] = transformZ.GetMatrix().GetElement(0,1)
  normal[1] = transformZ.GetMatrix().GetElement(1,1)
  normal[2] = transformZ.GetMatrix().GetElement(2,1)
  transformY = transformFromNormal(normal, origin)
  return [transformX, transformY, transformZ]


# ------------------------------------------------------
def findNormalPerpendicularAndClosestToOtherNormals(perpendicularToNormal, closestToNormal = [0., 0., 1.], vup = [0., 1., 0.]):
  s = [0.,1.,0.]
  t = [1.,0.,0.]

  vtk.vtkMath.Cross(perpendicularToNormal, closestToNormal, t)
  vtk.vtkMath.Cross(t, perpendicularToNormal, s)

  vtk.vtkMath.Normalize(s)
  direction = vtk.vtkMath.Dot(s, closestToNormal)
  if direction < 0:
    s[0] = -s[0]
    s[1] = -s[1]
    s[2] = -s[2]
  return s
