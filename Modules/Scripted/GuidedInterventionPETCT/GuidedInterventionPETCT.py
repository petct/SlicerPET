# coding=utf-8
import os
import unittest
import vtk, qt, ctk, slicer

import GuidedInterventionPETCTWorkflow

class GuidedInterventionPETCT:
  def __init__(self, parent):
    parent.title = "Guided Intervention PETCT"
    parent.categories = ["IGT"]
    parent.dependencies = []
    parent.contributors = ["Andinet Enquobahrie (Kitware), Guillaume Sala (Kitware), Julien Finet (Kitware), Dženan Zukić (Kitware), Rahul Khare (Children National Medical Center)"]
    parent.helpText = """
    This module was built to be used for PET/CT guided biopsy in a CT interventional suite without significant disruption.<br><br>

    The module follows a workflow that consist of several steps, three of them use existing Slicer modules:<br>
      a. OpenIGTLinkIF developed by Junichi Tokuda, Jean-Christophe Fillion-Robin and the OpenIGTLink community<br>
      b. PivotCalibration developed by Guillaume Sala (Uses PlusLib)<br>
      c. VolumeResliceDriver developed by Junichi Tokuda <br><br>

    The different steps of the workflow are as follows:<br><br>

    1. Load PET-CT Images, Load CBCT<br>
    2. Register MC CT-CBCT<br>
    3. Connect EM Hardware (OpenIGTLinkIF)<br>
    4. Pivot Calibration (PivotCalibration Module)<br>
    5. Register EM and PET-CT spaces<br>
    6. Simulating path between entry and target points<br>
    7. Tracking the needle tip<br>
    """
    parent.acknowledgementText = """
    This work is the result of a collaboration between Geortown University, Children National Medical Center,
    Prof. Paul Kinahan (University of Washington), Dr.Ron Korn (Scottsdale Medical Imaging) and Kitware Inc.<br>
    It was funded by the National Cancer Institute and the National Institutes of Health, grants 2R42CA153488-02A1 and R41CA153488-01.<br><br>

    This module works tightly with:<br>
    -IGSTK: To establish the connection between SlicerPET and the tracker device.<br>
    <a href=\"http://www.igstk.org\">www.igstk.org</a><br>
    -PlusLib: To run the pivot calibration algorithm.<br>
    <a href=\"https://www.assembla.com/spaces/plus/wiki\">https://www.assembla.com/spaces/plus/wiki</a><br>
    """
    self.parent = parent

#
# qGuidedInterventionPETCTWidget
#

class GuidedInterventionPETCTWidget:
  def __init__(self, parent = None):
    if not parent:
      self.parent = slicer.qMRMLWidget()
      self.parent.setLayout(qt.QVBoxLayout())
      self.parent.setMRMLScene(slicer.mrmlScene)
    else:
      self.parent = parent
    self.layout = self.parent.layout()
    if not parent:
      self.setup()
      self.parent.show()


  def setup(self):

    #Initializing the workflow Widget and logic
    self.workflow = ctk.ctkWorkflow()

    workflowWidget = ctk.ctkWorkflowStackedWidget()
    workflowWidget.setWorkflow( self.workflow )
    workflowWidget.buttonBoxWidget().hideGoToButtons = True

    # Grabing the GUI from other modules to include to the workflow
    openIGTGUI = slicer.util.getNewModuleGui('OpenIGTLinkIF')
    pivotCalibrationGUI = slicer.util.getNewModuleGui('PivotCalibration')
    annotationsGUI = slicer.util.getModuleGui('Markups')

    # Creating each step of the workflow
    self.loadDataStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTLoadDataStep('LoadData')
    self.registrationStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTRegistrationStep('Registration')
    self.connectStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTConnectHardwareStep('ConnectHardware', openIGTGUI)
    self.pivotCalibrationStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTPivotCalibrationStep('PivotCalibration', pivotCalibrationGUI)
    self.registerEMCBCTStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTRegisterEMCBCTStep('RegisterEMCBCT', annotationsGUI)
    self.pathPlannerStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTPathPlannerStep('PathPlanner')
    self.guidanceStep = GuidedInterventionPETCTWorkflow.GuidedInterventionPETCTGuidanceStep('Guidance')

    listOfSteps = []
    listOfSteps.append(self.loadDataStep)
    listOfSteps.append(self.registrationStep)
    listOfSteps.append(self.connectStep)
    listOfSteps.append(self.pivotCalibrationStep)
    listOfSteps.append(self.registerEMCBCTStep)
    listOfSteps.append(self.pathPlannerStep)
    listOfSteps.append(self.guidanceStep)

    # Connecting the created steps of the workflow
    self.workflow.addTransition(self.loadDataStep, self.registrationStep)
    self.workflow.addTransition(self.registrationStep, self.connectStep)
    self.workflow.addTransition(self.connectStep, self.pivotCalibrationStep)
    self.workflow.addTransition(self.pivotCalibrationStep, self.registerEMCBCTStep)
    self.workflow.addTransition(self.registerEMCBCTStep, self.pathPlannerStep)
    self.workflow.addTransition(self.pathPlannerStep, self.guidanceStep)

    # Starting and showing the module in layout
    self.workflow.start()
    workflowWidget.visible = True
    self.layout.addWidget(workflowWidget)

  def enter(self):
    layoutNode = slicer.mrmlScene.GetFirstNodeByName('Layout')
    layoutNode.SetViewArrangement(layoutNode.SlicerLayoutFourUpView)
    self.guidanceStep.makeRulersRed()
    