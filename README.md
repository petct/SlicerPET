The application and the modules developed in this project are distributed under the Apache 2.0 license:
http://www.apache.org/licenses/LICENSE-2.0

You are free to use, modify, and redistribute SlicerPET for non-commercial and commercial uses.

Project's wiki:
http://public.kitware.com/Wiki/PET-CT

List of all bugs:
http://public.kitware.com/Bug/view_all_bug_page.php
(choose project *PET-CT-guidance*)