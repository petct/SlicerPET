
set(proj PlusLib)

# Set dependency list
set(PlusLib_DEPENDENCIES VTKv6 ITKv4)

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj DEPENDS_VAR ${proj}_DEPENDENCIES)

if(UNIX)
  set(pluslib_cxx_flags "-fPIC ${ep_common_cxx_flags}")
  set(pluslib_c_flags "-fPIC ${ep_common_c_flags}")
else()
  set(pluslib_cxx_flags "${ep_common_cxx_flags}")
  set(pluslib_c_flags "${ep_common_c_flags}")
endif()

set(PLUSBUILD_ASSEMBLA_USERNAME "perklab_anonymous" )
set(PLUSBUILD_ASSEMBLA_PASSWORD "anonymous" )

set(PlusLib_SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj})
set(PlusLib_BINARY_DIR ${CMAKE_BINARY_DIR}/${proj}-build)

ExternalProject_Add(${proj}
  ${${proj}_EP_ARGS}
  SVN_USERNAME ${PLUSBUILD_ASSEMBLA_USERNAME}
  SVN_PASSWORD ${PLUSBUILD_ASSEMBLA_PASSWORD}
  SVN_REPOSITORY "https://subversion.assembla.com/svn/plus/trunk/PlusLib"
  SVN_REVISION -r "3900"
  SOURCE_DIR ${${proj}_SOURCE_DIR}
  BINARY_DIR ${${proj}_BINARY_DIR}
  CMAKE_CACHE_ARGS
    -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
    -DCMAKE_CXX_FLAGS:STRING=${pluslib_cxx_flags}
    -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
    -DCMAKE_C_FLAGS:STRING=${pluslib_c_flags}
    -DBUILD_TESTING:BOOL=OFF
    -DSubversion_SVN_EXECUTABLE:FILEPATH=${Subversion_SVN_EXECUTABLE}
    -DPLUS_EXECUTABLE_OUTPUT_PATH:STRING=${${proj}_BINARY_DIR}/bin
    -DPLUS_USE_BRACHY_TRACKER:BOOL=OFF
    -DPlusLib_DIR:PATH=${${proj}_BINARY_DIR}/src
    # Dependencies
    -DVTK_DIR:PATH=${VTK_DIR}
    -DITK_DIR:PATH=${ITK_DIR}
    -DQT_QMAKE_EXECUTABLE:FILEPATH=${QT_QMAKE_EXECUTABLE}
  INSTALL_COMMAND ""
  DEPENDS
    ${PlusLib_DEPENDENCIES}
  )

set(PlusLib_DIR ${CMAKE_BINARY_DIR}/PlusLib-build/src)

mark_as_superbuild(PlusLib_DIR)

#-----------------------------------------------------------------------------
# Launcher setting specific to build tree

set(_lib_subdir lib)
if(WIN32)
  set(_lib_subdir bin)
endif()

# library paths
set(${proj}_LIBRARY_PATHS_LAUNCHER_BUILD ${PlusLib_BINARY_DIR}/${_lib_subdir}/<CMAKE_CFG_INTDIR>)
mark_as_superbuild(
  VARS ${proj}_LIBRARY_PATHS_LAUNCHER_BUILD
  LABELS "LIBRARY_PATHS_LAUNCHER_BUILD"
  )

#-----------------------------------------------------------------------------
# Launcher setting specific to install tree

# library paths
if(UNIX AND NOT APPLE)
  set(${proj}_LIBRARY_PATHS_LAUNCHER_INSTALLED <APPLAUNCHER_DIR>/lib/PlusLib)
  mark_as_superbuild(
    VARS ${proj}_LIBRARY_PATHS_LAUNCHER_INSTALLED
    LABELS "LIBRARY_PATHS_LAUNCHER_INSTALLED"
    )
endif()
