#!/usr/local/bin/cmake -P

message("Upload SlicerPET packages")


set(MIDAS_URL "http://packages.kitware.com")
set(MIDAS_USER "dzenan.zukic@kitware.com")
set(MIDAS_API_KEY "yQL85kao0hyA33wNh385jdpkwQgMSZNgFpeBAfQq")

set(midas_api_script ${CMAKE_CURRENT_LIST_DIR}/MidasAPI.cmake)
if(NOT EXISTS ${midas_api_script})
  file(DOWNLOAD ${MIDAS_URL}/api/rest?method=midas.packages.script.download ${CMAKE_CURRENT_LIST_DIR}/MidasAPI.cmake)
endif()
include(${midas_api_script})

file(GLOB packages "SlicerPET-*")
foreach(package ${packages})
  get_filename_component(PACKAGE_NAME ${package} NAME)
  
  set(_version_regex "[0-2].[0-9].[0-9][0-9]?")
  set(_os_regex "-(win|linux|macosx|Darwin|Windows|Linux)")
  
  set(PACKAGE_IS_RELEASE FALSE)
  if(PACKAGE_NAME MATCHES ${_version_regex}${_os_regex})
    set(PACKAGE_IS_RELEASE TRUE)
  endif()
  
  string(REGEX MATCH ${_version_regex} PACKAGE_VERSION ${PACKAGE_NAME})
  
  string(REGEX MATCH ${_os_regex} _os ${PACKAGE_NAME})
  set(PACKAGE_OS "NA")
  if(NOT ${_os} STREQUAL "")
    if(${_os} MATCHES "-win|-Windows")
      set(PACKAGE_OS "Windows")
    elseif(${_os} MATCHES "-linux|-Linux")
      set(PACKAGE_OS "Linux")
    elseif(${_os} MATCHES "-macosx|-Darwin")
      set(PACKAGE_OS "MacOSX")
    endif()
  endif()
  
  set(PACKAGE_BITNESS "NA")
  if(${PACKAGE_NAME} MATCHES "amd64")
    set(PACKAGE_BITNESS "64-bit")
  elseif(${PACKAGE_NAME} MATCHES "i386")
    set(PACKAGE_BITNESS "32-bit")
  endif()
  if(PACKAGE_BITNESS STREQUAL "NA")
    if(PACKAGE_OS MATCHES "Linux" OR PACKAGE_OS MATCHES "MacOSX")
      set(PACKAGE_BITNESS "64-bit")
    elseif(PACKAGE_OS MATCHES "Windows")
      set(PACKAGE_BITNESS "32-bit")
    endif()
  endif()
  
  
  message("----------------------")
  message("${PACKAGE_NAME}")
  message("  os: ${PACKAGE_OS}")
  message("  bitness: ${PACKAGE_BITNESS}")
  message("  version: ${PACKAGE_VERSION}")
  message("  is_release: ${PACKAGE_IS_RELEASE}")
  
  if(PACKAGE_IS_RELEASE)
    midas_api_package_upload(
      API_URL ${MIDAS_URL}
      API_EMAIL ${MIDAS_USER}
      API_KEY ${MIDAS_API_KEY}
      FILE "${CMAKE_CURRENT_LIST_DIR}/${PACKAGE_NAME}"
      NAME ${PACKAGE_NAME}
      FOLDER_ID 194
      APPLICATION_ID 19
      OS ${PACKAGE_OS}
      ARCH ${PACKAGE_BITNESS}
      PACKAGE_TYPE "NSIS installer"
      SUBMISSION_TYPE Experimental
      RELEASE "${PACKAGE_VERSION}"
      RESULT_VARNAME upload_status
      )
    message("Upload status: ${upload_status}")
  endif()
endforeach()
