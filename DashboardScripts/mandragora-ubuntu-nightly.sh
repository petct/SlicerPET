#!/bin/bash
#
# Script to run nightly dashboard
#

# select off screen display
export DISPLAY=:0.0 # just DISPLAY=:0.0 without export is not enough

CTEST=/usr/local/bin/ctest

SCRIPT_DIR=/home/linus/projects/PET_CT-project/Dashboards/DashboardScripts
LOG_DIR=${SCRIPT_DIR}/Log

mkdir -p ${LOG_DIR}

# script from SlicerPET era dashboard

${CTEST} -S ${SCRIPT_DIR}/mandragora-ubuntu-64bits_slicer_release_nightly.cmake \
        -VV &> ${LOG_DIR}/mandragora-ubuntu-64bits_slicer_release_nightly.log

