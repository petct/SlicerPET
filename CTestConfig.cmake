
set(CTEST_PROJECT_NAME "SlicerPET")
set(CTEST_NIGHTLY_START_TIME "3:00:00 UTC")

set(CTEST_DROP_METHOD "http")
# See this issue for more details: http://public.kitware.com/Bug/view.php?id=12555

set(CTEST_DROP_SITE "kwcdash.kitware.com")
set(CTEST_DROP_LOCATION "/CDash/submit.php?project=SlicerPET")

set(CTEST_DROP_SITE_CDASH TRUE)
